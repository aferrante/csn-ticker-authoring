object TemplateDwellEditorDlg: TTemplateDwellEditorDlg
  Left = 539
  Top = 250
  BorderStyle = bsDialog
  Caption = 'Template Dwell Editor'
  ClientHeight = 574
  ClientWidth = 668
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 17
    Top = 5
    Width = 167
    Height = 16
    Caption = 'Templates in Database:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object First: TLabel
    Left = 37
    Top = 449
    Width = 31
    Height = 16
    Caption = 'First'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 98
    Top = 449
    Width = 34
    Height = 16
    Caption = 'Prior'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 160
    Top = 449
    Width = 32
    Height = 16
    Caption = 'Next'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 224
    Top = 449
    Width = 30
    Height = 16
    Caption = 'Last'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 286
    Top = 449
    Width = 29
    Height = 16
    Caption = 'Add'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 339
    Top = 449
    Width = 47
    Height = 16
    Caption = 'Delete'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 411
    Top = 449
    Width = 28
    Height = 16
    Caption = 'Edit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 470
    Top = 449
    Width = 32
    Height = 16
    Caption = 'Post'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label10: TLabel
    Left = 524
    Top = 449
    Width = 49
    Height = 16
    Caption = 'Cancel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label11: TLabel
    Left = 599
    Top = 449
    Width = 30
    Height = 16
    Caption = 'Refr'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 17
    Top = 429
    Width = 438
    Height = 16
    Caption = 'Note: Dwell Time Must be Set in Milliseconds (1000 = 1 Second)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object tsDBGrid1: TtsDBGrid
    Left = 16
    Top = 29
    Width = 633
    Height = 393
    CellSelectMode = cmNone
    CheckBoxStyle = stCheck
    Cols = 3
    DatasetType = dstStandard
    DataSource = dmMain.dsTemplate_Defs
    DefaultRowHeight = 20
    ExactRowCount = True
    ExportDelimiter = ','
    FieldState = fsCustomized
    HeadingFont.Charset = DEFAULT_CHARSET
    HeadingFont.Color = clWindowText
    HeadingFont.Height = -13
    HeadingFont.Name = 'MS Sans Serif'
    HeadingFont.Style = [fsBold]
    HeadingHeight = 20
    HeadingParentFont = False
    ParentShowHint = False
    RowChangedIndicator = riAutoReset
    RowMoving = False
    ShowHint = False
    TabOrder = 0
    Version = '2.20.26'
    XMLExport.Version = '1.0'
    XMLExport.DataPacketVersion = '2.0'
    DataBound = True
    ColProperties = <
      item
        DataCol = 1
        FieldName = 'Template_ID'
        Col.FieldName = 'Template_ID'
        Col.Font.Charset = DEFAULT_CHARSET
        Col.Font.Color = clWindowText
        Col.Font.Height = -13
        Col.Font.Name = 'MS Sans Serif'
        Col.Font.Style = []
        Col.Heading = 'ID'
        Col.ParentFont = False
        Col.ReadOnly = True
        Col.Width = 76
        Col.AssignedValues = '?'
      end
      item
        DataCol = 2
        FieldName = 'Template_Description'
        Col.FieldName = 'Template_Description'
        Col.Font.Charset = DEFAULT_CHARSET
        Col.Font.Color = clWindowText
        Col.Font.Height = -13
        Col.Font.Name = 'MS Sans Serif'
        Col.Font.Style = []
        Col.Heading = 'Description'
        Col.ParentFont = False
        Col.ReadOnly = True
        Col.Width = 402
        Col.AssignedValues = '?'
      end
      item
        DataCol = 3
        FieldName = 'Default_Dwell'
        Col.FieldName = 'Default_Dwell'
        Col.Font.Charset = DEFAULT_CHARSET
        Col.Font.Color = clWindowText
        Col.Font.Height = -13
        Col.Font.Name = 'MS Sans Serif'
        Col.Font.Style = []
        Col.Heading = 'Dwell Time (mS)'
        Col.ParentFont = False
        Col.Width = 126
        Col.AssignedValues = '?'
      end>
  end
  object BitBtn1: TBitBtn
    Left = 229
    Top = 520
    Width = 97
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 349
    Top = 520
    Width = 89
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Kind = bkCancel
  end
  object DBNavigator1: TDBNavigator
    Left = 21
    Top = 472
    Width = 620
    Height = 33
    DataSource = dmMain.dsTemplate_Defs
    TabOrder = 3
  end
end
