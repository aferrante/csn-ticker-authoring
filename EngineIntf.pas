// Rev: 02/04/10  M. Dilworth  Video Design Software Inc.
// Modified to use Stats Inc. as data source
unit EngineIntf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OoMisc, ADTrmEmu, AdPacket, AdPort, AdWnPort, ExtCtrls, Globals, FileCtrl,
  ScktComp;

type
  TEngineInterface = class(TForm)
    AdTerminal1: TAdTerminal;
    PacketEnableTimer: TTimer;
    TickerCommandDelayTimer: TTimer;
    PacketIndicatorTimer: TTimer;
    TickerPacketTimeoutTimer: TTimer;
    JumpToNextTickerRecordTimer: TTimer;
    BugCommandDelayTimer: TTimer;
    ExtraLineCommandDelayTimer: TTimer;
    JumpToNextBugRecordTimer: TTimer;
    JumpToNextExtraLineRecordTimer: TTimer;
    DisplayClockTimer: TTimer;
    BugPacketTimeoutTimer: TTimer;
    ExtraLinePacketTimeoutTimer: TTimer;
    CommandDwellTimer: TTimer;
    EnginePort: TClientSocket;
    procedure FormActivate(Sender: TObject);
    //Timer related
    procedure PacketEnableTimerTimer(Sender: TObject);
    procedure TickerCommandDelayTimerTimer(Sender: TObject);
    procedure PacketIndicatorTimerTimer(Sender: TObject);
    procedure TickerPacketTimeoutTimerTimer(Sender: TObject);
    procedure JumpToNextTickerRecordTimerTimer(Sender: TObject);
    //Engine connection port
    procedure EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
    //Logging
    procedure WriteToErrorLog (ErrorString: String);
    procedure WriteToAsRunLog (AsRunString: String);
    procedure CommandDwellTimerTimer(Sender: TObject);
  private
    function ProcessStyleChips(CmdStr: String): String;
  public
    procedure SendTickerRecord(NextRecord: Boolean; RecordIndex: SmallInt);
    function GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
    function GetMixedCase(InStr: String): String;
    function GetSponsorLogoFileName(LogoName: String): String;
    function LoadAllTemplateInfo(TemplateID: SmallInt; TemplateActionType: SmallInt): TemplateDefsRec;

    function CheckForActiveSponsorLogo: Boolean;

    //Added for XPression
    procedure SendCommand(CmdStr: String);
    procedure RunStartupCommands;
    procedure Delay(Delay: LongInt);
    procedure TickerOut;
    function GetSceneDirectorState(SceneDirectorName: String): SmallInt;
    procedure SetSceneDirectorState(SceneDirectorName: String; State: SmallInt; ResetAll: Boolean);
    function GetActionControllerState(ActionControllerID: SmallInt): SmallInt;
    procedure SetActionControllerState(ActionControllerID: SmallInt; State: SmallInt);
    function ParseCommandData(CommandIn: String): CommandRec;
    procedure AddCommandToQueue(CmdStr: String; DwellTime: SmallInt);
    function GetSceneDirectorNameForAction (ActionType, CommandType: SmallInt): String;
    function GetSceneNameFromID(SceneDirectorID: SmallInt): String;
    function GetActionControllerNameFromID(ActionID, ABState: SmallInt): String;
    procedure SendPlayCommandStrings(ActionType, CommandType, SceneDirectorID, ActionControllerID, Dwell: SmallInt; ActionIsCrawl: Boolean);
    procedure UpdateTimeOfDayClocks;
    function GetTimeDurationForQueue: SmallInt;
    procedure SetTemplateFieldLastValueCheckString(FieldID: SmallInt; LastValueCheckString: String);
    function GetTemplateFieldLastValueCheckString(FieldID: SmallInt): String;
    procedure DeleteActionControllersFromTempTemplateCommandCollection;
    procedure UpdateScoreChipStatus (CurrentGameData: GameRec);
  end;

var
  EngineInterface: TEngineInterface;
  DisableCommandTimer: Boolean;

const
 UseDataPacket = TRUE;

implementation

uses Main, //Main form
     DataModule,
     GameDataFunctions;

{$R *.DFM}

//Init
procedure TEngineInterface.FormActivate(Sender: TObject);
begin
end;

//These are the graphics engine control procedures
////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to delay a specified number of 100mS increments
procedure TEngineInterface.Delay(Delay: LongInt);
var
  i: SmallInt;
begin
  if (Delay >= 100) then
  begin
    for i := 1 to (Delay DIV 100) do
    begin
      Sleep(100);
      Application.ProcessMessages;
    end;
  end;
end;

//Function to send a simple command
procedure TEngineInterface.SendCommand(CmdStr: String);
begin
  if (SocketConnected) AND (EngineParameters.Enabled = TRUE) then
  begin
    //Send command
    try
      EnginePort.Socket.SendText(Trim(CmdStr) + #13#10);
    except
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying send startup command to gateway');
    end;
  end;
end;

//Function to run startup commands
procedure TEngineInterface.RunStartupCommands;
var
  i: SmallInt;
  SponsorLogoRecPtr: ^SponsorLogoRec;
  SponsorLogoPath: String;
begin
  //Load in startup command information
  with dmMain.UtilityQuery do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM Startup_Commands');
    Open;
    if (RecordCount > 0) then
    begin
      First;
      repeat
        //Send command
        SendCommand(FieldByName('Command').AsString);
        //Dwell
        Sleep (FieldByName('Dwell').AsInteger);
        //Go to next record
        Next;
      until (EOF);
    end;
  end;
end;

//Function to parse queued command to get scene ID, dwell and command
function TEngineInterface.ParseCommandData(CommandIn: String): CommandRec;
var
  Tokens: Array[1..2] of String;
  i, Cursor, TokenIndex: SmallInt;
  OutRec: CommandRec;
begin
  try
    //Parse the string for the scene number and command string
    if (Length(CommandIn) > 3) then
    begin
      //Parse for tokens
      for i := 1 to 2 do Tokens[i] := '';
      Cursor := 1;
      TokenIndex := 1;
      repeat
        if (CommandIn[Cursor] = '|') then
        begin
          Inc(Cursor);
          Inc(TokenIndex);
        end;
        Tokens[TokenIndex] := Tokens[TokenIndex] + CommandIn[Cursor];
        Inc(Cursor);
      until (Cursor > Length(CommandIn));
    end;
    //Set output record
    OutRec.Dwell := StrToIntDef(Tokens[1], 3000);
    OutRec.Command := Tokens[2];
    ParseCommandData := OutRec;
  except

  end;
end;

//Function to return the Scene Director name for a given template command
function TEngineInterface.GetSceneDirectorNameForAction (ActionType, CommandType: SmallInt): String;
var
  i: SmallInt;
  TempTemplateCommandRecPtr: ^TemplateCommandRec;
  OutVal: String;
begin
  OutVal := '';
  //Substitute the field update into the command value of the update fields command in the command collection
  if (TempTemplateCommand_Collection.Count > 1) then
  begin
    for i := 0 to TempTemplateCommand_Collection.Count-1 do
    begin
      TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
      if (TempTemplateCommandRecPtr^.ActionType = ActionType) and (TempTemplateCommandRecPtr^.CommandType = CommandType) then
        OutVal := TempTemplateCommandRecPtr^.SceneDirectorName;
    end;
  end;
  GetSceneDirectorNameForAction := OutVal;
end;

//Function to get total delay for all commands in queue
function TEngineInterface.GetTimeDurationForQueue: SmallInt;
var
  i: SmallInt;
  CommandInRec: CommandRec;
  TotalDelay: SmallInt;
begin
  TotalDelay := 0;
  //Process all commands to get total delay
  if (QueuedCommandCollection.Count > 0) then
  begin
    for i := 0 to QueuedCommandCollection.Count-1 do
    begin
      //Parse the command
      CommandInRec := ParseCommandData(QueuedCommandCollection[i]);
      TotalDelay := TotalDelay + CommandInRec.Dwell;
    end;
  end;
  GetTimeDurationForQueue := TotalDelay;
end;

//Function to get the current state of the scene director
function TEngineInterface.GetSceneDirectorState(SceneDirectorName: String): SmallInt;
var
  i, OutVal: SmallInt;
  SceneDirectorRecPtr: ^SceneDirectorRec;
begin
  OutVal := 0;
  if (SceneDirector_Collection.Count > 0) then
  begin
    for i := 0 to SceneDirector_Collection.Count-1 do
    begin
      SceneDirectorRecPtr := SceneDirector_Collection.At(i);
      if (SceneDirectorRecPtr^.SceneDirectorName = SceneDirectorName) then
        OutVal := SceneDirectorRecPtr^.SceneDirectorState;
    end;
  end;
  GetSceneDirectorState := OutVal;
end;

//Function to update the state of the scene director in the scene director collection
procedure TEngineInterface.SetSceneDirectorState(SceneDirectorName: String; State: SmallInt; ResetAll: Boolean);
var
  i: SmallInt;
  SceneDirectorRecPtr: ^SceneDirectorRec;
begin
  if (SceneDirector_Collection.Count > 0) then
  begin
    for i := 0 to SceneDirector_Collection.Count-1 do
    begin
      SceneDirectorRecPtr := SceneDirector_Collection.At(i);
      if (SceneDirectorRecPtr^.SceneDirectorName = SceneDirectorName) then
        SceneDirectorRecPtr^.SceneDirectorState := State;
      if (ResetAll) and
         (SceneDirectorRecPtr^.SceneDirectorName <> 'SPONSOR') and
         (SceneDirectorRecPtr^.SceneDirectorName <> 'CLOCK') and
         (SceneDirectorRecPtr^.SceneDirectorName <> 'LINEUP') then
        SceneDirectorRecPtr^.SceneDirectorState := SCENE_DIRECTOR_OUT;
    end;
  end;
end;

//Function to get the current state of the action controller
function TEngineInterface.GetActionControllerState(ActionControllerID: SmallInt): SmallInt;
var
  i, OutVal: SmallInt;
  ActionControllerRecPtr: ^ActionControllerRec;
begin
  OutVal := 0;
  if (ActionController_Collection.Count > 0) then
  begin
    for i := 0 to ActionController_Collection.Count-1 do
    begin
      ActionControllerRecPtr := ActionController_Collection.At(i);
      if (ActionControllerRecPtr^.ActionControllerID = ActionControllerID) then
        OutVal := ActionControllerRecPtr^.ActionControllerState;
    end;
  end;
  GetActionControllerState := OutVal;
end;

//Procedure to get the current check string for the specified action controller
function TEngineInterface.GetTemplateFieldLastValueCheckString(FieldID: SmallInt): String;
var
  i: SmallInt;
  OutStr: String;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
begin
  OutStr := '';
  if (Temporary_Fields_Collection.Count > 0) then
  begin
    for i := 0 to Temporary_Fields_Collection.Count-1 do
    begin
      TemplateFieldsRecPtr := Temporary_Fields_Collection.At(i);
      if (TemplateFieldsRecPtr^.Field_ID = FieldID) then
        OutStr := TemplateFieldsRecPtr^.LastValueCheckString;
    end;
  end;
  GetTemplateFieldLastValueCheckString := OutStr;
end;

//Set the state of the action controller
procedure TEngineInterface.SetActionControllerState(ActionControllerID: SmallInt; State: SmallInt);
var
  i: SmallInt;
  ActionControllerRecPtr: ^ActionControllerRec;
begin
  if (ActionController_Collection.Count > 0) then
  begin
    for i := 0 to ActionController_Collection.Count-1 do
    begin
      ActionControllerRecPtr := ActionController_Collection.At(i);
      if (ActionControllerRecPtr^.ActionControllerID = ActionControllerID) then
        ActionControllerRecPtr^.ActionControllerState := State;
    end;
  end;
end;

//Procedure to set the check string for the specified action controller - used to determine if we switch A <-> B
procedure TEngineInterface.SetTemplateFieldLastValueCheckString(FieldID: SmallInt; LastValueCheckString: String);
var
  i: SmallInt;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
begin
  if (Temporary_Fields_Collection.Count > 0) then
  begin
    for i := 0 to Temporary_Fields_Collection.Count-1 do
    begin
      TemplateFieldsRecPtr := Temporary_Fields_Collection.At(i);
      if (TemplateFieldsRecPtr^.Field_ID = FieldID) then
        TemplateFieldsRecPtr^.LastValueCheckString := LastValueCheckString;
    end;
  end;
end;

//Function to update the state of the action controller in the action controller collection
function TEngineInterface.GetSceneNameFromID(SceneDirectorID: SmallInt): String;
var
  i: SmallInt;
  SceneDirectorRecPtr: ^SceneDirectorRec;
  OutStr: String;
begin
  if (SceneDirector_Collection.Count > 0) then
  begin
    for i := 0 to SceneDirector_Collection.Count-1 do
    begin
      SceneDirectorRecPtr := SceneDirector_Collection.At(i);
      if (SceneDirectorRecPtr^.SceneDirectorID = SceneDirectorID) then
        OutStr := SceneDirectorRecPtr^.SceneDirectorName;
    end;
  end;
  GetSceneNameFromID := OutStr;
end;

//Function to get the action controller name for the specified ID and state
function TEngineInterface.GetActionControllerNameFromID(ActionID, ABState: SmallInt): String;
var
  i: SmallInt;
  ActionControllerRecPtr: ^ActionControllerRec;
  OutStr: String;
begin
  if (ActionController_Collection.Count > 0) then
  begin
    for i := 0 to ActionController_Collection.Count-1 do
    begin
      ActionControllerRecPtr := ActionController_Collection.At(i);
      if (ActionControllerRecPtr^.ActionControllerID = ActionID) then
      begin
        if (ABState = ACTION_CONTROLLER_IN_A) then
          OutStr := ActionControllerRecPtr^.ActionControllerName_A
        else if (ABState = ACTION_CONTROLLER_IN_B) then
          OutStr := ActionControllerRecPtr^.ActionControllerName_B;
      end;
    end;
  end;
  GetActionControllerNameFromID := OutStr;
end;

//Procedure to delete an action controller from the temporary command collection to prevent it from running on playout
procedure TEngineInterface.DeleteActionControllersFromTempTemplateCommandCollection;
var
  i,j: SmallInt;
  TempTemplateCommandRecPtr: ^TemplateCommandRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  DeleteDirector: Boolean;
  RunningGameDirector: Boolean;
begin
  if (TempTemplateCommand_Collection.Count > 0) then
  begin
    i := 0;
    RunningGameDirector := FALSE;

    repeat
      DeleteDirector := TRUE;

      TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
      if (TempTemplateCommandRecPtr^.CommandType = COMMAND_TYPE_FIRE_ACTION_CONTROLLER) then
      begin
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            if (TempTemplateCommandRecPtr^.ActionControllerID = TemplateFieldsRecPtr^.Action_Controller_ID) and
               (TempTemplateCommandRecPtr^.CommandType = COMMAND_TYPE_FIRE_ACTION_CONTROLLER) and
               (TemplateFieldsRecPtr^.ValueHasChanged) then
              DeleteDirector := FALSE;
            //Reset flag if a game was deleted and it's the stat field
            if (RunningGameDirector) and
               ((TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_GAME_STATS_DATA) or
                (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_NCAA_GAME_STATS_DATA) or
                (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_GAME_DATA) or
                (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_NCAA_GAME_DATA)) then
            DeleteDirector := FALSE;

            //Don't delete if flagged to always play
            if (TempTemplateCommandRecPtr^.AlwaysFireActionController) then
              DeleteDirector := FALSE;
          end;
        end;
      end
      else DeleteDirector := FALSE;

      if (DeleteDirector) then
      begin
        TempTemplateCommand_Collection.AtDelete(i);
        //Set flag if game deleted
      end
      else begin
        if (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_GAME_DATA) or
           (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_NCAA_GAME_DATA) then
          RunningGameDirector := TRUE;
        inc(i);
      end;
    until (i = TempTemplateCommand_Collection.Count);
    TempTemplateCommand_Collection.Pack;
  end;
end;

//Function to get a constructed command string for playing a scene director or action controller
procedure TEngineInterface.SendPlayCommandStrings(ActionType, CommandType, SceneDirectorID, ActionControllerID, Dwell: SmallInt; ActionIsCrawl: Boolean);
var
  i, j: SmallInt;
  CommandTypeRecPtr: ^CommandTypeRec;
  ActionControllerRecPtr: ^ActionControllerRec;
  CmdStr: String;
  SceneName: String;
begin
  Case ActionType of
    //Template in initial
    ACTION_TEMPLATE_IN_INITIAL:
    begin
      Case CommandType of
        COMMAND_TYPE_FIRE_SCENE_DIRECTOR: begin
          SceneName := GetSceneNameFromID(SceneDirectorID);
          CmdStr := 'B\PL\' + Scenename  + '�1\\';
          AddCommandToQueue(CmdStr, Dwell);
          //Set updated stats
          if (GetSceneDirectorState(SceneName) = SCENE_DIRECTOR_OUT) then
          begin
            SetSceneDirectorState(SceneName, SCENE_DIRECTOR_IN_A, DONT_RESET_ALL);
          end;
        end;
        //Code here for completeness - should never fire action controller on an initial in
        COMMAND_TYPE_FIRE_ACTION_CONTROLLER: begin
          CmdStr := 'B\PL\' +  GetActionControllerNameFromID(ActionControllerID, ACTION_CONTROLLER_IN_A) + '�1\\';
          AddCommandToQueue(CmdStr, Dwell);
          SetActionControllerState(ActionControllerID, ACTION_CONTROLLER_IN_A);
        end;
      end;
    end;
  end;
end;

//Update the time of day clocks
procedure TEngineInterface.UpdateTimeOfDayClocks;
var
  CmdStr: String;
begin
  CmdStr := 'B\UP\tod_clock_txt�' +  FormatDateTime('h:nn am/pm', Now) + ' ' + TimeZoneSuffix + '\\';
  AddCommandToQueue(CmdStr, 50);
  CmdStr := 'B\UP\breaking_clock_txt�' +  FormatDateTime('h:nn am/pm', Now) + ' ' + TimeZoneSuffix + '\\';
  AddCommandToQueue(CmdStr, 50);
end;

//Procedure to update the global game status array
procedure TEngineInterface.UpdateScoreChipStatus (CurrentGameData: GameRec);
var
  i: SmallInt;
begin
  for i := 1 to 6 do
  begin
    //Init
    ScoreChipStatus[i].NewChipStatus := FALSE;
    //Set new status
    if (CurrentGameData.State = FINAL) then
    begin
      if (CurrentGameData.VScore > CurrentGameData.HScore) then
      begin
        if (CurrentGameData.VScore < 10) then
          ScoreChipStatus[1].NewChipStatus := TRUE
        else if (CurrentGameData.VScore >= 10) and (CurrentGameData.VScore < 100) then
          ScoreChipStatus[2].NewChipStatus := TRUE
        else
          ScoreChipStatus[3].NewChipStatus := TRUE;
      end
      else if (CurrentGameData.HScore > CurrentGameData.VScore) then
      begin
        if (CurrentGameData.HScore < 10) then
          ScoreChipStatus[4].NewChipStatus := TRUE
        else if (CurrentGameData.HScore >= 10) and (CurrentGameData.HScore < 100) then
          ScoreChipStatus[5].NewChipStatus := TRUE
        else
          ScoreChipStatus[6].NewChipStatus := TRUE;
      end;
    end;
  end;
end;

//Function to get a parsed token from a delited string
function GetItem(TokenNo: Integer; Data: string; Delimiter: Char = '|'): string;
var
  Token: string;
  StrLen, TEnd, TNum: Integer;
begin
  StrLen := Length(Data);
  //Init
  TNum := 1;
  TEnd := StrLen;
  //Walk through string and parse for delimiter
  while ((TNum <= TokenNo) and (TEnd <> 0)) do
  begin
    TEnd := Pos(Delimiter, Data);
    //Not last value
    if TEnd <> 0 then
    begin
      //Copy & delete data from input string; increment token counter
      Token := Copy(Data, 1, TEnd - 1);
      Delete(Data, 1, TEnd);
      Inc(TNum);
    end
    //Last value
    else begin
      Token := Data;
    end;
  end;
  //Return data
  if TNum >= TokenNo then
  begin
    Result := Token;
  end
  //Return error code
  else begin
    Result := #27;
  end;
end;

//Function to do style chip substitutions
function TEngineInterface.ProcessStyleChips(CmdStr: String): String;
var
  i,j: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  OutStr: String;
  SwitchPos: SmallInt;
begin
  //Check for presence of style chips
  //NOTE: Conversion to mixed case/upper case done in function to get field contents
  OutStr := CmdStr;
  if (StyleChip_Collection.Count > 0) AND (Length(CmdStr) >= 4) then
  begin
    for i := 0 to StyleChip_Collection.Count-1 do
    begin
      StyleChipRecPtr := StyleChip_Collection.At(i);
      //Check style chip type and substiture accordingly
      //Type 1 = Use substitution string
      if (StyleChipRecPtr^.StyleChip_Type = 1) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  StyleChipRecPtr^.StyleChip_String, [rfReplaceAll])
      //Type 2 = Use substitution font & character
      else if (StyleChipRecPtr^.StyleChip_Type = 2) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  '[f ' + IntToStr(StyleChipRecPtr^.StyleChip_FontCode) + ']' +
                  Char(StyleChipRecPtr^.StyleChip_CharacterCode), [rfReplaceAll])
    end;
  end;
  ProcessStyleChips := OutStr;
end;

// Function to take upper case string & return mixed-case string
function TEngineInterface.GetMixedCase(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := ANSILowerCase(InStr);
  if (Length(OutStr) > 0) then
  begin
    //Set first character to upper case if it's not a digit
    if (Ord(OutStr[1]) < 48) OR (Ord(OutStr[1]) > 57) then  OutStr[1] := Char(Ord(OutStr[1])-32);
    //Check if two words in state name, and set 2nd word to upper case
    if (Length(OutStr) > 2) then
    begin
      for i := 2 to Length(OutStr) do
      begin
        if (OutStr[i-1] = ' ') then OutStr[i] := Char(Ord(OutStr[i])-32);
      end;
    end;
  end;
  GetMixedCase := OutStr;
end;

//Function to get sponsor logo file name based on logo description
function TEngineInterface.GetSponsorLogoFilename(LogoName: String): String;
var
  i: SmallInt;
  OutStr: String;
  SponsorLogoPtr: ^SponsorLogoRec;
begin
  for i := 0 to SponsorLogo_Collection.Count-1 do
  begin
    SponsorLogoPtr := SponsorLogo_Collection.At(i);
    if (SponsorLogoPtr^.SponsorLogoName = LogoName) then
    begin
      OutStr := SponsorLogoPtr^.SponsorLogoFileName;
    end;
  end;
  GetSponsorLogoFilename := OutStr;
end;

//Procedure to load temporary collection for fields (for current template); also returns
//associated template information
function TEngineInterface.LoadAllTemplateInfo(TemplateID: SmallInt; TemplateActionType: SmallInt): TemplateDefsRec;
var
  i: SmallInt;
  TemplateFieldsRecPtr, NewTemplateFieldsRecPtr: ^TemplateFieldsRec;
  TemplateRecPtr: ^TemplateDefsRec;
  OutRec: TemplateDefsRec;
  FoundMatch: Boolean;
  TemplateCommandRecPtr, NewTemplateCommandRecPtr: ^TemplateCommandRec;
begin
  //Clear the existing collections
  if (TemplateActionType = ACTION_TEMPLATE_IN_INITIAL) then
  begin
    Temporary_Fields_Collection.Clear;
    Temporary_Fields_Collection.Pack;
  end;
  TempTemplateCommand_Collection.Clear;
  TempTemplateCommand_Collection.Pack;

  //First, get the engine template ID
  if (Template_Defs_Collection.Count > 0) then
  begin
    i := 0;
    FoundMatch := FALSE;
    repeat
      TemplateRecPtr := Template_Defs_Collection.At(i);
      if (TemplateID = TemplateRecPtr^.Template_ID) then
      begin
        OutRec.Template_ID := TemplateRecPtr^.Template_ID;
        OutRec.Template_Type := TemplateRecPtr^.Template_Type;
        OutRec.Template_Description := TemplateRecPtr^.Template_Description;
        OutRec.Record_Type := TemplateRecPtr^.Record_Type;
        OutRec.SceneDirectorID := TemplateRecPtr^.SceneDirectorID;
        OutRec.Default_Dwell := TemplateRecPtr^.Default_Dwell;
        OutRec.UsesGameData := TemplateRecPtr^.UsesGameData;
        OutRec.RequiredGameState := TemplateRecPtr^.RequiredGameState;
        OutRec.TemplateSponsorType := TemplateRecPtr^.TemplateSponsorType;
        FoundMatch := TRUE;
      end;
      Inc(i);
    until (FoundMatch) OR (i = Template_Defs_Collection.Count);
  end;

  //Now, get the fields for the template
  if (TemplateActionType = ACTION_TEMPLATE_IN_INITIAL) then
  begin
    for i := 0 to Template_Fields_Collection.Count-1 do
    begin
      TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
      if (TemplateFieldsRecPtr^.Template_ID = TemplateID) then
      begin
        GetMem (NewTemplateFieldsRecPtr, SizeOf(TemplateFieldsRec));
        With NewTemplateFieldsRecPtr^ do
        begin
          Template_ID := TemplateFieldsRecPtr^.Template_ID;
          Field_ID := TemplateFieldsRecPtr^.Field_ID;
          Field_Type := TemplateFieldsRecPtr^.Field_Type;
          Field_Is_Manual := TemplateFieldsRecPtr^.Field_Is_Manual;
          Field_Label := TemplateFieldsRecPtr^.Field_Label;
          Action_Controller_ID := TemplateFieldsRecPtr^.Action_Controller_ID;
          Scene_Field_Name_A := TemplateFieldsRecPtr^.Scene_Field_Name_A;
          Scene_Field_Name_B := TemplateFieldsRecPtr^.Scene_Field_Name_B;
          Field_Contents := TemplateFieldsRecPtr^.Field_Contents;
          Field_Format_Prefix := TemplateFieldsRecPtr^.Field_Format_Prefix;
          Field_Format_Suffix := TemplateFieldsRecPtr^.Field_Format_Suffix;
          Field_Max_Length := TemplateFieldsRecPtr^.Field_Max_Length;
          LastValueCheckString := '';
          ValueHasChanged := FALSE;
        end;
        if (Temporary_Fields_Collection.Count <= 100) then
        begin
          //Add to collection
          Temporary_Fields_Collection.Insert(NewTemplateFieldsRecPtr);
          Temporary_Fields_Collection.Pack;
        end;
      end;
    end;
  end;

  //Load the temporary template commands collection
  for i := 0 to TemplateCommand_Collection.Count-1 do
  begin
    TemplateCommandRecPtr := TemplateCommand_Collection.At(i);
    if (TemplateCommandRecPtr^.TemplateID = TemplateID) and (TemplateCommandRecPtr^.ActionType = TemplateActionType) then
    begin
      GetMem (NewTemplateCommandRecPtr, SizeOf(TemplateCommandRec));
      With NewTemplateCommandRecPtr^ do
      begin
        TemplateID := TemplateCommandRecPtr^.TemplateID;
        ActionID := TemplateCommandRecPtr^.ActionID;
        CommandID := TemplateCommandRecPtr^.CommandID;
        ActionType := TemplateCommandRecPtr^.ActionType;
        ActionDescription := TemplateCommandRecPtr^.ActionDescription;
        CommandDescription := TemplateCommandRecPtr^.CommandDescription;
        CommandType := TemplateCommandRecPtr^.CommandType;
        SceneDirectorID := TemplateCommandRecPtr^.SceneDirectorID;
        SceneDirectorName := TemplateCommandRecPtr^.SceneDirectorName;
        ActionControllerID := TemplateCommandRecPtr^.ActionControllerID;
        AlwaysFireActionController := TemplateCommandRecPtr^.AlwaysFireActionController;
        ActionController_A := TemplateCommandRecPtr^.ActionController_A;
        ActionController_B := TemplateCommandRecPtr^.ActionController_B;
        ActionIsCrawl := TemplateCommandRecPtr^.ActionIsCrawl;
        Delay := TemplateCommandRecPtr^.Delay;
      end;
      if (TempTemplateCommand_Collection.Count <= 100) then
      begin
        //Add to collection
        TempTemplateCommand_Collection.Insert(NewTemplateCommandRecPtr);
        TempTemplateCommand_Collection.Pack;
      end;
    end;
  end;

  LoadAllTemplateInfo := OutRec;
end;

//Utility function to strip off any ASCII characters above ASCII 127
function StripPrefix(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := '';
  if (Length(InStr) > 0) then
  begin
    for i := 1 to Length(InStr) do
    begin
      if (Ord(InStr[i]) <=127) then OutStr := OutStr + InStr[i];
    end;
  end;
  StripPrefix := OutStr;
end;


//FOR ODDS PAGES
const
  DELIMITER:STRING = Chr(160);

////////////////////////////////////////////////////////////////////////////////
// ENGINE COMMAND STRING FORMATTING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Function to get & return text strings to be used for line-ups
function TEngineInterface.GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
var
  i,j: SmallInt;
  OutRec: LineupText;
  TickerRecPtrCurrent, TickerRecPtrPrevious,TickerRecPtrNext: ^TickerRec;
  PreviousEntryIndex, NextEntryIndex: SmallInt;
  CurrentEntryLeague, PreviousEntryLeague, NextEntryLeague: String;
  HeaderIndex, CollectionItemCounter: SmallInt;
  OneLeagueOnly: Boolean;
  FirstLeague: String;
  CurrentLineupTickerEntryIndex: SmallInt;
begin
  //Always set flag to do line-up
  OutRec.UseLineup := TRUE;

  //Get text for line-ups
  if (OutRec.UseLineup = TRUE) AND (CurrentTickerEntryIndex <> NOENTRYINDEX) then
  begin
    //Init
    HeaderIndex := 1;
    CollectionItemCounter := 1;

    //Get current entry types
    CurrentLineupTickerEntryIndex := CurrentTickerEntryIndex;
    TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);

    //Make sure record is enabled and within time window
    j := 0;
    While ((TickerRecPtrCurrent^.Enabled = FALSE) OR (TickerRecPtrCurrent^.StartEnableDateTime > Now) OR
          (TickerRecPtrCurrent^.EndEnableDateTime <= Now) OR (TickerRecPtrCurrent^.League = 'NONE')) AND
          (j < Ticker_Collection.Count) do
    begin
      Inc(CurrentLineupTickerEntryIndex);
      if (CurrentLineupTickerEntryIndex = Ticker_Collection.Count) then
      begin
        CurrentLineupTickerEntryIndex := 0;
      end;
      TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);
      Inc(j);
    end;

    //Special case to just show MLB for all MLB leagues
    CurrentEntryLeague :=  TickerRecPtrCurrent^.League;
    if (CurrentEntryLeague = 'AL') OR (CurrentEntryLeague = 'NL') OR
       (CurrentEntryLeague = 'ML') OR (CurrentEntryLeague = 'IL') then
      CurrentEntryLeague := 'MLB';

    //Do subsitutions for Fantasy and Leaders
    if (TickerRecPtrCurrent^.Template_ID >= FantasyStartTemplate) and
       (TickerRecPtrCurrent^.Template_ID <= FantasyEndTemplate) then
      CurrentEntryLeague := 'FANTASY'
    else if (TickerRecPtrCurrent^.Template_ID >= LeadersStartTemplate) and
       (TickerRecPtrCurrent^.Template_ID <= LeadersEndTemplate) then
      CurrentEntryLeague := 'LEADERS';

    NextEntryIndex := CurrentLineupTickerEntryIndex+1;
    //Get type of next entry
    if (NextEntryIndex = Ticker_Collection.Count) then NextEntryIndex := 0;

    //Set first entry
    OutRec.TextFields[HeaderIndex] := CurrentEntryLeague;

    //Get remaining entries
    repeat
      TickerRecPtrNext := Ticker_Collection.At(NextEntryIndex);
      NextEntryLeague := TickerRecPtrNext^.League;
      if (NextEntryLeague = 'AL') OR (NextEntryLeague = 'NL') OR (NextEntryLeague = 'IL') OR (NextEntryLeague = 'ML') then
        NextEntryLeague := 'MLB';

      //Do subsitutions for Fantasy and Leaders
      if (TickerRecPtrNext^.Template_ID >= FantasyStartTemplate) and
         (TickerRecPtrNext^.Template_ID <= FantasyEndTemplate) then
        NextEntryLeague := 'FANTASY'
      else if (TickerRecPtrNext^.Template_ID >= LeadersStartTemplate) and
         (TickerRecPtrNext^.Template_ID <= LeadersEndTemplate) then
        NextEntryLeague := 'LEADERS';

      //If new league or child to prior parent, set text field; otherwise, continue iterating through
      if (NextEntryLeague <> CurrentEntryLeague) AND
         (TickerRecPtrNext^.Enabled = TRUE) AND (TickerRecPtrNext^.StartEnableDateTime <= Now) AND
         (TickerRecPtrNext^.EndEnableDateTime > Now) AND (j < Ticker_Collection.Count) AND
         (TickerRecPtrNext^.League <> 'NONE') then
      begin
        Inc(HeaderIndex);
        //Use parent league for field and clear flag
        OutRec.TextFields[HeaderIndex] := NextEntryLeague;
        CurrentEntryLeague := NextEntryLeague;
      end;
      if (NextEntryIndex = Ticker_Collection.Count-1) then NextEntryIndex := 0
      else Inc(NextEntryIndex);
      Inc(CollectionItemCounter);
    until (HeaderIndex = 6) OR (CollectionItemCounter = Ticker_Collection.Count*3);

    //Repeat fields if not enough after iterating
    if (HeaderIndex = 1) then
    begin
      OutRec.TextFields[2] :=  OutRec.TextFields[1];
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
      OutRec.TextFields[5] :=  OutRec.TextFields[1];
      OutRec.TextFields[6] :=  OutRec.TextFields[1];
    end
    else if (HeaderIndex = 2) then
    begin
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[2];
      OutRec.TextFields[5] :=  OutRec.TextFields[1];
      OutRec.TextFields[6] :=  OutRec.TextFields[2];
    end
    else if (HeaderIndex = 3) then
    begin
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
      OutRec.TextFields[5] :=  OutRec.TextFields[2];
      OutRec.TextFields[6] :=  OutRec.TextFields[3];
    end
    else if (HeaderIndex = 4) then
    begin
      OutRec.TextFields[5] :=  OutRec.TextFields[1];
      OutRec.TextFields[6] :=  OutRec.TextFields[2];
    end
    else if (HeaderIndex = 5) then
    begin
      OutRec.TextFields[6] :=  OutRec.TextFields[1];
    end;
  end
  else
    for i := 1 to 6 do OutRec.TextFields[i] := ' ';
  //Return
  for i := 1 to 6 do if (OutRec.TextFields[i] = '') then OutRec.TextFields[i] := ' ';
  GetLineupText := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to trigger current graphic
procedure TEngineInterface.TickerOut;
var
  i: SmallInt;
  CmdStr: String;
  TemplateCommandRecPtr: ^TemplateCommandRec;
  ActionControllerRecPtr: ^ActionControllerRec;
  SceneDirectorRecPtr: ^SceneDirectorRec;
begin
  //Send command to reload scene
  CmdStr := 'B\OP\Scene1\\';
  //Add command to queue
  AddCommandToQueue(CmdStr, 50);

  //Set last template and scene director IDs - used to reset on next template
  LastTemplateID := -1;
  LastSceneDirectorID := -1;

  //Clear state of all scene directors
  if (SceneDirector_Collection.Count > 0) then
  begin
    for i := 0 to SceneDirector_Collection.Count-1 do
    begin
      SceneDirectorRecPtr := SceneDirector_Collection.At(i);
      SceneDirectorRecPtr^.SceneDirectorState := SCENE_DIRECTOR_OUT;
    end;
  end;

  //Clear state and check string for all action controllers
  if (ActionController_Collection.Count > 0) then
  begin
    for i := 0 to ActionController_Collection.Count-1 do
    begin
      ActionControllerRecPtr := ActionController_Collection.At(i);
      ActionControllerRecPtr^.ActionControllerState := ACTION_CONTROLLER_OUT;
      //ActionControllerRecPtr^.LastValueCheckString := '';
    end;
  end;

  //Clear flag
  InitialDataTemplateFired := FALSE;

  //Disable packet timeout timer
  TickerPacketTimeoutTimer.Enabled := FALSE;
end;

//FUNCTIONS FOR ADVANCING RECORDS
//Handler for packet requesting more data for ticker
procedure TEngineInterface.EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
var
  Data: String;
  i: SmallInt;
  CurrentTemplateIDNum: SmallInt;
  CurrentTemplateIDNumStr: String;
  RequiredDelay: SmallInt;
  TempTemplateCommandRecPtr: ^TemplateCommandRec;
  CmdStr: String;
begin
  //Get the data
  Data := Socket.ReceiveText;

  //Light the indicator
  MainForm.ApdStatusLight2.Lit := TRUE;
  //Light ACK received indicator
  PacketIndicatorTimer.Enabled := TRUE;

  //Check if data needs to crawl - fire animation controller if it does
  if (Pos('1', Data) > 0) then
  begin
    //Fire scene director for crawl
    for i := 0 to TempTemplateCommand_Collection.Count-1 do
    begin
      TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
      if (TempTemplateCommandRecPtr^.CommandType = COMMAND_TYPE_FIRE_ACTION_CONTROLLER) and
         (TempTemplateCommandRecPtr^.ActionIsCrawl = TRUE) then
      begin
        //Add command to queue
        SendPlayCommandStrings(TempTemplateCommandRecPtr^.ActionType, TempTemplateCommandRecPtr^.CommandType,
          TempTemplateCommandRecPtr^.SceneDirectorID, TempTemplateCommandRecPtr^.ActionControllerID, TempTemplateCommandRecPtr^.Delay, TempTemplateCommandRecPtr^.ActionIsCrawl);
      end;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// DISPLAY DWELL TIMERS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Handler for timer to send next command
procedure TEngineInterface.TickerCommandDelayTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) then
  begin
    //Prevent retrigger
    TickerCommandDelayTimer.Enabled := FALSE;
    //Send next record
    SendTickerRecord(TRUE, 0);
  end;
end;
//Handler for 1 mS timer used to send next command when jumping over stats headers
procedure TEngineInterface.JumpToNextTickerRecordTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) then
  begin
    //Prevent retrigger
    JumpToNextTickerRecordTimer.Enabled := FALSE;
    //Send next record
    SendTickerRecord(TRUE, 0);
  end;
end;

//Function to check the ticker collection for any sponsor logo with a valid time window
function TEngineInterface.CheckForActiveSponsorLogo: Boolean;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  OutVal: Boolean;
begin
  //Init
  OutVal := FALSE;
  //Check collection for any sponsor logo with a valid time window
  if (Ticker_Collection.Count > 0) then
  begin
    for i := 0 to Ticker_Collection.Count-1 do
    begin
      TickerRecPtr := Ticker_Collection.At(i);
      if ((TickerRecPtr^.Template_ID = ONE_LINE_SPONSOR_TAGLINE) OR
         (TickerRecPtr^.Template_ID = ONE_LINE_SPONSOR_FULL_PAGE) OR
         (TickerRecPtr^.Template_ID = TWO_LINE_SPONSOR_TAGLINE) OR
         (TickerRecPtr^.Template_ID = TWO_LINE_SPONSOR_FULL_PAGE))
      AND (TickerRecPtr^.StartEnableDateTime <= Now) AND
         (TickerRecPtr^.EndEnableDateTime > Now) then OutVal := TRUE;
    end;
  end;
  CheckForActiveSponsorLogo := OutVal;
end;

//Procedure to add a command to the queue
procedure TEngineInterface.AddCommandToQueue(CmdStr: String; DwellTime: SmallInt);
var
  StartTimer: Boolean;
begin
  //Send or queue command
  if (QueuedCommandCollection.Count = 0) then StartTimer := TRUE
  else StartTimer := FALSE;

  //Dwell to front of command - will be parsed out on playout; prevent 0 mS timer interval
  if (DwellTime = 0) then DwellTime := 10;
  CmdStr := IntToStr(DwellTime) + '|' + CmdStr;
  //Add command to the queue
  QueuedCommandCollection.Add(CmdStr);

  //If first command in list, fire the command timer
  if (StartTimer) then
  begin
    CommandDwellTimer.Interval := 1;
    CommandDwellTimer.Enabled := TRUE;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Procedure to send next record after receipt of packet
procedure TEngineInterface.SendTickerRecord(NextRecord: Boolean; RecordIndex: SmallInt);
var
  i,j,k,m: SmallInt;
  TempStr, CmdStr: String;
  TickerRecPtr, TickerRecPtrNew: ^TickerRec;
  FoundGame: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  IsLast: Boolean;
  OverlayIndex, OverlayIndexNext: SmallInt;
  IsNewOverlay, PadText: Boolean;
  InGameStatsRequired: Boolean;
  FoundGameInDatamart: Boolean;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentGameData: GameRec;
  OkToGo, OKToDisplay: Boolean;
  TokenCounter: SmallInt;
  UpdateCmdStr: String;
  SymbolValueData: SymbolValueRec;
  TempTemplateCommandRecPtr: ^TemplateCommandRec;
begin
  //Init flags
  OKToDisplay := TRUE;
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  //Init FoundGame flag - used to prevent inadverant commands from being sent if game not found
  FoundGame := TRUE;
  RecordIsSponsor := FALSE;

  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;

  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Init
    IsLast := FALSE;

    //Make sure we haven't dumped put
    if ((EngineParameters.Enabled) AND (TickerAbortFlag = FALSE) AND (Ticker_Collection.Count > 0)) then
    begin
      OKToGo := TRUE;

      //Triggering specific record, so set current entry index
      CurrentTickerEntryIndex := RecordIndex;
      //Disble command timer
      DisableCommandTimer := TRUE;

      //Proceed if not at end or in looping mode and at beggining
      if (CurrentTickerEntryIndex <= Ticker_Collection.Count-1) AND (OKToGo) then
      begin
        //Get pointer to current record
        TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR TICKER FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        TemplateInfo := LoadAllTemplateInfo(TickerRecPtr^.Template_ID, ACTION_TEMPLATE_IN_INITIAL);

        //Get game data if template requires it
        if (TemplateInfo.UsesGameData) then
        begin
          try
            //Use full game data
            CurrentGameData := GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to get game data for ticker');
          end;
          FoundGame := CurrentGameData.DataFound;
        end;

        //Set the default dwell time; set packet timeout interval
        //If lower-right sponsor template, sponsor logo dwell does not need to be specified
        if (TemplateInfo.TemplateSponsorType = 1) then
        begin
          //Set the current sponsor logo name, template, taglines & dwell for the lower-right sponsor logo
          CurrentSponsorInfo.CurrentSponsorLogoName := TickerRecPtr^.SponsorLogo_Name;
          CurrentSponsorInfo.CurrentSponsorLogoDwell := TickerRecPtr^.SponsorLogo_Dwell;
          CurrentSponsorInfo.CurrentSponsorTemplate := TickerRecPtr^.Template_ID;
        end
        //If clear lower-right sponsor template, clear the name
        else if (TemplateInfo.TemplateSponsorType = 3) then
        begin
          //Set the current sponsor logo name, template, taglines & dwell for the lower-right sponsor logo
          CurrentSponsorInfo.CurrentSponsorLogoName := '';
          CurrentSponsorInfo.CurrentSponsorLogoDwell := 0;
          CurrentSponsorInfo.CurrentSponsorTemplate := TickerRecPtr^.Template_ID;
        end;

        //////////////////////////////////////////////////////////////////////
        // Initial setup - trigger ticker and line-up in
        //////////////////////////////////////////////////////////////////////

        CmdStr := 'B\OP\Scene1\\';
        //Add command to queue
        if (UseLongDelaysForPlayout) then
          AddCommandToQueue(CmdStr, 100)
        else
          AddCommandToQueue(CmdStr, 500);

        if not (UseLongDelaysForPlayout) then
        begin
          //Start of command processing
          //Send command to fade content out
          CmdStr := 'B\PL\CONTENT_ON�1\\';
          //Add command to queue
          AddCommandToQueue(CmdStr, 50);
        end;

        //Trigger ticker in
        CmdStr := 'B\PL\TICKER�1\\';
        //Add command to queue
        if (UseLongDelaysForPlayout) then
          AddCommandToQueue(CmdStr, 1500)
        else
          AddCommandToQueue(CmdStr, 50);

        //Fire line-up text
        LineupData := GetLineuptext(CurrentTickerEntryIndex, TickerDisplayMode);
        if (LineupData.UseLineup = TRUE) then
        begin
          CmdStr := 'B\UP\prime_header_txt�' + LineupData.TextFields[1] +
            '\header_2_txt�' + LineupData.TextFields[2] +
            '\header_3_txt�' + LineupData.TextFields[3] +
            '\header_4_txt�' + LineupData.TextFields[4] +
            '\header_5_txt�' + LineupData.TextFields[5] +
            '\header_6_txt�' + LineupData.TextFields[6] +
            '\\';
        end;
        //Add command to queue
        if (UseLongDelaysForPlayout) then
          AddCommandToQueue(CmdStr, 500)
        else
          AddCommandToQueue(CmdStr, 50);

        //If lower-right sponsor template, bring sponsor in/out then skip to next record - will bring data in then
        if (TemplateInfo.TemplateSponsorType = TEMPLATE_SPONOSR_TYPE_IN) and (CurrentSponsorInfo.CurrentSponsorLogoName <> '') then
        begin
          //Set sponsor logo
          CmdStr := 'B\MT\SPONSOR_IMG�' + CurrentSponsorInfo.CurrentSponsorLogoName + '\\';
          AddCommandToQueue(CmdStr, 100);

          //Bring sponsor in if it was out
          //if (GetSceneDirectorState('SPONSOR') = SCENE_DIRECTOR_OUT) then
          begin
            CmdStr := 'B\PL\SPONSOR�1\\';
            AddCommandToQueue(CmdStr, 100);
            //Update the state
            SetSceneDirectorState('SPONSOR', SCENE_DIRECTOR_IN_A, DONT_RESET_ALL)
          end;

          //Trigger line-up
          CmdStr := 'B\PL\LINEUP�1\\';
          //Add command to queue
          if (UseLongDelaysForPlayout) then
            //AddCommandToQueue(CmdStr, 500)
          else
            //AddCommandToQueue(CmdStr, 100);
          //SetSceneDirectorState('LINEUP', SCENE_DIRECTOR_IN_A, DONT_RESET_ALL);

          //Exit and send next record
          //JumpToNextTickerRecordTimer.Enabled := TRUE;
          Exit;
        end
        else if (TemplateInfo.TemplateSponsorType = TEMPLATE_SPONOSR_TYPE_OUT) then
        begin
          //Take out sponsor in if it was in
          //if (GetSceneDirectorState('SPONSOR') = SCENE_DIRECTOR_IN_A) then
          begin
            //Take sponsor out
            CmdStr := 'B\PL\SPONSOR�1\\';
            AddCommandToQueue(CmdStr, 50);
            //Update the state
            SetSceneDirectorState('SPONSOR', SCENE_DIRECTOR_OUT, DONT_RESET_ALL)
          end;

          //Trigger line-up
          CmdStr := 'B\PL\LINEUP�1\\';
          //Add command to queue
          if (UseLongDelaysForPlayout) then
            AddCommandToQueue(CmdStr, 500)
          else
            AddCommandToQueue(CmdStr, 100);
          SetSceneDirectorState('LINEUP', SCENE_DIRECTOR_IN_A, DONT_RESET_ALL);

          //Exit and send next record
          //JumpToNextTickerRecordTimer.Enabled := TRUE;
          Exit;
        end
        else begin
          //Trigger line-up
          CmdStr := 'B\PL\LINEUP�1\\';
          //Add command to queue
          if (UseLongDelaysForPlayout) then
            AddCommandToQueue(CmdStr, 500)
          else
            AddCommandToQueue(CmdStr, 100);
          SetSceneDirectorState('LINEUP', SCENE_DIRECTOR_IN_A, DONT_RESET_ALL);
        end;

        //Start processing the data for the main template update
        //Add the fields from the temporary fields collection
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          //Start building command string
          UpdateCmdStr := 'B\UP\';
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            //Version 2.1
            //Check for single fields (not compound fields required for 2-line, new look engine)
            if (TemplateFieldsRecPtr^.Field_Type > 0) then
            begin
              //Filter out fields ID values of -1 => League designator
              //if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                begin
                  TempStr := TemplateFieldsRecPtr^.Field_Contents;
                  //Add previx and/or suffix if applicable
                  if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                  if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                end
                //It's a symbolic name, so get the field contents
                else begin
                  try
                    SymbolValueData := GetValueOfSymbol(TICKER, TemplateFieldsRecPtr^.Field_Contents,
                      CurrentTickerEntryIndex, CurrentGameData, TickerDisplayMode);
                    TempStr := SymbolValueData.SymbolValue;
                    //If data not received, skip record
                    if (SymbolValueData.OKToDisplay = FALSE) then
                    begin
                      JumpToNextTickerRecordTimer.Enabled := TRUE;
                      Exit;
                    end;

                    //Add previx and/or suffix if applicable
                    if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                    if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                  end;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                end;
              end;
            end;
            //Add name\value pair to command string - always start with "A" side
            UpdateCmdStr :=  UpdateCmdStr + TemplateFieldsRecPtr^.Scene_Field_Name_A + '�' + TempStr;
            if (j < Temporary_Fields_Collection.Count-1) then
              UpdateCmdStr :=  UpdateCmdStr + '\';
          end;
          //Close the command
          UpdateCmdStr := UpdateCmdStr + '\\';
        end;

        //Substitute style chip codes
        UpdateCmdStr := ProcessStyleChips(UpdateCmdStr);

        //Send the commands
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
        begin
          try
            if (TempTemplateCommand_Collection.Count > 0) then
            begin
              //Update the game status for the winner chips
              UpdateScoreChipStatus(CurrentGameData);

              for i := 0 to TempTemplateCommand_Collection.Count-1 do
              begin
                TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
                Case TempTemplateCommandRecPtr^.CommandType of
                  COMMAND_TYPE_UPDATE_TEMPLATE_FIELDS: begin
                    //Add command to queue
                    AddCommandToQueue(UpdateCmdStr, TempTemplateCommandRecPtr^.Delay);

                    //Now, bring in the winner chip if applicable
                    if (TemplateInfo.SceneDirectorID = SCENE_DIRECTOR_ID_GAME_STATS) or (TemplateInfo.SceneDirectorID = SCENE_DIRECTOR_ID_NCAA_GAME_STATS) then
                    for m := 1 to 6 do
                    begin
                      //Now, bring in the new chip
                      //if (ScoreChipStatus[m].CurrentChipStatus <> ScoreChipStatus[m].NewChipStatus) and
                      //   (ScoreChipStatus[m].NewChipStatus = TRUE) then
                      if (ScoreChipStatus[m].NewChipStatus = TRUE) then
                      begin
                        //Add command to queue
                        if (UseLongDelaysForPlayout) then
                          AddCommandToQueue ('<DELAY>', 1000)
                        else
                          AddCommandToQueue ('<DELAY>', 500);
                        AddCommandToQueue ('B\PL\' + ScoreChipStatus[m].ChipDirectorName + '�1\\', 50);
                      end;
                      //Set updated status
                      //ScoreChipStatus[m].CurrentChipStatus := ScoreChipStatus[m].NewChipStatus;
                      //ScoreChipStatus[m].NewChipStatus := FALSE;
                    end;
                    for m := 1 to 6 do ScoreChipStatus[m].NewChipStatus := FALSE;
                  end;
                  COMMAND_TYPE_FIRE_SCENE_DIRECTOR: begin
                    //Add command to queue
                    SendPlayCommandStrings(TempTemplateCommandRecPtr^.ActionType, TempTemplateCommandRecPtr^.CommandType,
                      TempTemplateCommandRecPtr^.SceneDirectorID, TempTemplateCommandRecPtr^.ActionControllerID, TempTemplateCommandRecPtr^.Delay, TempTemplateCommandRecPtr^.ActionIsCrawl);
                    //Set flag to indicate that the first template with data has fired
                    InitialDataTemplateFired := TRUE;
                  end;
                end;
              end;

              //Get the total time required to clear the queue before dwell and pad dwell timer
              //TickerCommandDelayTimer.Interval := TickerCommandDelayTimer.Interval + GetTimeDurationForQueue;

              //Set last template ID - used to reset on next template
              LastTemplateID := TemplateInfo.Template_ID;
              LastSceneDirectorID := TemplateInfo.SceneDirectorID;
            end;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying send ticker command to engine');
          end;
          //Enable packet timeout timer
          TickerPacketTimeoutTimer.Enabled := FALSE;
          TickerPacketTimeoutTimer.Enabled := TRUE;
          //Start timer to send next record
          //if (USEDATAPACKET = FALSE) then TickerCommandDelayTimer.Enabled := TRUE;
        end;
      end;
    end;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send additional records to ticker');
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// SOCKET EVENT HANDLES
////////////////////////////////////////////////////////////////////////////////
//Handler for packet re-enable timer
procedure TEngineInterface.PacketEnableTimerTimer(Sender: TObject);
begin
  //Disable to prevent retrigger
  PacketEnableTimer.Enabled := FALSE;
  //Re-enable packets
  PacketEnable := TRUE;
end;

//Handler to turn off packet recevied indicator after 250 mS
procedure TEngineInterface.PacketIndicatorTimerTimer(Sender: TObject);
begin
  PacketIndicatorTimer.Enabled := FALSE;
  MainForm.ApdStatusLight2.Lit := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// PACKET TIMEOUT TIMERS
////////////////////////////////////////////////////////////////////////////////
//Handler for ticker packet timeout timer - sends next command if status command not
//received
procedure TEngineInterface.TickerPacketTimeoutTimerTimer(Sender: TObject);
begin
  if (EngineParameters.Enabled) then
  begin
    //Disable timer to prevent retriggering
    TickerPacketTimeoutTimer.Enabled := FALSE;
    //Enable delay timer
    if (RunningTicker = TRUE) then
      //Send next record
      SendTickerRecord(TRUE, 0);
    //Log the timeout
    if (ErrorLoggingEnabled = True) then
    begin
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('A timeout occurred while waiting for a Main Ticker command return status from the graphics engine.');
    end;
  end;
end;

//Handler to light indicator
procedure TEngineInterface.EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := TRUE;
  SocketConnected := TRUE;
end;

//Handler to turn off indicator
procedure TEngineInterface.EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine');
  end;
end;

//Handler for socket error
procedure TEngineInterface.EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  ErrorCode := 0;
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine');
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// LOGGING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToErrorLog (ErrorString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   ErrorLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists('c:\TSTN_Error_LogFiles') = FALSE) then CreateDir('c:\TSTN_Error_LogFiles');
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   ErrorLogStr := TimeStr + '    ' + DateStr + '    ' + ErrorString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := 'c:\TSTN_Error_Logfiles\TSTNErrorLog' + DayStr + '.txt';
   AssignFile (ErrorLogFile, FileName);
   //Write error information to one line in file
   //If file doesn't exist create a new one. Otherwise, check to see if it's one
   //month old. If so, delete & create a new one. Otherwise, append error to file
   if (Not (FileExists(FileName))) then
   begin
      ReWrite (ErrorLogFile);
      try
         Writeln (ErrorLogFile, ErrorLogStr);      finally         CloseFile (ErrorLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (ErrorLogFile);      try         Readln (ErrorLogFile, TestStr);      finally         CloseFile (ErrorLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (ErrorLogFile);         {Create a new logfile}         ReWrite (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end;   end;end;

// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToAsRunLog (AsRunString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   AsRunLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists(AsRunLogFileDirectoryPath) = FALSE) then
   begin
     if (DirectoryExists('c:\TSTN_AsRun_LogFiles') = FALSE) then
       CreateDir('c:\TSTN_AsRun_LogFiles');
     DirectoryStr := 'c:\TSTN_AsRun_LogFiles\';
   end
   else DirectoryStr := AsRunLogFileDirectoryPath;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   AsRunLogStr := TimeStr + '    ' + DateStr + '    ' + AsRunString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'TSTNAsRunLog' + DayStr + '.txt';
   AssignFile (AsRunLogFile, FileName);
   {If (FileExists (ErrorLogFile)) then Erase (ErrorLogFile);}
   {Write error information to one line in file}
   {If file doesn't exist create a new one. Otherwise, check to see if it's one }
   {month old. If so, delete & create a new one. Otherwise, append error to file}
   if (Not (FileExists(FileName))) then begin
      ReWrite (AsRunLogFile);
      try
         Writeln (AsRunLogFile, AsRunLogStr);      finally         CloseFile (AsRunLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (AsRunLogFile);      try         Readln (AsRunLogFile, TestStr);      finally         CloseFile (AsRunLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then      begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (AsRunLogFile);         {Create a new logfile}         ReWrite (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end;   end;end;

//Handler for command dwell timer
procedure TEngineInterface.CommandDwellTimerTimer(Sender: TObject);
var
  CommandInRec: CommandRec;
begin
  //Disable to prevent re-trigger
  CommandDwellTimer.Enabled := FALSE;

  //Process the next command if there is one
  if (QueuedCommandCollection.Count > 0) then
  begin
    //Parse the command
    CommandInRec := ParseCommandData(QueuedCommandCollection[0]);

    //Set flag if it was the lineup so that we hold off a ticker out
    if (Pos('LINEUP', CommandInRec.Command) > 0) then
      LastCommandSentWasLineup := TRUE
    else
      LastCommandSentWasLineup := FALSE;

    //For debug
    //MainForm.Memo1.Lines.Add(FormatDateTime('hh:nn:ss.zzz', Now) + ': CommandSent: '  + CommandInRec.Command);
    //MainForm.Memo1.Lines.Add('');

    //If it's just a delay, don't send the command - just delay
    if (CommandInRec.Command <> '<DELAY>') then
      SendCommand(CommandInRec.Command);

    //Delete the command from the top of stack
    QueuedCommandCollection.Delete(0);

    //Setup & enable timer
    CommandDwellTimer.Interval := CommandInRec.Dwell;
    CommandDwellTimer.Enabled := TRUE;
  end;
end;

end.


