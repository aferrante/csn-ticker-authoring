unit ManualGameEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, Buttons, ExtCtrls, Mask, ComCtrls;

type
  TManualGameDlg = class(TForm)
    ManualGameEditPanel: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    VisitorScore: TSpinEdit;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    HomeScore: TSpinEdit;
    Label10: TLabel;
    Label11: TLabel;
    GamePhase: TComboBox;
    Label12: TLabel;
    ManualGamesOverrideEnable: TCheckBox;
    VisitorName: TEdit;
    HomeName: TEdit;
    GameState: TRadioGroup;
    EndOfPeriod: TCheckBox;
    Panel2: TPanel;
    Label21: TLabel;
    SelectedGameLabel: TLabel;
    StartTime: TDateTimePicker;
    StartTimeLabel: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ManualGameDlg: TManualGameDlg;

implementation

{$R *.DFM}

end.
