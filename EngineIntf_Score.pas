// Rev: 03/09/06  18:40  M. Dilworth  Video Design Software Inc.
unit EngineIntf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OoMisc, ADTrmEmu, AdPacket, AdPort, AdWnPort, ExtCtrls, Globals, FileCtrl;

type
  TEngineInterface = class(TForm)
    ApdWinsockPort1: TApdWinsockPort;
    ApdDataPacket1: TApdDataPacket;
    AdTerminal1: TAdTerminal;
    PacketEnableTimer: TTimer;
    TickerCommandDelayTimer: TTimer;
    PacketIndicatorTimer: TTimer;
    TickerPacketTimeoutTimer: TTimer;
    JumpToNextTickerRecordTimer: TTimer;
    BugCommandDelayTimer: TTimer;
    ExtraLineCommandDelayTimer: TTimer;
    JumpToNextBugRecordTimer: TTimer;
    JumpToNextExtraLineRecordTimer: TTimer;
    DisplayClockTimer: TTimer;
    BugPacketTimeoutTimer: TTimer;
    ExtraLinePacketTimeoutTimer: TTimer;
    procedure ApdWinsockPort1WsConnect(Sender: TObject);
    procedure ApdWinsockPort1WsDisconnect(Sender: TObject);
    procedure ApdWinsockPort1WsError(Sender: TObject; ErrCode: Integer);
    procedure ApdDataPacket1StringPacket(Sender: TObject; Data: String);
    procedure PacketEnableTimerTimer(Sender: TObject);
    procedure TickerCommandDelayTimerTimer(Sender: TObject);
    procedure PacketIndicatorTimerTimer(Sender: TObject);
    procedure TickerPacketTimeoutTimerTimer(Sender: TObject);
    procedure JumpToNextTickerRecordTimerTimer(Sender: TObject);
    procedure WriteToErrorLog (ErrorString: String);
    procedure WriteToAsRunLog (AsRunString: String);
    procedure BugCommandDelayTimerTimer(Sender: TObject);
    procedure JumpToNextBugRecordTimerTimer(Sender: TObject);
    procedure ExtraLineCommandDelayTimerTimer(Sender: TObject);
    procedure JumpToNextExtraLineRecordTimerTimer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DisplayClockTimerTimer(Sender: TObject);
    procedure BugPacketTimeoutTimerTimer(Sender: TObject);
    procedure ExtraLinePacketTimeoutTimerTimer(Sender: TObject);
  private
    { Private declarations }
    function ProcessStyleChips(CmdStr: String): String;
  public
    { Public declarations }
    procedure SetAudioFile (AudioFileID: SmallInt);
    procedure StartTickerData;
    procedure SendTickerRecord(NextRecord: Boolean; RecordIndex: SmallInt);
    //Added for 1-line ticker support
    procedure Send2ndPageTickerRecord;
    procedure SendBugRecord(NextRecord: Boolean; RecordIndex: SmallInt);
    procedure SendExtraLineRecord(NextRecord: Boolean; RecordIndex: SmallInt);
    procedure TriggerGraphic (Mode: SmallInt);
    function GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
    function GetProgressIndicatorValue(CurrentTickerEntryIndex: SmallInt): SmallInt;
    function GetMixedCase(InStr: String): String;
    function FormatTimeString (InStr: String): String;
    function GetSponsorLogoFileName(LogoName: String): String;
    function GetTeamMnemonic (LeagueCode: String; STTeamCode: String): String;
    function GetTeamBasename (LeagueCode: String; STTeamCode: String): String;
    function GetGameState(LEAGUE: String; GTIME: String; GPHASE: SmallInt): Integer;
    function GetGameTimeString(GTimeStr: String): String;
    function GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
    function LoadTempTemplateFields(TemplateID: SmallInt): TemplateDefsRec;
    function GetValueOfSymbol(PlaylistType: SmallInt; Symbolname: String; CurrentEntryIndex: SmallInt;
      CurrentGameData: GameRec; TickerMode: SmallInt): String;
    //Function to run stored procedures to get game information
    function GetGameData (League: String; GCode: String; OddsOnly: Boolean): GameRec;
    function FormatGameClock(TimeMin: SmallInt; TimeSec: SmallInt): String;
    procedure EnableLogoClock;
    procedure DisableLogoClock;
    procedure ClearBreakingNews;
    procedure SendNextClockLogoCommand;
    procedure UpdateTickerRecordDataFromDB(PlaylistID: Double; TickerRecordIndex: SmallInt; EventGUID: TGUID);
    procedure UpdateBugRecordDataFromDB(PlaylistID: Double; BugRecordIndex: SmallInt; EventGUID: TGUID);
    function GetFootballGameSituationData(CurrentGameData: GameRec): FootballSituationRec;
    //For 1-line ticker
    function GetSingleLineTemplateInfo(TemplateID: SmallInt): OneLineTemplateInfoRec;
    function GetForecastText(Icon_Name: String): String;
  end;

const
  SOT: String[1] = #1;
  ETX: String[1] = #3;
  EOT: String[1] = #4;
  GS: String[1] = #29;
  RS: String[1] = #30;
  NAK: String[1] = #21; //Used as No-Op
  EOB: String[1] = #23; //Send at end of last record; will cause SS 11 to be sent by engine
  SingleLineWinningLarge: String[1] = #180;
  SingleLineLosingLarge: String[1]  = #181;
  SingleLineWinningSmall: String[1]  = #182;
  SingleLineLosingSmall: String[1]  = #183;

var
  EngineInterface: TEngineInterface;
  DisableCommandTimer: Boolean;
  //Boolean to select whether or not the data packet is used to trigger the next graphic

const
 UseDataPacket = TRUE;


implementation

uses Main, //Main form
     DataModule; //Data module

{$R *.DFM}
//Imit
procedure TEngineInterface.FormActivate(Sender: TObject);
begin
end;

//These are the graphics engine control procedures
////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Function to take a standard template ID and get the alternate single-line template ID
function TEngineInterface.GetSingleLineTemplateInfo(TemplateID: SmallInt): OneLineTemplateInfoRec;
var
  i: SmallInt;
  TemplateDefRecPtr: ^TemplateDefsRec;
  TemplateFound: Boolean;
  OutVal: OneLineTemplateInfoRec;
begin
   //Inits
   TemplateFound := FALSE;
   OutVal.OneLine_Template_ID := -1;
   OutVal.OneLine_SecondTemplate_Enable := FALSE;
   OutVal.OneLine_SecondTemplate_ID := 0;
   //Search for match
   if (Template_Defs_Collection.Count > 0) then
   begin
     i := 0;
     repeat
       TemplateDefRecPtr := Template_Defs_Collection.At(i);
       if (TemplateDefRecPtr^.Template_ID = TemplateID) then
       begin
         TemplateFound := TRUE;
         OutVal.OneLine_Template_ID := TemplateDefRecPtr^.OneLine_Template_ID;
         OutVal.OneLine_SecondTemplate_Enable := TemplateDefRecPtr^.OneLine_SecondTemplate_Enable;
         OutVal.OneLine_SecondTemplate_ID := TemplateDefRecPtr^.OneLine_SecondTemplate_ID;
       end;
       inc(i);
     until (TemplateFound) OR (i = Template_Defs_Collection.Count);
   end;
   GetSingleLineTemplateInfo := OutVal;
end;

//Function to do style chip substitutions
function TEngineInterface.ProcessStyleChips(CmdStr: String): String;
var
  i,j: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  OutStr: String;
  SwitchPos: SmallInt;
begin
  //Check for presence of style chips
  //NOTE: Conversion to mixed case/upper case done in function to get field contents
  OutStr := CmdStr;
  if (StyleChip_Collection.Count > 0) AND (Length(CmdStr) >= 4) then
  begin
    for i := 0 to StyleChip_Collection.Count-1 do
    begin
      StyleChipRecPtr := StyleChip_Collection.At(i);
      //Check style chip type and substiture accordingly
      //Type 1 = Use substitution string
      if (StyleChipRecPtr^.StyleChip_Type = 1) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  StyleChipRecPtr^.StyleChip_String, [rfReplaceAll])
      //Type 2 = Use substitution font & character
      else if (StyleChipRecPtr^.StyleChip_Type = 2) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  '[f ' + IntToStr(StyleChipRecPtr^.StyleChip_FontCode) + ']' +
                  Char(StyleChipRecPtr^.StyleChip_CharacterCode), [rfReplaceAll])
      //Type 7 = Don;t use title page for first page in 1-line page sequence;
      //Simply replace with null
      else if (StyleChipRecPtr^.StyleChip_Type = 7) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code, '', [rfReplaceAll]);
    end;
  end;
  ProcessStyleChips := OutStr;
end;

//Function to get a textual description of the forecast based on the text
function TEngineInterface.GetForecastText(Icon_Name: String): String;
var
  i: SmallInt;
  WeatherIconRecPtr: ^WeatherIconRec;
  OutStr: String;
begin
  OutStr := '';
  if (Weather_Icon_Collection.Count > 0) then
  begin
    for i := 0 to Weather_Icon_Collection.Count-1 do
    begin
      WeatherIconRecPtr := Weather_Icon_Collection.At(i);
      if (WeatherIconRecPtr^.Icon_Name = Icon_Name) then
        OutStr := WeatherIconRecPtr^.Forecast_Text;
    end;
  end;
  GetForecastText := OutStr;
end;

//Function to take a football game situation record and return the parsed situation data
function TEngineInterface.GetFootballGameSituationData(CurrentGameData: GameRec): FootballSituationRec;
var
  i: SmallInt;
  OutRec: FootballSituationRec;
  VTeamID, HTeamID, PossessionTeamID: Double;
  PossessionYardsFromGoal: SmallInt;
  TeamRecPtr: ^TeamRec;
  StrCursor, SubStrCounter: SmallInt;
  Substrings: Array[1..4] of String;
begin
  //Init
  HTeamID := 0;
  VTeamID := 0;
  StrCursor := 1;
  SubStrCounter := 1;
  for i := 1 to 4 do Substrings[i] := '';

  //Parse out situation string to components
  While (StrCursor <= Length(CurrentGameData.Situation)) do
  begin
    //Get substrings for data fields
    While (CurrentGameData.Situation[StrCursor] <> '-') do
    begin
      SubStrings[SubStrCounter] := SubStrings[SubStrCounter] + CurrentGameData.Situation[StrCursor];
      Inc(StrCursor);
    end;
    //Go to next field
    Inc(StrCursor);
    Inc(SubStrCounter);
  end;

  //Set values
  //Do ID of team with possession
  if (Trim(SubStrings[1]) <> '') then
  begin
    try
      PossessionTeamID := Trunc(StrToFloat(Substrings[1]));
    except
      PossessionTeamID := 0;
      WriteToErrorLog('Error occurred while trying to convert team ID for football possession');
    end;
  end
  else PossessionTeamID := 0;

  //Get yards from goal
  if (Trim(SubStrings[2]) <> '') then
  begin
    try
      PossessionYardsFromGoal := StrToInt(Substrings[2]);
    except
      PossessionYardsFromGoal := 0;
      WriteToErrorLog('Error occurred while trying to convert field position for football possession');
    end;
  end
  else PossessionYardsFromGoal := 0;

  //Get team IDs of visiting & home teams
  if (Team_Collection.Count > 0) then
  begin
    for i := 0 to Team_Collection.Count-1 do
    begin
      TeamRecPtr := Team_Collection.At(i);
      if (CurrentGameData.VMnemonic = TeamRecPtr^.ShortDisplayName1) AND
         (CurrentGameData.League = TeamRecPtr^.League) then
        VTeamID := TeamRecPtr^.StatsIncId;
    end;
    for i := 0 to Team_Collection.Count-1 do
    begin
      TeamRecPtr := Team_Collection.At(i);
      if (CurrentGameData.HMnemonic = TeamRecPtr^.ShortDisplayName1) AND
         (CurrentGameData.League = TeamRecPtr^.League) then
        HTeamID := TeamRecPtr^.StatsIncId;
    end;
  end;
  //Check to find team with possession
  if (VTeamID = PossessionTeamID) then
  begin
    OutRec.VisitorHasPossession := TRUE;
    OutRec.HomeHasPossession := FALSE;
  end
  else if (HTeamID = PossessionTeamID) then
  begin
    OutRec.VisitorHasPossession := FALSE;
    OutRec.HomeHasPossession := TRUE;
  end
  else begin
    OutRec.VisitorHasPossession := FALSE;
    OutRec.HomeHasPossession := FALSE;
  end;
  OutRec.YardsFromGoal := PossessionYardsFromGoal;
  //Set return value
  GetFootballGameSituationData := OutRec;
end;

//Function to run stored procedures to get game information
function TEngineInterface.GetGameData (League: String; GCode: String; OddsOnly: Boolean): GameRec;
var
  OutRec: GameRec;
begin
  //Check if valid league first
  if (League <> 'NONE') then
  begin
    if (OddsOnly = TRUE) then
    begin
      try
        dmMain.SportbaseQuery2.Active := FALSE;
        dmMain.SportbaseQuery2.SQL.Clear;
        dmMain.SportbaseQuery2.SQL.Add('/* */sp_Get' + League + 'OddsInfo' + ' ' + GCode);
        dmMain.SportbaseQuery2.Open;
        //Check for record count > 0 and process
        if (dmMain.SportbaseQuery2.RecordCount > 0) then
        begin
          OutRec.League := dmMain.SportbaseQuery2.FieldByName('League').AsString;
          //Extra MLB info not available in odds table
          OutRec.SubLeague := ' ';
          OutRec.DoubleHeader := FALSE;
          OutRec.DoubleHeaderGameNumber := 0;
          OutRec.HName := dmMain.SportbaseQuery2.FieldByName('HName').AsString;
          OutRec.VName := dmMain.SportbaseQuery2.FieldByName('VName').AsString;
          OutRec.HMnemonic := dmMain.SportbaseQuery2.FieldByName('HMnemonic').AsString;
          OutRec.VMnemonic := dmMain.SportbaseQuery2.FieldByName('VMnemonic').AsString;
          OutRec.HScore := 0;
          OutRec.VScore := 0;
          OutRec.VOpeningOdds := dmMain.SportbaseQuery2.FieldByName('VOpeningOdds').AsFloat;
          OutRec.HOpeningOdds := dmMain.SportbaseQuery2.FieldByName('HOpeningOdds').AsFloat;
          OutRec.OpeningTotal := dmMain.SportbaseQuery2.FieldByName('OpeningTotal').AsFloat;
          OutRec.VCurrentOdds := dmMain.SportbaseQuery2.FieldByName('VCurrentOdds').AsFloat;
          OutRec.HCurrentOdds := dmMain.SportbaseQuery2.FieldByName('HCurrentOdds').AsFloat;
          OutRec.CurrentTotal := dmMain.SportbaseQuery2.FieldByName('CurrentTotal').AsFloat;
          OutRec.Phase := -1;
          OutRec.TimeMin := 0;
          OutRec.TimeSec := 0;
          //Set the game state to 1 - required to allow odds only pages to appear
          OutRec.State := 1;
          OutRec.Count := '000';
          OutRec.Situation := 'FFF';
          OutRec.Description := OutRec.League + ': ' + OutRec.VName + ' @ ' + OutRec.HName + ' (ODDS)';
          OutRec.Date := dmMain.SportbaseQuery2.FieldByName('Date').AsDateTime;
          OutRec.StartTime := dmMain.SportbaseQuery2.FieldByName('StartTime').AsDateTime;
          OutRec.Visitor_Param1 := 0;
          OutRec.Home_Param1 := 0;
          OutRec.DataFound := TRUE;
          //If college, get ranks
          if (League = 'CFB') OR (League = 'CBK') OR (League = 'CBB') then
          begin
            OutRec.VRank := dmMain.SportbaseQuery2.FieldByName('VRank').AsString;
            OutRec.HRank := dmMain.SportbaseQuery2.FieldByName('HRank').AsString;
          end
          else begin
            OutRec.VRank := ' ';
            OutRec.HRank := ' ';
          end;
        end
        else OutRec.DataFound := FALSE;
      except
        if (ErrorLoggingEnabled = True) then
          WriteToErrorLog('Error occurred in GetGameData function for odds only');
        //Clear data found flag
        OutRec.DataFound := FALSE;
      end;
    end
    else begin
      try
        //Execute required stored procedure
        dmMain.SportbaseQuery2.Active := FALSE;
        dmMain.SportbaseQuery2.SQL.Clear;
        dmMain.SportbaseQuery2.SQL.Add('/* */sp_GetSI' + League + 'GameInfo' + ' ' + QuotedStr(GCode));
        dmMain.SportbaseQuery2.Open;
        //Check for record count > 0 and process
        if (dmMain.SportbaseQuery2.RecordCount > 0) then
        begin
          OutRec.League := dmMain.SportbaseQuery2.FieldByName('League').AsString;
          //Get extra fields if it's an MLB game
          if (League = 'MLB') then
          begin
            OutRec.SubLeague := dmMain.SportbaseQuery2.FieldByName('SubLeague').AsString;
            OutRec.DoubleHeader := dmMain.SportbaseQuery2.FieldByName('DoubleHeader').AsBoolean;
            OutRec.DoubleHeaderGameNumber := dmMain.SportbaseQuery2.FieldByName('DoubleHeaderGameNumber').AsInteger;
            OutRec.TimeMin := 0;
            OutRec.TimeSec := 0;
            OutRec.Count := dmMain.SportbaseQuery2.FieldByName('Count').AsString;;
            OutRec.Situation := dmMain.SportbaseQuery2.FieldByName('Situation').AsString;;
          end
          else if (League = 'CFB') OR (League = 'CFL') OR (League = 'NFL')then
          begin
            OutRec.SubLeague := ' ';
            OutRec.DoubleHeader := FALSE;
            OutRec.DoubleHeaderGameNumber := 0;
            OutRec.TimeMin := dmMain.SportbaseQuery2.FieldByName('TimeMin').AsInteger;
            OutRec.TimeSec := dmMain.SportbaseQuery2.FieldByName('TimeSec').AsInteger;
            OutRec.Count := '000';
            OutRec.Situation := dmMain.SportbaseQuery2.FieldByName('Situation').AsString;;
          end
          else begin
            OutRec.SubLeague := ' ';
            OutRec.DoubleHeader := FALSE;
            OutRec.DoubleHeaderGameNumber := 0;
            OutRec.TimeMin := dmMain.SportbaseQuery2.FieldByName('TimeMin').AsInteger;
            OutRec.TimeSec := dmMain.SportbaseQuery2.FieldByName('TimeSec').AsInteger;
            OutRec.Count := '000';
            OutRec.Situation := 'FFF';
          end;
          //Set team strength variables for NHL
          if (League = 'NHL') then
          begin
            if (ANSIUpperCase(dmMain.SportbaseQuery2.FieldByName('VStrength').AsString) = 'Powerplay') then
              OutRec.Visitor_Param1 := 1
            else
              OutRec.Visitor_Param1 := 0;
            if (ANSIUpperCase(dmMain.SportbaseQuery2.FieldByName('HStrength').AsString) = 'Powerplay') then
              OutRec.Home_Param1 := 1
            else
              OutRec.Home_Param1 := 0;
          end
          else begin
            OutRec.Visitor_Param1 := 0;
            OutRec.Home_Param1 := 0;
          end;
          //If college, get ranks
          if (League = 'CFB') OR (League = 'CBK') OR (League = 'CBB') then
          begin
            OutRec.VRank := dmMain.SportbaseQuery2.FieldByName('VRank').AsString;
            OutRec.HRank := dmMain.SportbaseQuery2.FieldByName('HRank').AsString;
          end
          else begin
            OutRec.VRank := ' ';
            OutRec.HRank := ' ';
          end;
          //Set remaining parameters
          OutRec.HName := dmMain.SportbaseQuery2.FieldByName('HName').AsString;
          OutRec.VName := dmMain.SportbaseQuery2.FieldByName('VName').AsString;
          OutRec.HMnemonic := dmMain.SportbaseQuery2.FieldByName('HMnemonic').AsString;
          OutRec.VMnemonic := dmMain.SportbaseQuery2.FieldByName('VMnemonic').AsString;
          OutRec.HScore := dmMain.SportbaseQuery2.FieldByName('HScore').AsInteger;
          OutRec.VScore := dmMain.SportbaseQuery2.FieldByName('VScore').AsInteger;
          OutRec.VOpeningOdds := dmMain.SportbaseQuery2.FieldByName('VOpeningOdds').AsFloat;
          OutRec.HOpeningOdds := dmMain.SportbaseQuery2.FieldByName('HOpeningOdds').AsFloat;
          OutRec.OpeningTotal := dmMain.SportbaseQuery2.FieldByName('OpeningTotal').AsFloat;
          OutRec.VCurrentOdds := dmMain.SportbaseQuery2.FieldByName('VCurrentOdds').AsFloat;
          OutRec.HCurrentOdds := dmMain.SportbaseQuery2.FieldByName('HCurrentOdds').AsFloat;
          OutRec.CurrentTotal := dmMain.SportbaseQuery2.FieldByName('CurrentTotal').AsFloat;
          OutRec.Phase := dmMain.SportbaseQuery2.FieldByName('Phase').AsInteger;
          //Set the game state
          if (Trim(dmMain.SportbaseQuery2.FieldByName('State').AsString) = 'Pre-Game') then
            OutRec.State := 1
          else if (Trim(dmMain.SportbaseQuery2.FieldByName('State').AsString) = 'In-Progress') then
            OutRec.State := 2
          else if (Trim(dmMain.SportbaseQuery2.FieldByName('State').AsString) = 'Final') then
            OutRec.State := 3
          else if (Trim(dmMain.SportbaseQuery2.FieldByName('State').AsString) = 'Postponed') then
            OutRec.State := 4
          else if (Trim(dmMain.SportbaseQuery2.FieldByName('State').AsString) = 'Delayed') then
            OutRec.State := 5
          else if (Trim(dmMain.SportbaseQuery2.FieldByName('State').AsString) = 'Suspended') then
            OutRec.State := 6
          else
            OutRec.State := -1;
          OutRec.Description := OutRec.League + ': ' + OutRec.VName + ' @ ' + OutRec.HName;
          OutRec.Date := dmMain.SportbaseQuery2.FieldByName('Date').AsDateTime;
          OutRec.StartTime := dmMain.SportbaseQuery2.FieldByName('StartTime').AsDateTime;
          OutRec.DataFound := TRUE;
        end
        else OutRec.DataFound := FALSE;
      except
        if (ErrorLoggingEnabled = True) then
          WriteToErrorLog('Error occurred in GetGameData function for full game data');
        //Clear data found flag
        OutRec.DataFound := FALSE;
      end;
    end;
  end;
  //Return
  GetGameData := OutRec;
end;

//Function to format the game clock into minutes and seconds
function TEngineInterface.FormatGameClock(TimeMin: SmallInt; TimeSec: SmallInt): String;
var
  SecsStr: String;
  OutStr: String;
begin
  //Check for manual game as indicated by min, sec = -1
  if (TimeSec = -1) AND (TimeMin = -1) then OutStr := ''
  else begin
    if (TimeSec < 10) then SecsStr := '0' + IntToStr(TimeSec)
    else SecsStr := IntToStr(TimeSec);
    if (TimeMin > 0) then
      OutStr := IntToStr(TimeMin) + ':' + SecsStr
    else
      OutStr := ':' + SecsStr;
  end;
  FormatGameClock := OutStr;
end;

//Function to get game state
function TEngineInterface.GetGameState(LEAGUE: String; GTIME: String; GPHASE: SmallInt): Integer;
begin
  if (LEAGUE = 'AL') OR (LEAGUE = 'ML') OR (LEAGUE = 'NL') OR (LEAGUE = 'CAC') OR
     (LEAGUE = 'GPFT') OR (LEAGUE = 'MLB') THEN
  begin
    if (GTIME = '9999') then
      //Game not started
      Result := 0
    else if (GTIME = 'FINA') OR (GTIME = 'SUM-') then
      //Game is final
      Result := 3
    else if (GTIME = 'END-') then
      //End of period/quarter
      Result := 2
    else if (StrToIntDef(Trim(GTIME), -1) <> -1) then
      //Game in progress
      Result := 1
    else
      //All other conditions
      Result := -1; // Game is suspended due to rain or cancelled etc. (GTIME = 'RAIN' , GTIME = 'SUSP', GTIME = 'CANC')
  end
  else begin
    if (GTIME = '9999') OR (GPHASE = 0) then
      //Game not started
      Result := 0
    else if (GTIME = 'FINA') OR (GTIME = 'SUM-') then
      //Game is final
      Result := 3
    else if (GTIME = 'END-') then
      //End of period/quarter
      Result := 2
    else if (GPHASE <> 0) AND (StrToIntDef(Trim(GTIME), -1) <> -1) then
      //Game in progress
      Result := 1
    else
      //All other conditions
      Result := -1; // Game is suspended due to rain or cancelled etc. (GTIME = 'RAIN' , GTIME = 'SUSP', GTIME = 'CANC')
  end;
end;

//Function to get game time string from GT Server time value
function TEngineInterface.GetGameTimeString(GTimeStr: String): String;
var
  Min, Sec: String;
begin
  //Blank out case where time = 9999 - for MLB
  if (GTimeStr = '9999') then Result := ' '
  //Actual clock time
  else if (StrToIntDef(Trim(GTimeStr), -1) <> -1) AND (Length(GTimeStr) = 4) then
  begin
    Min := GTimeStr[1] + GTimeStr[2];
    Sec := GTimeStr[3] + GTimeStr[4];
    Min := IntToStr(StrToInt(GTimeStr[1]+GTimeStr[2]));
    Result := Min + ':' + Sec;
  end
  else if (GTimeStr = 'END-') then Result := 'End'
  else if (GTimeStr = 'POST') then Result := 'PPD'
  else if (GTimeStr = 'SUSP') then Result := 'PPD'
  else if (GTimeStr = 'DELA') then Result := 'DLY'
  else if (GTimeStr = 'RAIN') then Result := 'RD'
  else if (GTimeStr = 'CANC') then Result := 'PPD'
  else Result := ' ';
end;

//Function to take a league and a game phase code, and return a game phase record
function TEngineInterface.GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
var
  i: SmallInt;
  GamePhaseRecPtr: ^GamePhaseRec; //Pointer to game phase record type
  OutRec: GamePhaseRec;
  FoundRecord: Boolean;
begin
  FoundRecord := FALSE;
  OutRec.League := ' ';
  OutRec.ST_Phase_Code := 0;
  OutRec.SI_Phase_Code := 0;
  OutRec.Display_Period := ' ';
  OutRec.End_Is_Half := FALSE;
  OutRec.Display_Half := ' ';
  OutRec.Display_Final := ' ';
  OutRec.Display_Extended1 := ' ';
  OutRec.Display_Extended2 := ' ';
  OutRec.Game_OT := FALSE;
  if (Game_Phase_Collection.Count > 0) then
  begin
    i := 0;
    repeat
      GamePhaseRecPtr := Game_Phase_Collection.At(i);
      if (Trim(GamePhaseRecPtr^.League) = Trim(League)) AND (GamePhaseRecPtr^.SI_Phase_Code = GPhase) then
      begin
        OutRec.League := Trim(League);
        OutRec.ST_Phase_Code := GPhase;
        OutRec.SI_Phase_Code := GPhase;
        OutRec.Label_Period := Trim(GamePhaseRecPtr^.Label_Period);
        OutRec.Display_Period := Trim(GamePhaseRecPtr^.Display_Period);
        OutRec.End_Is_Half := GamePhaseRecPtr^.End_Is_Half;
        OutRec.Display_Half := GamePhaseRecPtr^.Display_Half;
        OutRec.Display_Half_Short := GamePhaseRecPtr^.Display_Half_Short;
        OutRec.Display_Half := Trim(GamePhaseRecPtr^.Display_Half);
        OutRec.Display_Final := Trim(GamePhaseRecPtr^.Display_Final);
        OutRec.Display_Final_Short := Trim(GamePhaseRecPtr^.Display_Final_Short);
        OutRec.Display_Extended1 := Trim(GamePhaseRecPtr^.Display_Extended1);
        OutRec.Display_Extended2 := Trim(GamePhaseRecPtr^.Display_Extended2);
        OutRec.Game_OT := GamePhaseRecPtr^.Game_OT;
        FoundRecord := TRUE;
      end;
      Inc(i);
    until (i = Game_Phase_Collection.Count) OR (FoundRecord = TRUE);
  end;
  GetGamePhaseRec := OutRec;
end;

//Function to take the league code and Sportsticker team code, and return the team menmonic
function TEngineInterface.GetTeamMnemonic(LeagueCode: String; STTeamCode: String): String;
var
  i: SmallInt;
  TeamRecPtr: ^TeamRec; //Pointer to stat record type
  OutStr: String;
  FoundRec: Boolean;
begin
  OutStr := ' ';
  if (Team_Collection.Count > 0) then
  begin
    i := 0;
    FoundRec := FALSE;
    repeat
      TeamRecPtr := Team_Collection.At(i);
//      if (Trim(TeamRecPtr^.NewSTTeamCode) = Trim(STTeamCode)) AND (Trim(TeamRecPtr^.League) = LeagueCode) then
//      begin
//        OutStr := Trim(TeamRecPtr^.TeamMnemonic);
//        FoundRec := TRUE;
//      end;
      Inc(i);
    Until (FoundRec = TRUE) OR (i = Team_Collection.Count);
  end;
  GetTeamMnemonic := OutStr;
end;

//Function to take the league code and Sportsticker team code, and return the team base name
function TEngineInterface.GetTeamBaseName(LeagueCode: String; STTeamCode: String): String;
var
  i: SmallInt;
  TeamRecPtr: ^TeamRec; //Pointer to stat record type
  OutStr: String;
  FoundRec: Boolean;
begin
  OutStr := ' ';
  if (Team_Collection.Count > 0) then
  begin
    i := 0;
    FoundRec := FALSE;
    repeat
      TeamRecPtr := Team_Collection.At(i);
//      if (Trim(TeamRecPtr^.NewSTTeamCode) = Trim(STTeamCode)) AND (Trim(TeamRecPtr^.League) = Trim(LeagueCode)) then
//      begin
//        OutStr := Trim(TeamRecPtr^.BaseName);
//        FoundRec := TRUE;
//      end;
      Inc(i);
    until (FoundRec = TRUE) OR (i = Team_Collection.Count);
  end;
  GetTeamBaseName := OutStr;
end;

// Function to take upper case string & return mixed-case string
function TEngineInterface.GetMixedCase(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := ANSILowerCase(InStr);
  if (Length(OutStr) > 0) then
  begin
    //Set first character to upper case if it's not a digit
    if (Ord(OutStr[1]) < 48) OR (Ord(OutStr[1]) > 57) then  OutStr[1] := Char(Ord(OutStr[1])-32);
    //Check if two words in state name, and set 2nd word to upper case
    if (Length(OutStr) > 2) then
    begin
      for i := 2 to Length(OutStr) do
      begin
        if (OutStr[i-1] = ' ') then OutStr[i] := Char(Ord(OutStr[i])-32);
      end;
    end;
  end;
  GetMixedCase := OutStr;
end;

//Procedure to set audio file
procedure TEngineInterface.SetAudioFile (AudioFileID: SmallInt);
var
  CmdStr: String;
begin
  try
    //Build & send command string
    CmdStr := SOT + 'SA' + ETX + IntToStr(AudioFileID) + EOT;
    if (SocketConnected) AND (EngineParameters.Enabled = TRUE) then ApdWinsockPort1.Output := CmdStr;
  except
  end;
end;

//Function to take 4-digit clock time string from Datamart and format as "mm:ss"
function TEngineInterface.FormatTimeString (InStr: String): String;
var
  OutStr: String;
begin
  OutStr := '';
  InStr := Trim(InStr);
  Case Length(InStr) of
    0: Begin
         OutStr := '';
       end;
    1: Begin
         OutStr := ':0' + InStr[1];
       end;
    2: Begin
         OutStr := ':' + InStr[1] + InStr[2];
       end;
    3: Begin
         OutStr := InStr[1] + ':' + InStr[2] + InStr[3];
       end;
    4: Begin
         OutStr := InStr[1] + InStr[2] + ':' + InStr[3] + InStr[4];
       end;
  end;
  FormatTimeString := OutStr;
end;

//Function to get sponsor logo file name based on logo description
function TEngineInterface.GetSponsorLogoFilename(LogoName: String): String;
var
  i: SmallInt;
  OutStr: String;
  SponsorLogoPtr: ^SponsorLogoRec;
begin
  for i := 0 to SponsorLogo_Collection.Count-1 do
  begin
    SponsorLogoPtr := SponsorLogo_Collection.At(i);
    if (SponsorLogoPtr^.SponsorLogoName = LogoName) then
    begin
      OutStr := SponsorLogoPtr^.SponsorLogoFileName;
    end;
  end;
  GetSponsorLogoFilename := OutStr;
end;

//Procedure to load temporary collection for fields (for current template); also returns
//associated template information
function TEngineInterface.LoadTempTemplateFields(TemplateID: SmallInt): TemplateDefsRec;
var
  i: SmallInt;
  TemplateFieldsRecPtr, NewTemplateFieldsRecPtr: ^TemplateFieldsRec;
  TemplateRecPtr: ^TemplateDefsRec;
  OutRec: TemplateDefsRec;
  FoundMatch: Boolean;
begin
  //Clear the existing collection
  Temporary_Fields_Collection.Clear;
  Temporary_Fields_Collection.Pack;
  //First, get the engine template ID
  if (Template_Defs_Collection.Count > 0) then
  begin
    i := 0;
    FoundMatch := FALSE;
    repeat
      TemplateRecPtr := Template_Defs_Collection.At(i);
      if (TemplateID = TemplateRecPtr^.Template_ID) then
      begin
        OutRec.Template_ID := TemplateRecPtr^.Template_ID;
        OutRec.Template_Type := TemplateRecPtr^.Template_Type;
        OutRec.Template_Description := TemplateRecPtr^.Template_Description;
        OutRec.Record_Type := TemplateRecPtr^.Record_Type;
        OutRec.Engine_Template_ID := TemplateRecPtr^.Engine_Template_ID;
        OutRec.Default_Dwell := TemplateRecPtr^.Default_Dwell;
        OutRec.UsesGameData := TemplateRecPtr^.UsesGameData;
        OutRec.RequiredGameState := TemplateRecPtr^.RequiredGameState;
        FoundMatch := TRUE;
      end;
      Inc(i);
    until (FoundMatch) OR (i = Template_Defs_Collection.Count);
  end;
  //Now, get the fields for the template
  for i := 0 to Template_Fields_Collection.Count-1 do
  begin
    TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
    if (TemplateFieldsRecPtr^.Template_ID = TemplateID) then
    begin
      GetMem (NewTemplateFieldsRecPtr, SizeOf(TemplateFieldsRec));
      With NewTemplateFieldsRecPtr^ do
      begin
        Template_ID := TemplateFieldsRecPtr^.Template_ID;
        Field_ID := TemplateFieldsRecPtr^.Field_ID;
        Field_Type := TemplateFieldsRecPtr^.Field_Type;
        Field_Is_Manual := TemplateFieldsRecPtr^.Field_Is_Manual;
        Field_Label := TemplateFieldsRecPtr^.Field_Label;
        Field_Contents := TemplateFieldsRecPtr^.Field_Contents;
        Field_Max_Length := TemplateFieldsRecPtr^.Field_Max_Length;
        Engine_Field_ID := TemplateFieldsRecPtr^.Engine_Field_ID;
      end;
      if (Temporary_Fields_Collection.Count <= 100) then
      begin
        //Add to collection
        Temporary_Fields_Collection.Insert(NewTemplateFieldsRecPtr);
        Temporary_Fields_Collection.Pack;
      end;
    end;
  end;
  LoadTempTemplateFields := OutRec;
end;

//Utility function to strip off any ASCII characters above ASCII 127
function StripPrefix(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := '';
  if (Length(InStr) > 0) then
  begin
    for i := 1 to Length(InStr) do
    begin
      if (Ord(InStr[i]) <=127) then OutStr := OutStr + InStr[i];
    end;
  end;
  StripPrefix := OutStr;
end;

//FOR ODDS PAGES
const
  DELIMITER:STRING = Chr(160);
////////////////////////////////////////////////////////////////////////////////
// Function to take a symbolic name and return the value of the string to be
// sent to the engine
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetValueOfSymbol(PlaylistType: SmallInt; Symbolname: String;
                                           CurrentEntryIndex: SmallInt; CurrentGameData: GameRec;
                                           TickerMode: SmallInt): String;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  BugRecPtr: ^BugRec;
  ExtraLineRecPtr: ^ExtraLineRec;
  OutStr, OutStr2, OutStr3: String;
  TextFieldBias: SmallInt;
  GamePhaseData: GamePhaseRec;
  BaseName: String;
  CharPos: SmallInt;
  Suffix: String;
  SwitchPosMixed, SwitchPosUpper: SmallInt;
  SwitchPosStartBlack, SwitchPosEndBlack: SmallInt;
  TempStr, TempStr2, TempStr3: String;
  Days: array[1..7] of string;
  OneLineBias: SmallInt;
begin
  //Init
  OutStr := SymbolName;
  BaseName := '';
  Suffix := '';
  LongTimeFormat := 'h:mm AM/PM';

  //Set text field bias
  Case TickerMode of
    -1: TextFieldBias := 0;
     1: TextFieldBias := 25;
  end;

  //Get info on current record
  if (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
     TICKER: TickerRecPtr := Ticker_Collection.At(CurrentEntryIndex);
     BUG: BugRecPtr := Bug_Collection.At(CurrentEntryIndex);
     EXTRALINE: ExtraLineRecPtr := ExtraLine_Collection.At(CurrentEntryIndex);
    end;
  end;

  //Extract out base symbol name and any suffix
  CharPos := Pos('|', SymbolName);
  if (CharPos > 0) then
  begin
    //Get base name
    if (CharPos > 1) then
      for i := 1 to CharPos-1 do BaseName := BaseName + SymbolName[i];
    //Get suffix
    if (Length(Symbolname) > CharPos) then
      for i := CharPos+1 to Length(Symbolname) do Suffix := Suffix + SymbolName[i];
    //Set new base name
    SymbolName := BaseName;
  end;
  //Sponsor logo
  if (SymbolName = '$Blank') then OutStr := ' '
  else if (SymbolName = '$Sponsor_Logo') then
  begin
    if (CurrentEntryIndex <> NOENTRYINDEX) then
    begin
      Case  PlaylistType of
        TICKER: OutStr := TickerRecPtr^.SponsorLogo_Name;
      end;
    end
    else OutStr := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
  end
  //Promo logo
  else if (SymbolName = '$Promo_Logo') then
  begin
    if (CurrentEntryIndex <> NOENTRYINDEX) then
    begin
      Case  PlaylistType of
        TICKER: OutStr := TickerRecPtr^.SponsorLogo_Name;
      end;
    end
    else OutStr := dmMain.tblPromo_Logos.FieldByName('LogoFilename').AsString;
  end
  //League
  else if (SymbolName = '$League') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := UpperCase(TickerRecPtr^.Subleague_Mnemonic_Standard);
      BUG: OutStr := UpperCase(BugRecPtr^.League);
      EXTRALINE: OutStr := UpperCase(ExtraLineRecPtr^.League);
    end;
  end

  //User-defined text fields
  else if (SymbolName = '$Text_1') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: begin
               //DISABLE FOR NOW - CAUSES PROBLEMS WITH OTHER TEMPLATES
               //Check for doubleheader in baseball
               //if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.State = 3) then
               //begin
                 //End of game, so display game number in note field
               //  if (CurrentGameData.DoubleHeaderGameNumber = 1) then
               //    OutStr := 'GAME 1'
               //  else if (CurrentGameData.DoubleHeaderGameNumber = 2) then
               //    OutStr := 'GAME 2'
               //  else OutStr := ' ';
               //end
               //else
               OutStr := TickerRecPtr^.UserData[1+TextFieldBias];
               //Added for 1-line ticker to get weather forecast text
               if (Suffix = 'GET_FORECAST_TEXT') then
                 OutStr := GetForecastText(TickerRecPtr^.UserData[1+TextFieldBias])
               else if (Suffix <> '') then OutStr := OutStr + Suffix;
              end;
      BUG: OutStr := BugRecPtr^.UserData[1];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[1];
    end;
  end
  else if (SymbolName = '$Text_2') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: begin
        OutStr := TickerRecPtr^.UserData[2+TextFieldBias];
        //Added for 1-line ticker to get weather forecast text
        if (Suffix = 'GET_FORECAST_TEXT') then
          OutStr := GetForecastText(TickerRecPtr^.UserData[2+TextFieldBias])
        else if (Suffix <> '') then OutStr := OutStr + Suffix;
      end;
      BUG: OutStr := BugRecPtr^.UserData[2];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[2];
    end;
  end
  else if (SymbolName = '$Text_3') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: begin
        OutStr := TickerRecPtr^.UserData[3+TextFieldBias];
        //Added for 1-line ticker to get weather forecast text
        if (Suffix = 'GET_FORECAST_TEXT') then
          OutStr := GetForecastText(TickerRecPtr^.UserData[3+TextFieldBias])
        else if (Suffix <> '') then OutStr := OutStr + Suffix;
      end;
      BUG: OutStr := BugRecPtr^.UserData[3];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[3];
    end;
  end
  else if (SymbolName = '$Text_4') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: begin
        OutStr := TickerRecPtr^.UserData[4+TextFieldBias];
        //Added for 1-line ticker to get weather forecast text
        if (Suffix = 'GET_FORECAST_TEXT') then
          OutStr := GetForecastText(TickerRecPtr^.UserData[4+TextFieldBias])
        else if (Suffix <> '') then OutStr := OutStr + Suffix;
      end;
      BUG: OutStr := BugRecPtr^.UserData[4];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[4];
    end;
  end
  else if (SymbolName = '$Text_5') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: begin
         OutStr := TickerRecPtr^.UserData[5+TextFieldBias];
         //Added for 1-line ticker to get weather forecast text
         if (Suffix = 'GET_FORECAST_TEXT') then
           OutStr := GetForecastText(TickerRecPtr^.UserData[5+TextFieldBias])
         else if (Suffix <> '') then OutStr := OutStr + Suffix;
      end;
      BUG: OutStr := BugRecPtr^.UserData[5];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[5];
    end;
  end
  else if (SymbolName = '$Text_6') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: begin
        OutStr := TickerRecPtr^.UserData[6+TextFieldBias];
        //Added for 1-line ticker to get weather forecast text
        if (Suffix = 'GET_FORECAST_TEXT') then
          OutStr := GetForecastText(TickerRecPtr^.UserData[6+TextFieldBias])
        else if (Suffix <> '') then OutStr := OutStr + Suffix;
      end;
      BUG: OutStr := BugRecPtr^.UserData[6];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[6];
    end;
  end
  else if (SymbolName = '$Text_7') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: begin
        OutStr := TickerRecPtr^.UserData[7+TextFieldBias];
        //Added for 1-line ticker to get weather forecast text
        if (Suffix = 'GET_FORECAST_TEXT') then
          OutStr := GetForecastText(TickerRecPtr^.UserData[7+TextFieldBias])
        else if (Suffix <> '') then OutStr := OutStr + Suffix;
      end;
      BUG: OutStr := BugRecPtr^.UserData[7];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[7];
    end;
  end
  else if (SymbolName = '$Text_8') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: begin
        OutStr := TickerRecPtr^.UserData[8+TextFieldBias];
        //Added for 1-line ticker to get weather forecast text
        if (Suffix = 'GET_FORECAST_TEXT') then
          OutStr := GetForecastText(TickerRecPtr^.UserData[8+TextFieldBias])
        else if (Suffix <> '') then OutStr := OutStr + Suffix;
      end;
      BUG: OutStr := BugRecPtr^.UserData[8];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[8];
    end;
  end
  else if (SymbolName = '$Text_9') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: begin
        OutStr := TickerRecPtr^.UserData[9+TextFieldBias];
        //Added for 1-line ticker to get weather forecast text
        if (Suffix = 'GET_FORECAST_TEXT') then
          OutStr := GetForecastText(TickerRecPtr^.UserData[9+TextFieldBias])
        else if (Suffix <> '') then OutStr := OutStr + Suffix;
      end;
      BUG: OutStr := BugRecPtr^.UserData[9];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[9];
    end;
  end
  else if (SymbolName = '$Text_10') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: begin
        OutStr := TickerRecPtr^.UserData[10+TextFieldBias];
        //Added for 1-line ticker to get weather forecast text
        if (Suffix = 'GET_FORECAST_TEXT') then
          OutStr := GetForecastText(TickerRecPtr^.UserData[10+TextFieldBias])
        else if (Suffix <> '') then OutStr := OutStr + Suffix;
      end;
      BUG: OutStr := BugRecPtr^.UserData[10];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[10+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_11') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[11];
      BUG: OutStr := BugRecPtr^.UserData[11];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[11+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_12') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[12];
      BUG: OutStr := BugRecPtr^.UserData[12];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[12+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_13') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[13];
      BUG: OutStr := BugRecPtr^.UserData[13];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[13+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_14') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[14];
      BUG: OutStr := BugRecPtr^.UserData[14];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[14+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_15') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[15+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[15];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[15];
    end;
  end
  else if (SymbolName = '$Text_16') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[16+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[16];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[16];
    end;
  end
  else if (SymbolName = '$Text_17') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[17+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[17];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[17];
    end;
  end
  else if (SymbolName = '$Text_18') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[18+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[18];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[18];
    end;
  end
  else if (SymbolName = '$Text_19') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[19+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[19];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[19];
    end;
  end
  else if (SymbolName = '$Text_20') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[20+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[20];
      EXTRALINE: OutStr := ExtraLineRecPtr^.UserData[20];
    end;
  end
  else if (SymbolName = '$Text_21') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[21+TextFieldBias];
      BUG: OutStr := '';
      EXTRALINE: OutStr := '';
    end;
  end
  else if (SymbolName = '$Text_22') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[22+TextFieldBias];
      BUG: OutStr := '';
      EXTRALINE: OutStr := '';
    end;
  end
  else if (SymbolName = '$Text_23') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[23+TextFieldBias];
      BUG: OutStr := '';
      EXTRALINE: OutStr := '';
    end;
  end
  else if (SymbolName = '$Text_24') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[24+TextFieldBias];
      BUG: OutStr := '';
      EXTRALINE: OutStr := '';
    end;
  end
  else if (SymbolName = '$Text_25') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[25+TextFieldBias];
      BUG: OutStr := '';
      EXTRALINE: OutStr := '';
    end;
  end

  //Game related data
  else if (SymbolName = '$Start_Time') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
      OutStr := 'GAME 2'
    else begin
      //Check to see if start time more than 24 hours ahead. If so, show day of week.
      if ((Trunc(CurrentGameData.Date) + CurrentGameData.StartTime) - Now > 1) then
      begin
        Days[1] := 'SUNDAY';
        Days[2] := 'MONDAY';
        Days[3] := 'TUESDAY';
        Days[4] := 'WEDNESDAY';
        Days[5] := 'THURSDAY';
        Days[6] := 'FRIDAY';
        Days[7] := 'SATURDAY';
        OutStr := Days[DayOfWeek(CurrentGameData.Date)];
      end
      else
        OutStr := TimeToStr(CurrentGameData.StartTime) + ' ET';
    end;
  end
  else if (SymbolName = '$Start_Time_No_Timezone') then OutStr := TimeToStr(CurrentGameData.StartTime)
  else if (SymbolName = '$Start_Time_No_Suffix') then
  begin
    OutStr := TimeToStr(CurrentGameData.StartTime);
    OutStr := StringReplace(OutStr, 'AM', ' ', [rfReplaceAll]);
    OutStr := StringReplace(OutStr, 'PM', ' ', [rfReplaceAll]);
    OutStr := Trim(OutStr) + ' ET';
  end
  else if (SymbolName = '$Visitor_Name') then
  begin
    OutStr := UpperCase(CurrentGameData.VName);
    //If NHL and game in-progress, append strength indicator symbols
    if (CurrentGameData.League = 'NHL') AND (CurrentGameData.State = 2) then
    begin
      if ((CurrentGameData.Visitor_Param1 - CurrentGameData.Home_Param1) = 2) then
        OutStr := OutStr + ' ' + TWOMANADVANTAGE
      else if ((CurrentGameData.Visitor_Param1 - CurrentGameData.Home_Param1) = 1) then
        OutStr := OutStr + ' ' + POWERPLAY;
    end
    //If NFL, CFB or CFL, append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'CFB') OR
             (CurrentGameData.League = 'CFL')) AND (CurrentGameData.State = 2) then
    begin
      if (GetFootballGameSituationData(CurrentGameData).VisitorHasPossession = TRUE) then
        OutStr := OutStr + ' ' + FOOTBALLPOSSESSION;
    end;
  end
  else if (SymbolName = '$Home_Name') then
  begin
    OutStr := UpperCase(CurrentGameData.HName);
    //If NHL and game in-progress, append strength indicator symbols
    if (CurrentGameData.League = 'NHL') AND (CurrentGameData.State = 2) then
    begin
      if ((CurrentGameData.Home_Param1 - CurrentGameData.Visitor_Param1) = 2) then
        OutStr := OutStr + ' ' + TWOMANADVANTAGE
      else if ((CurrentGameData.Home_Param1 - CurrentGameData.Visitor_Param1) = 1) then
        OutStr := OutStr + ' ' + POWERPLAY;
    end
    //If NFL, CFB or CFL, append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'CFB') OR
             (CurrentGameData.League = 'CFL')) AND (CurrentGameData.State = 2) then
    begin
      if (GetFootballGameSituationData(CurrentGameData).HomeHasPossession = TRUE) then
        OutStr := OutStr + ' ' + FOOTBALLPOSSESSION;
    end;
  end
  else if (SymbolName = '$Visitor_Mnemonic') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic);
    //If NHL and game in-progress, append strength indicator symbols
    if (CurrentGameData.League = 'NHL') AND (CurrentGameData.State = 2) then
    begin
      if ((CurrentGameData.Visitor_Param1 - CurrentGameData.Home_Param1) = 2) then
        OutStr := OutStr + ' ' + TWOMANADVANTAGE
      else if ((CurrentGameData.Visitor_Param1 - CurrentGameData.Home_Param1) = 1) then
        OutStr := OutStr + ' ' + POWERPLAY;
    end
    //If NFL, CFB or CFL, append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'CFB') OR
             (CurrentGameData.League = 'CFL')) AND (CurrentGameData.State = 2) then
    begin
      if (GetFootballGameSituationData(CurrentGameData).VisitorHasPossession = TRUE) then
        OutStr := OutStr + ' ' + FOOTBALLPOSSESSION;
    end;
  end
  else if (SymbolName = '$Home_Mnemonic') then
  begin
    OutStr := UpperCase(CurrentGameData.HMnemonic);
    //If NHL and game in-progress, append strength indicator symbols
    if (CurrentGameData.League = 'NHL') AND (CurrentGameData.State = 2) then
    begin
      if ((CurrentGameData.Home_Param1 - CurrentGameData.Visitor_Param1) = 2) then
        OutStr := OutStr + ' ' + TWOMANADVANTAGE
      else if ((CurrentGameData.Home_Param1 - CurrentGameData.Visitor_Param1) = 1) then
        OutStr := OutStr + ' ' + POWERPLAY;
    end
    //If NFL, CFB or CFL, append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'CFB') OR
             (CurrentGameData.League = 'CFL')) AND (CurrentGameData.State = 2) then
    begin
      if (GetFootballGameSituationData(CurrentGameData).HomeHasPossession = TRUE) then
        OutStr := OutStr + ' ' + FOOTBALLPOSSESSION;
    end;
  end
  else if (SymbolName = '$Visitor_Score') then
  begin
    //Don't display score if game is postponed or suspended, or if delayed and at top of 1st
    if (CurrentGameData.State <> 4) AND (CurrentGameData.State <> 6) AND
       (NOT((CurrentGameData.State = 5) AND (CurrentGameData.Phase = 0))) then
      OutStr := IntToStr(CurrentGameData.VScore)
    else
      OutStr := ' ';
  end
  else if (SymbolName = '$Home_Score') then
  begin
    //Don't delay score if game is postponed or suspended
    if (CurrentGameData.State <> 4) AND (CurrentGameData.State <> 6) AND
       (NOT((CurrentGameData.State = 5) AND (CurrentGameData.Phase = 0))) then
      OutStr := IntToStr(CurrentGameData.HScore)
    else
      OutStr := ' ';
  end
  //Game clock
  else if (SymbolName = '$Game_Clock') then
  begin
    if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) then
      OutStr := ''
    else
      OutStr := FormatGameClock(CurrentGameData.TimeMin, CurrentGameData.TimeSec)
  end
  //Game phase
  else if (SymbolName = '$Game_Phase') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = 1) then OutStr := ' '
    //In-Progress
    else if (CurrentGameData.State = 2) then
    begin
      if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = FALSE) then
        OutStr := 'END ' + UpperCase(GamePhaseData.Display_Period)
      else if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = TRUE) then
        OutStr := UpperCase(GamePhaseData.Display_Half)
      else
        OutStr := UpperCase(GamePhaseData.Display_Period);
    end
    //Final
    else if (CurrentGameData.State = 3) then
    begin
      OutStr := UpperCase(GamePhaseData.Display_Final);
    end
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = 4) then
    begin
      Case PlaylistType of
        TICKER: OutStr := 'POSTPONED';
        BUG: OutStr := 'POSTPONED';
        EXTRALINE: OutStr := 'PPD';
      end;
    end
    //Delayed
    else if (CurrentGameData.State = 5) then
    begin
      Case PlaylistType of
        TICKER: OutStr := 'DELAY';
        BUG: OutStr := 'DELAY';
        EXTRALINE: OutStr := 'DLY';
      end;
    end
    //Suspended
    else if (CurrentGameData.State = 6) then
    begin
      Case PlaylistType of
        TICKER: OutStr := 'SUSPENDED';
        BUG: OutStr := 'SUSPENDED';
        EXTRALINE: OutStr := 'SPD';
      end;
    end;
  end
  //Game phase (short)
  else if (SymbolName = '$Game_Phase_Short') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = 1) then OutStr := ' '
    //In-Progress
    else if (CurrentGameData.State = 2) AND (CurrentEntryIndex <> NOENTRYINDEX) then
    begin
      if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = FALSE) then
      begin
        Case PlaylistType of
          TICKER: TempStr := UpperCase(TickerRecPtr^.Subleague_Mnemonic_Standard);
          BUG: TempStr := UpperCase(BugRecPtr^.League);
          EXTRALINE: TempStr := UpperCase(ExtraLineRecPtr^.League);
        end;
        if (TempStr = 'MLB') then
        begin
          //Display top/btm inning indicator; do substitution to get small arrows
          if (OneLineModeEnable) then
          begin
            TempStr3 := UpperCase(GamePhaseData.Display_Period);
            TempStr3 := StringReplace(TempStr3, Chr(181), Chr(183),[rfReplaceAll]);
            TempStr3 := StringReplace(TempStr3, Chr(182), Chr(184),[rfReplaceAll]);
            OutStr := '[r 0]' + TempStr3;
          end
          else
            OutStr := UpperCase(GamePhaseData.Label_Period);
        end
        else
          OutStr := 'END ' + UpperCase(GamePhaseData.Display_Period);
      end
      else if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = TRUE) then
        OutStr := UpperCase(GamePhaseData.Display_Half_Short)
      else
        OutStr := UpperCase(GamePhaseData.Display_Period);
    end
    //Final
    else if (CurrentGameData.State = 3) then
    begin
      OutStr := UpperCase(GamePhaseData.Display_Final_Short);
    end
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = 4) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = 5) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = 6) then
    begin
      OutStr := 'SPD';
    end
    else OutStr := '';
  end
  //Game phase (short) with no END prefix
  else if (SymbolName = '$Game_Phase_Short_No_End') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = 1) then OutStr := ' '
    //In-Progress
    else if (CurrentGameData.State = 2) AND (CurrentEntryIndex <> NOENTRYINDEX) then
    begin
      if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = FALSE) then
      begin
        Case PlaylistType of
          TICKER: TempStr := UpperCase(TickerRecPtr^.Subleague_Mnemonic_Standard);
          BUG: TempStr := UpperCase(BugRecPtr^.League);
          EXTRALINE: TempStr := UpperCase(ExtraLineRecPtr^.League);
        end;
        if (TempStr = 'MLB') then
          OutStr := UpperCase(GamePhaseData.Label_Period)
        else
          OutStr := UpperCase(GamePhaseData.Display_Period);
      end
      else if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = TRUE) then
        OutStr := UpperCase(GamePhaseData.Display_Half_Short)
      else
        OutStr := UpperCase(GamePhaseData.Display_Period);
    end
    //Final
    else if (CurrentGameData.State = 3) then
    begin
      OutStr := UpperCase(GamePhaseData.Display_Final_Short);
    end
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = 4) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = 5) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = 6) then
    begin
      OutStr := 'SPD';
    end
    else OutStr := '';
  end
  //Game phase (short) with top/bottom prefix stripped off
  else if (SymbolName = '$Game_Phase_Short_No_Prefix') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = 1) then OutStr := ' '
    //In-Progress
    else if (CurrentGameData.State = 2) AND (CurrentEntryIndex <> NOENTRYINDEX) then
    begin
      if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = FALSE) then
      begin
        Case PlaylistType of
          TICKER: TempStr := UpperCase(TickerRecPtr^.Subleague_Mnemonic_Standard);
          BUG: TempStr := UpperCase(BugRecPtr^.League);
          EXTRALINE: TempStr := UpperCase(ExtraLineRecPtr^.League);
        end;
        if (TempStr = 'MLB') then
          OutStr := UpperCase(StripPrefix(GamePhaseData.Display_Period))
        else
          OutStr := 'END ' + UpperCase(StripPrefix(GamePhaseData.Display_Period));
      end
      else if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = TRUE) then
        OutStr := UpperCase(GamePhaseData.Display_Half_Short)
      else
        OutStr := UpperCase(GamePhaseData.Display_Period);
    end
    //Final
    else if (CurrentGameData.State = 3) then
    begin
      OutStr := UpperCase(GamePhaseData.Display_Final_Short);
    end
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = 4) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = 5) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = 6) then
    begin
      OutStr := 'SPD';
    end
    else OutStr := '';
  end
  //Game clock + phase phase
  else if (SymbolName = '$Game_Clock_Phase') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = 1) then
    begin
      OutStr := TimeToStr(CurrentGameData.StartTime) + ' ET';
    end
    //In-Progress
    else if (CurrentGameData.State = 2) AND (CurrentEntryIndex <> NOENTRYINDEX) then
    begin
      if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = FALSE) then
      begin
        Case PlaylistType of
          TICKER: TempStr := UpperCase(TickerRecPtr^.Subleague_Mnemonic_Standard);
          BUG: TempStr := UpperCase(BugRecPtr^.League);
          EXTRALINE: TempStr := UpperCase(ExtraLineRecPtr^.League);
        end;
        if (TempStr = 'MLB') then
          OutStr := UpperCase(GamePhaseData.Label_Period)
        else
          OutStr := 'END ' + UpperCase(GamePhaseData.Display_Period);
      end
      else if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = TRUE) then
        OutStr := UpperCase(GamePhaseData.Display_Half)
      else
        OutStr := FormatGameClock(CurrentGameData.TimeMin, CurrentGameData.TimeSec) + '  ' +
          UpperCase(GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase).Display_Period);
    end
    //Final
    else if (CurrentGameData.State = 3) then
    begin
      Case PlaylistType of
        TICKER: begin
          OutStr := UpperCase(GamePhaseData.Display_Final);
          //Check for doubleheader in baseball and append suffix if applicable
          if (CurrentGameData.DoubleHeader = TRUE) then
          begin
            //End of game, so display game number in note field
            if (CurrentGameData.DoubleHeaderGameNumber = 1) then
              OutStr := OutStr + ' - GAME 1'
            else if (CurrentGameData.DoubleHeaderGameNumber = 2) then
              OutStr := OutStr + ' - GAME 2'
          end;
        end;
        BUG, EXTRALINE: begin
          OutStr := UpperCase(GamePhaseData.Display_Final);
          //Check for doubleheader in baseball and append suffix if applicable
          if (CurrentGameData.DoubleHeader = TRUE) then
          begin
            //End of game, so display game number in note field
            if (CurrentGameData.DoubleHeaderGameNumber = 1) then
              OutStr := OutStr + ' - GM 1'
            else if (CurrentGameData.DoubleHeaderGameNumber = 2) then
              OutStr := OutStr + ' - GM 2'
          end;
        end;
      end;
    end
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = 4) then
    begin
      Case PlaylistType of
        TICKER: OutStr := 'POSTPONED';
        BUG: OutStr := 'POSTPONED';
        EXTRALINE: OutStr := 'PPD';
      end;
    end
    //Delayed
    else if (CurrentGameData.State = 5) then
    begin
      Case PlaylistType of
        TICKER: OutStr := 'DELAY';
        BUG: OutStr := 'DELAY';
        EXTRALINE: OutStr := 'DLY';
      end;
    end
    //Suspended
    else if (CurrentGameData.State = 6) then
    begin
      Case PlaylistType of
        TICKER: OutStr := 'SUSPENDED';
        BUG: OutStr := 'SUSPENDED';
        EXTRALINE: OutStr := 'SPD';
      end;
    end
    else OutStr := '';
  end

  //Inning indicator with inning number
  else if (SymbolName = '$Inning_Indicator_Number') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = 2) then
      OutStr := GamePhaseData.Display_Period
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = 4) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = 5) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = 6) then
    begin
      OutStr := 'SPD';
    end
    else
      OutStr := ' ';
  end
  //Top of inning indicator
  else if (SymbolName = '$Inning_Indicator_Top') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = 2) then
    begin
      if (Pos('TOP', UpperCase(GamePhaseData.Label_Period)) <> 0) then OutStr := 'TOP'
      else OutStr := 'CLEAR';
    end
    else OutStr := 'CLEAR';
  end
  //Bottom of inning indicator
  else if (SymbolName = '$Inning_Indicator_Bottom') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = 2) then
    begin
      if (Pos('BOT', UpperCase(GamePhaseData.Label_Period)) <> 0) then OutStr := 'BOTTOM'
      else OutStr := 'CLEAR';
    end
    else OutStr := 'CLEAR';
  end
  //Baserunner situation
  else if (SymbolName = '$Baserunner_Situation') then
  begin
    if (CurrentGameData.State = 2) then
    begin
      if (CurrentGameData.Situation = 'FFF') then OutStr := 'BB_FIELD_FFF'
      else if (CurrentGameData.Situation = 'FFT') then OutStr := 'BB_FIELD_FFT'
      else if (CurrentGameData.Situation = 'FTF') then OutStr := 'BB_FIELD_FTF'
      else if (CurrentGameData.Situation = 'FTT') then OutStr := 'BB_FIELD_FTT'
      else if (CurrentGameData.Situation = 'TFF') then OutStr := 'BB_FIELD_TFF'
      else if (CurrentGameData.Situation = 'TFT') then OutStr := 'BB_FIELD_TFT'
      else if (CurrentGameData.Situation = 'TTF') then OutStr := 'BB_FIELD_TTF'
      else if (CurrentGameData.Situation = 'TTT') then OutStr := 'BB_FIELD_TTT'
      else OutStr := 'BB_FIELD_FFF';
    end
    else OutStr := 'BB_FIELD_FFF';
  end
  //Baserunner situation
  else if (SymbolName = '$Number_Outs') then
  begin
    if (CurrentGameData.State = 2) AND (Length(CurrentGameData.Count) >= 3) then
    begin
      if (CurrentGameData.Count[3] = '0') then OutStr := '0 OUT'
      else if (CurrentGameData.Count[3] = '1') then OutStr := '1 OUT'
      else if (CurrentGameData.Count[3] = '2') then OutStr := '2 OUT'
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Ranks for college games
  else if (SymbolName = '$Visitor_Rank') then
  begin
    OutStr := CurrentGameData.VRank;
  end
  else if (SymbolName = '$Home_Rank') then
  begin
    OutStr := CurrentGameData.HRank;
  end

  //Odds data
  else if (SymbolName = '$Opening_Total') then
  begin
    if (CurrentGameData.OpeningTotal = -9999) then
      OutStr := 'T: N/A'
    else begin
      CurrentGameData.OpeningTotal := (Trunc(CurrentGameData.OpeningTotal*2))/2;
      if (CurrentGameData.OpeningTotal = Trunc(CurrentGameData.OpeningTotal)) then
        OutStr := 'T: ' + FloatToStrF(CurrentGameData.OpeningTotal, ffFixed, 8, 0)
      else
        OutStr := 'T: ' + FloatToStrF(CurrentGameData.OpeningTotal, ffFixed, 8, 1);
    end;
  end
  else if (SymbolName = '$Current_Total') then
  begin
    if (CurrentGameData.CurrentTotal = -9999) then
      OutStr := 'T: N/A'
    else begin
      CurrentGameData.CurrentTotal := (Trunc(CurrentGameData.CurrentTotal*2))/2;
      if (CurrentGameData.CurrentTotal = Trunc(CurrentGameData.CurrentTotal)) then
        OutStr := 'T: ' + FloatToStrF(CurrentGameData.CurrentTotal, ffFixed, 8, 0)
      else
        OutStr := 'T: ' + FloatToStrF(CurrentGameData.CurrentTotal, ffFixed, 8, 1);
    end;
  end
  else if (SymbolName = '$Opening_Current_Total') then
  begin
    if (CurrentGameData.OpeningTotal = -9999) OR (CurrentGameData.CurrentTotal = -9999) then
      OutStr := 'T: N/A'
    else begin
      CurrentGameData.OpeningTotal := (Trunc(CurrentGameData.OpeningTotal*2))/2;
      CurrentGameData.CurrentTotal := (Trunc(CurrentGameData.CurrentTotal*2))/2;
      if (CurrentGameData.OpeningTotal = Trunc(CurrentGameData.OpeningTotal)) then
        TempStr := FloatToStrF(CurrentGameData.OpeningTotal, ffFixed, 8, 0)
      else
        TempStr := FloatToStrF(CurrentGameData.OpeningTotal, ffFixed, 8, 1);
      if (CurrentGameData.CurrentTotal = Trunc(CurrentGameData.CurrentTotal)) then
        TempStr2 := FloatToStrF(CurrentGameData.CurrentTotal, ffFixed, 8, 0)
      else
        TempStr2 := FloatToStrF(CurrentGameData.CurrentTotal, ffFixed, 8, 1);
      OutStr := 'T: ' + TempStr + ' / ' + TempStr2;
    end;
  end
  //Names + odds data
  else if (SymbolName = '$Visitor_Name_Opening_Odds') then
  begin
    OutStr := UpperCase(CurrentGameData.VName);
    //Add the color/font commands
    if (PlaylistType = TICKER) then OutStr := OutStr + Delimiter + ODDS_FORMAT_TICKER
    else if (PlaylistType = BUG) then OutStr := OutStr + ODDS_FORMAT_BUG
    else if (PlaylistType = EXTRALINE) then OutStr := OutStr + ODDS_FORMAT_EXTRALINE;
    //Show the leader with odds data
    if (CurrentGameData.VOpeningOdds <> -9999) AND (CurrentGameData.VOpeningOdds < 0) AND
       (CurrentGameData.VOpeningOdds < CurrentGameData.HOpeningOdds) then
    begin
      CurrentGameData.VOpeningOdds := (Trunc(CurrentGameData.VOpeningOdds*2))/2;
      if (CurrentGameData.VOpeningOdds = Trunc(CurrentGameData.VOpeningOdds)) then
        OutStr := OutStr + ' OPEN ' + FloatToStrF(CurrentGameData.VOpeningOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' OPEN ' + FloatToStrF(CurrentGameData.VOpeningOdds, ffFixed, 8, 1);
    end;
  end
  else if (SymbolName = '$Visitor_Mnemonic_Opening_Odds') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic);
    //Add the color/font commands
    if (PlaylistType = TICKER) then OutStr := OutStr + Delimiter + ODDS_FORMAT_TICKER
    else if (PlaylistType = BUG) then OutStr := OutStr + ODDS_FORMAT_BUG
    else if (PlaylistType = EXTRALINE) then OutStr := OutStr + ODDS_FORMAT_EXTRALINE;
    //Show the leader with odds data
    if (CurrentGameData.VOpeningOdds <> -9999) AND (CurrentGameData.VOpeningOdds < 0) AND
       (CurrentGameData.VOpeningOdds < CurrentGameData.HOpeningOdds) then
    begin
      CurrentGameData.VOpeningOdds := (Trunc(CurrentGameData.VOpeningOdds*2))/2;
      if (CurrentGameData.VOpeningOdds = Trunc(CurrentGameData.VOpeningOdds)) then
        OutStr := OutStr + ' OPEN ' + FloatToStrF(CurrentGameData.VOpeningOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' OPEN ' + FloatToStrF(CurrentGameData.VOpeningOdds, ffFixed, 8, 1);
    end;
  end
  else if (SymbolName = '$Home_Name_Opening_Odds') then
  begin
    OutStr := UpperCase(CurrentGameData.HName);
    //Add the color/font commands
    if (PlaylistType = TICKER) then OutStr := OutStr + Delimiter + ODDS_FORMAT_TICKER
    else if (PlaylistType = BUG) then OutStr := OutStr + ODDS_FORMAT_BUG
    else if (PlaylistType = EXTRALINE) then OutStr := OutStr + ODDS_FORMAT_EXTRALINE;
    //Show the leader with odds data
    if ((CurrentGameData.HOpeningOdds <> -9999) AND (CurrentGameData.HOpeningOdds < 0) AND
       (CurrentGameData.HOpeningOdds < CurrentGameData.VOpeningOdds)) OR
       //Put in special case for same odds - show home team as favorite
       ((CurrentGameData.HOpeningOdds = CurrentGameData.VOpeningOdds) AND
        (CurrentGameData.HOpeningOdds <> -9999) AND
        (CurrentGameData.HOpeningOdds < 0)) then
    begin
      CurrentGameData.HOpeningOdds := (Trunc(CurrentGameData.HOpeningOdds*2))/2;
      if (CurrentGameData.HOpeningOdds = Trunc(CurrentGameData.HOpeningOdds)) then
        OutStr := OutStr + ' OPEN ' + FloatToStrF(CurrentGameData.HOpeningOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' OPEN ' + FloatToStrF(CurrentGameData.HOpeningOdds, ffFixed, 8, 1);
    end
    //Put in special case for even/pick-em
    else if ((CurrentGameData.HOpeningOdds = 0) AND (CurrentGameData.VOpeningOdds = 0)) OR
     ((CurrentGameData.HOpeningOdds = 100) AND (CurrentGameData.VOpeningOdds = 100)) then
      OutStr := OutStr + ' PICK';
  end
  else if (SymbolName = '$Home_Mnemonic_Opening_Odds') then
  begin
    OutStr := UpperCase(CurrentGameData.HMnemonic);
    //Add the color/font commands
    if (PlaylistType = TICKER) then OutStr := OutStr + Delimiter + ODDS_FORMAT_TICKER
    else if (PlaylistType = BUG) then OutStr := OutStr + ODDS_FORMAT_BUG
    else if (PlaylistType = EXTRALINE) then OutStr := OutStr + ODDS_FORMAT_EXTRALINE;
    //Show the leader with odds data
    if ((CurrentGameData.HOpeningOdds <> -9999) AND (CurrentGameData.HOpeningOdds < 0) AND
       (CurrentGameData.HOpeningOdds < CurrentGameData.VOpeningOdds)) OR
       //Put in special case for same odds - show home team as favorite
       ((CurrentGameData.HOpeningOdds = CurrentGameData.VOpeningOdds) AND
        (CurrentGameData.HOpeningOdds <> -9999) AND
        (CurrentGameData.HOpeningOdds < 0)) then
    begin
      CurrentGameData.HOpeningOdds := (Trunc(CurrentGameData.HOpeningOdds*2))/2;
      if (CurrentGameData.HOpeningOdds = Trunc(CurrentGameData.HOpeningOdds)) then
        OutStr := OutStr + ' OPEN ' + FloatToStrF(CurrentGameData.HOpeningOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' OPEN ' + FloatToStrF(CurrentGameData.HOpeningOdds, ffFixed, 8, 1);
    end
    //Put in special case for even/pick-em
    else if ((CurrentGameData.HOpeningOdds = 0) AND (CurrentGameData.VOpeningOdds = 0)) OR
     ((CurrentGameData.HOpeningOdds = 100) AND (CurrentGameData.VOpeningOdds = 100)) then
      OutStr := OutStr + ' PICK';
  end

  //Names + current odds data
  else if (SymbolName = '$Visitor_Name_Current_Odds') then
  begin
    OutStr := UpperCase(CurrentGameData.VName);
    //Add the color/font commands
    if (PlaylistType = TICKER) then OutStr := OutStr + Delimiter + ODDS_FORMAT_TICKER
    else if (PlaylistType = BUG) then OutStr := OutStr + ODDS_FORMAT_BUG
    else if (PlaylistType = EXTRALINE) then OutStr := OutStr + ODDS_FORMAT_EXTRALINE;
    //Show the leader with odds data
    if (CurrentGameData.VCurrentOdds <> -9999) AND (CurrentGameData.VCurrentOdds < 0) AND
       (CurrentGameData.VCurrentOdds < CurrentGameData.HCurrentOdds) then
    begin
      CurrentGameData.VCurrentOdds := (Trunc(CurrentGameData.VCurrentOdds*2))/2;
      if (CurrentGameData.VCurrentOdds = Trunc(CurrentGameData.VCurrentOdds)) then
        OutStr := OutStr + ' ' + FloatToStrF(CurrentGameData.VCurrentOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' ' + FloatToStrF(CurrentGameData.VCurrentOdds, ffFixed, 8, 1);
    end;
  end
  else if (SymbolName = '$Visitor_Mnemonic_Current_Odds') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic);
    //Add the color/font commands
    if (PlaylistType = TICKER) then OutStr := OutStr + Delimiter + ODDS_FORMAT_TICKER
    else if (PlaylistType = BUG) then OutStr := OutStr + ODDS_FORMAT_BUG
    else if (PlaylistType = EXTRALINE) then OutStr := OutStr + ODDS_FORMAT_EXTRALINE;
    //Show the leader with odds data
    if (CurrentGameData.VCurrentOdds <> -9999) AND (CurrentGameData.VCurrentOdds < 0) AND
       (CurrentGameData.VCurrentOdds < CurrentGameData.HCurrentOdds) then
    begin
      CurrentGameData.VCurrentOdds := (Trunc(CurrentGameData.VCurrentOdds*2))/2;
      if (CurrentGameData.VCurrentOdds = Trunc(CurrentGameData.VCurrentOdds)) then
        OutStr := OutStr + ' ' + FloatToStrF(CurrentGameData.VCurrentOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' ' + FloatToStrF(CurrentGameData.VCurrentOdds, ffFixed, 8, 1);
    end;
  end
  else if (SymbolName = '$Home_Name_Current_Odds') then
  begin
    OutStr := UpperCase(CurrentGameData.HName);
    //Add the color/font commands
    if (PlaylistType = TICKER) then OutStr := OutStr + Delimiter + ODDS_FORMAT_TICKER
    else if (PlaylistType = BUG) then OutStr := OutStr + ODDS_FORMAT_BUG
    else if (PlaylistType = EXTRALINE) then OutStr := OutStr + ODDS_FORMAT_EXTRALINE;
    //Show the leader with odds data
    if ((CurrentGameData.HCurrentOdds <> -9999) AND (CurrentGameData.HCurrentOdds < 0) AND
       (CurrentGameData.HCurrentOdds < CurrentGameData.VCurrentOdds)) OR
       //Put in special case for same odds - show home team as favorite
       ((CurrentGameData.HCurrentOdds = CurrentGameData.VCurrentOdds) AND
        (CurrentGameData.HCurrentOdds <> -9999) AND
        (CurrentGameData.HCurrentOdds < 0)) then
    begin
      CurrentGameData.HCurrentOdds := (Trunc(CurrentGameData.HCurrentOdds*2))/2;
      if (CurrentGameData.HCurrentOdds = Trunc(CurrentGameData.HCurrentOdds)) then
        OutStr := OutStr + ' ' + FloatToStrF(CurrentGameData.HCurrentOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' ' + FloatToStrF(CurrentGameData.HCurrentOdds, ffFixed, 8, 1);
    end
    //Put in special case for even/pick-em
    else if ((CurrentGameData.HCurrentOdds = 0) AND (CurrentGameData.VCurrentOdds = 0)) OR
     ((CurrentGameData.HCurrentOdds = 100) AND (CurrentGameData.VCurrentOdds = 100)) then
      OutStr := OutStr + ' PICK';
  end
  else if (SymbolName = '$Home_Mnemonic_Current_Odds') then
  begin
    OutStr := UpperCase(CurrentGameData.HMnemonic);
    //Add the color/font commands
    if (PlaylistType = TICKER) then OutStr := OutStr + Delimiter + ODDS_FORMAT_TICKER
    else if (PlaylistType = BUG) then OutStr := OutStr + ODDS_FORMAT_BUG
    else if (PlaylistType = EXTRALINE) then OutStr := OutStr + ODDS_FORMAT_EXTRALINE;
    //Show the leader with odds data
    if ((CurrentGameData.HCurrentOdds <> -9999) AND (CurrentGameData.HCurrentOdds < 0) AND
       (CurrentGameData.HCurrentOdds < CurrentGameData.VCurrentOdds)) OR
       //Put in special case for same odds - show home team as favorite
       ((CurrentGameData.HCurrentOdds = CurrentGameData.VCurrentOdds) AND
        (CurrentGameData.HCurrentOdds <> -9999) AND
        (CurrentGameData.HCurrentOdds < 0)) then
    begin
      CurrentGameData.HCurrentOdds := (Trunc(CurrentGameData.HCurrentOdds*2))/2;
      if (CurrentGameData.HCurrentOdds = Trunc(CurrentGameData.HCurrentOdds)) then
        OutStr := OutStr + ' ' + FloatToStrF(CurrentGameData.HCurrentOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' ' + FloatToStrF(CurrentGameData.HCurrentOdds, ffFixed, 8, 1);
    end
    //Put in special case for even/pick-em
    else if ((CurrentGameData.HCurrentOdds = 0) AND (CurrentGameData.VCurrentOdds = 0)) OR
     ((CurrentGameData.HCurrentOdds = 100) AND (CurrentGameData.VCurrentOdds = 100)) then
      OutStr := OutStr + ' PICK';
  end

  //////////////////////////////////////////////////////////////////////////////
  //Names + opening AND current odds data
  //////////////////////////////////////////////////////////////////////////////
  else if (SymbolName = '$Visitor_Name_Opening_Current_Odds') then
  begin
    OutStr := UpperCase(CurrentGameData.VName);
    //Add the color/font commands
    if (PlaylistType = TICKER) then OutStr := OutStr + Delimiter + ODDS_FORMAT_TICKER
    else if (PlaylistType = BUG) then OutStr := OutStr + ODDS_FORMAT_BUG
    else if (PlaylistType = EXTRALINE) then OutStr := OutStr + ODDS_FORMAT_EXTRALINE;
    //Show the leader with odds data
    //Do opening odds
    if (CurrentGameData.VOpeningOdds <> -9999) AND (CurrentGameData.VOpeningOdds < 0) AND
       (CurrentGameData.VOpeningOdds < CurrentGameData.HOpeningOdds) then
    begin
      CurrentGameData.VOpeningOdds := (Trunc(CurrentGameData.VOpeningOdds*2))/2;
      if (CurrentGameData.VOpeningOdds = Trunc(CurrentGameData.VOpeningOdds)) then
        OutStr := OutStr + ' ' + FloatToStrF(CurrentGameData.VOpeningOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' ' + FloatToStrF(CurrentGameData.VOpeningOdds, ffFixed, 8, 1);
    end
    else OutStr := OutStr + '  ';
    //Add the current odds if applicable
    if (CurrentGameData.VCurrentOdds <> -9999) AND (CurrentGameData.VCurrentOdds < 0) AND
       (CurrentGameData.VCurrentOdds < CurrentGameData.HCurrentOdds) then
    begin
      CurrentGameData.VCurrentOdds := (Trunc(CurrentGameData.VCurrentOdds*2))/2;
      if (CurrentGameData.VCurrentOdds = Trunc(CurrentGameData.VCurrentOdds)) then
        OutStr := OutStr + ' / ' + FloatToStrF(CurrentGameData.VCurrentOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' / ' + FloatToStrF(CurrentGameData.VCurrentOdds, ffFixed, 8, 1);
    end;
  end
  else if (SymbolName = '$Visitor_Mnemonic_Opening_Current_Odds') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic);
    //Add the color/font commands
    if (PlaylistType = TICKER) then OutStr := OutStr + Delimiter + ODDS_FORMAT_TICKER
    else if (PlaylistType = BUG) then OutStr := OutStr + ODDS_FORMAT_BUG
    else if (PlaylistType = EXTRALINE) then OutStr := OutStr + ODDS_FORMAT_EXTRALINE;
    //Show the leader with odds data
    //Do opening odds
    if (CurrentGameData.VOpeningOdds <> -9999) AND (CurrentGameData.VOpeningOdds < 0) AND
       (CurrentGameData.VOpeningOdds < CurrentGameData.HOpeningOdds) then
    begin
      CurrentGameData.VOpeningOdds := (Trunc(CurrentGameData.VOpeningOdds*2))/2;
      if (CurrentGameData.VOpeningOdds = Trunc(CurrentGameData.VOpeningOdds)) then
        OutStr := OutStr + ' ' + FloatToStrF(CurrentGameData.VOpeningOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' ' + FloatToStrF(CurrentGameData.VOpeningOdds, ffFixed, 8, 1);
    end
    else OutStr := OutStr + '  ';
    //Add the current odds if applicable
    if (CurrentGameData.VCurrentOdds <> -9999) AND (CurrentGameData.VCurrentOdds < 0) AND
       (CurrentGameData.VCurrentOdds < CurrentGameData.HCurrentOdds) then
    begin
      CurrentGameData.VCurrentOdds := (Trunc(CurrentGameData.VCurrentOdds*2))/2;
      if (CurrentGameData.VCurrentOdds = Trunc(CurrentGameData.VCurrentOdds)) then
        OutStr := OutStr + ' / ' + FloatToStrF(CurrentGameData.VCurrentOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' / ' + FloatToStrF(CurrentGameData.VCurrentOdds, ffFixed, 8, 1);
    end;
  end
  else if (SymbolName = '$Home_Name_Opening_Current_Odds') then
  begin
    OutStr := UpperCase(CurrentGameData.HName);
    //Add the color/font commands
    if (PlaylistType = TICKER) then OutStr := OutStr + Delimiter + ODDS_FORMAT_TICKER
    else if (PlaylistType = BUG) then OutStr := OutStr + ODDS_FORMAT_BUG
    else if (PlaylistType = EXTRALINE) then OutStr := OutStr + ODDS_FORMAT_EXTRALINE;
    //Show the leader with odds data
    //Do opening odds
    if ((CurrentGameData.HOpeningOdds <> -9999) AND (CurrentGameData.HOpeningOdds < 0) AND
       (CurrentGameData.HOpeningOdds < CurrentGameData.VOpeningOdds)) OR
       //Put in special case for same odds - show home team as favorite
       ((CurrentGameData.HOpeningOdds = CurrentGameData.VOpeningOdds) AND
        (CurrentGameData.HOpeningOdds <> -9999) AND
        (CurrentGameData.HOpeningOdds < 0)) then
    begin
      CurrentGameData.HOpeningOdds := (Trunc(CurrentGameData.HOpeningOdds*2))/2;
      if (CurrentGameData.HOpeningOdds = Trunc(CurrentGameData.HOpeningOdds)) then
        OutStr := OutStr + ' ' + FloatToStrF(CurrentGameData.HOpeningOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' ' + FloatToStrF(CurrentGameData.HOpeningOdds, ffFixed, 8, 1);
    end
    //Put in special case for even/pick-em
    else if ((CurrentGameData.HOpeningOdds = 0) AND (CurrentGameData.VOpeningOdds = 0)) OR
     ((CurrentGameData.HOpeningOdds = 100) AND (CurrentGameData.VOpeningOdds = 100)) then
      OutStr := OutStr + ' PICK'
    else OutStr := OutStr + '  ';
    //Add the current odds if applicable
    if ((CurrentGameData.HCurrentOdds <> -9999) AND (CurrentGameData.HCurrentOdds < 0) AND
       (CurrentGameData.HCurrentOdds < CurrentGameData.VCurrentOdds)) OR
       //Put in special case for same odds - show home team as favorite
       ((CurrentGameData.HCurrentOdds = CurrentGameData.VCurrentOdds) AND
        (CurrentGameData.HCurrentOdds <> -9999) AND
        (CurrentGameData.HCurrentOdds < 0)) then
    begin
      CurrentGameData.HCurrentOdds := (Trunc(CurrentGameData.HCurrentOdds*2))/2;
      if (CurrentGameData.HCurrentOdds = Trunc(CurrentGameData.HCurrentOdds)) then
        OutStr := OutStr + ' / ' + FloatToStrF(CurrentGameData.HCurrentOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' / ' + FloatToStrF(CurrentGameData.HCurrentOdds, ffFixed, 8, 1);
    end
    //Put in special case for even/pick-em
    else if ((CurrentGameData.HCurrentOdds = 0) AND (CurrentGameData.VCurrentOdds = 0)) OR
     ((CurrentGameData.HCurrentOdds = 100) AND (CurrentGameData.VCurrentOdds = 100)) then
      OutStr := OutStr + ' / PICK';
  end
  else if (SymbolName = '$Home_Mnemonic_Opening_Current_Odds') then
  begin
    OutStr := UpperCase(CurrentGameData.HMnemonic);
    //Add the color/font commands
    if (PlaylistType = TICKER) then OutStr := OutStr + Delimiter + ODDS_FORMAT_TICKER
    else if (PlaylistType = BUG) then OutStr := OutStr + ODDS_FORMAT_BUG
    else if (PlaylistType = EXTRALINE) then OutStr := OutStr + ODDS_FORMAT_EXTRALINE;
    //Show the leader with odds data
    //Do opening odds
    if ((CurrentGameData.HOpeningOdds <> -9999) AND (CurrentGameData.HOpeningOdds < 0) AND
       (CurrentGameData.HOpeningOdds < CurrentGameData.VOpeningOdds)) OR
       //Put in special case for same odds - show home team as favorite
       ((CurrentGameData.HOpeningOdds = CurrentGameData.VOpeningOdds) AND
        (CurrentGameData.HOpeningOdds <> -9999) AND
        (CurrentGameData.HOpeningOdds < 0)) then
    begin
      CurrentGameData.HOpeningOdds := (Trunc(CurrentGameData.HOpeningOdds*2))/2;
      if (CurrentGameData.HOpeningOdds = Trunc(CurrentGameData.HOpeningOdds)) then
        OutStr := OutStr + ' OPEN ' + FloatToStrF(CurrentGameData.HOpeningOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' OPEN ' + FloatToStrF(CurrentGameData.HOpeningOdds, ffFixed, 8, 1);
    end
    //Put in special case for even/pick-em
    else if ((CurrentGameData.HOpeningOdds = 0) AND (CurrentGameData.VOpeningOdds = 0)) OR
     ((CurrentGameData.HOpeningOdds = 100) AND (CurrentGameData.VOpeningOdds = 100)) then
      OutStr := OutStr + ' PICK'
    else OutStr := OutStr + '  ';
    //Add the current odds if applicable
    //Show the leader with odds data
    if ((CurrentGameData.HCurrentOdds <> -9999) AND (CurrentGameData.HCurrentOdds < 0) AND
       (CurrentGameData.HCurrentOdds < CurrentGameData.VCurrentOdds)) OR
       //Put in special case for same odds - show home team as favorite
       ((CurrentGameData.HCurrentOdds = CurrentGameData.VCurrentOdds) AND
        (CurrentGameData.HCurrentOdds <> -9999) AND
        (CurrentGameData.HCurrentOdds < 0)) then
    begin
      CurrentGameData.HCurrentOdds := (Trunc(CurrentGameData.HCurrentOdds*2))/2;
      if (CurrentGameData.HCurrentOdds = Trunc(CurrentGameData.HCurrentOdds)) then
        OutStr := OutStr + ' / ' + FloatToStrF(CurrentGameData.HCurrentOdds, ffFixed, 8, 0)
      else
        OutStr := OutStr + ' / ' + FloatToStrF(CurrentGameData.HCurrentOdds, ffFixed, 8, 1);
    end
    //Put in special case for even/pick-em
    else if ((CurrentGameData.HCurrentOdds = 0) AND (CurrentGameData.VCurrentOdds = 0)) OR
     ((CurrentGameData.HCurrentOdds = 100) AND (CurrentGameData.VCurrentOdds = 100)) then
      OutStr := OutStr + ' / PICK';
  end
  //////////////////////////////////////////////////////////////////////////////

  //Visitor winner animation
  else if (SymbolName = '$Winner_Animation_Visitor') then
  begin
    if (CurrentGameData.State = 3) then
    begin
      if (CurrentGameData.VScore > CurrentGameData.HScore) then OutStr := 'WINNER3000'
      else if (CurrentGameData.VScore <= CurrentGameData.HScore) then OutStr := 'CLEAR';
    end
    else OutStr := 'CLEAR';
  end
  //Home winner animation
  else if (SymbolName = '$Winner_Animation_Home') then
  begin
    if (CurrentGameData.State = 3) then
    begin
      if (CurrentGameData.HScore > CurrentGameData.VScore) then OutStr := 'WINNER3000'
      else if (CurrentGameData.HScore <= CurrentGameData.VScore) then OutStr := 'CLEAR';
    end
    else OutStr := 'CLEAR';
  end
  //Visitor winner indicator
  else if (SymbolName = '$Winner_Indicator_Visitor') then
  begin
    if (CurrentGameData.State = 3) then
    begin
      if (CurrentGameData.VScore > CurrentGameData.HScore) then
      Case PlaylistType of
        TICKER: OutStr := 'WINNER_INDICATOR';
        BUG: OutStr := 'WINNER_INDICATOR_BUG';
        EXTRALINE: OutStr := 'WINNER_INDICATOR_EL';
      end
      else if (CurrentGameData.VScore <= CurrentGameData.HScore) then OutStr := 'CLEAR';
    end
    else OutStr := 'CLEAR';
  end
  //Home winner indicator
  else if (SymbolName = '$Winner_Indicator_Home') then
  begin
    if (CurrentGameData.State = 3) then
    begin
      if (CurrentGameData.VScore < CurrentGameData.HScore) then
      Case PlaylistType of
        TICKER: OutStr := 'WINNER_INDICATOR';
        BUG: OutStr := 'WINNER_INDICATOR_BUG';
        EXTRALINE: OutStr := 'WINNER_INDICATOR_EL';
      end
      else if (CurrentGameData.HScore <= CurrentGameData.VScore) then OutStr := 'CLEAR';
    end
    else OutStr := 'CLEAR';
  end

  //Visitor winner indicator - bug
  else if (SymbolName = '$Winner_Indicator_Visitor_Bug') then
  begin
    if (CurrentGameData.State = 3) then
    begin
      if (CurrentGameData.VScore > CurrentGameData.HScore) then OutStr := 'WINNER_INDICATOR_BUG'
      else if (CurrentGameData.VScore <= CurrentGameData.HScore) then OutStr := 'CLEAR';
    end
    else OutStr := 'CLEAR';
  end
  //Home winner indicator - bug
  else if (SymbolName = '$Winner_Indicator_Home_Bug') then
  begin
    if (CurrentGameData.State = 3) then
    begin
      if (CurrentGameData.HScore > CurrentGameData.VScore) then OutStr := 'WINNER_INDICATOR_BUG'
      else if (CurrentGameData.HScore <= CurrentGameData.VScore) then OutStr := 'CLEAR';
    end
    else OutStr := 'CLEAR';
  end

  //Visitor winner indicator - extraline
  else if (SymbolName = '$Winner_Indicator_Visitor_ExtraLine') then
  begin
    if (CurrentGameData.State = 3) then
    begin
      if (CurrentGameData.VScore > CurrentGameData.HScore) then OutStr := 'WINNER_INDICATOR_EL'
      else if (CurrentGameData.VScore <= CurrentGameData.HScore) then OutStr := 'CLEAR';
    end
    else OutStr := 'CLEAR';
  end
  //Home winner indicator - bug
  else if (SymbolName = '$Winner_Indicator_Home_ExtraLine') then
  begin
    if (CurrentGameData.State = 3) then
    begin
      if (CurrentGameData.HScore > CurrentGameData.VScore) then OutStr := 'WINNER_INDICATOR_EL'
      else if (CurrentGameData.HScore <= CurrentGameData.VScore) then OutStr := 'CLEAR';
    end
    else OutStr := 'CLEAR';
  end

  //Visitor winner animation (Manual)
  else if (SymbolName = '$Winner_Animation_Visitor_Manual') then
  begin
    Case PlaylistType of
      TICKER: if (TickerRecPtr^.UserData[24] = 'TRUE') then OutStr := 'WINNER3000'
              else OutStr := 'CLEAR';
      BUG: if (BugRecPtr^.UserData[24] = 'TRUE') then OutStr := 'WINNER3000'
              else OutStr := 'CLEAR';
      EXTRALINE: if (ExtraLineRecPtr^.UserData[24] = 'TRUE') then OutStr := 'WINNER3000'
              else OutStr := 'CLEAR';
    end;
  end
  //Home winner animation (Manual)
  else if (SymbolName = '$Winner_Animation_Home_Manual') then
  begin
    Case PlaylistType of
      TICKER: if (TickerRecPtr^.UserData[25] = 'TRUE') then OutStr := 'WINNER3000'
              else OutStr := 'CLEAR';
      BUG: if (BugRecPtr^.UserData[25] = 'TRUE') then OutStr := 'WINNER3000'
              else OutStr := 'CLEAR';
      EXTRALINE: if (ExtraLineRecPtr^.UserData[25] = 'TRUE') then OutStr := 'WINNER3000'
              else OutStr := 'CLEAR';
    end;
  end
  //Visitor winner indicator (Manual)
  else if (SymbolName = '$Winner_Indicator_Visitor_Manual') then
  begin
    Case PlaylistType of
      TICKER: if (TickerRecPtr^.UserData[24] = 'TRUE') then OutStr := 'WINNER_INDICATOR'
              else OutStr := 'CLEAR';
      BUG: if (BugRecPtr^.UserData[24] = 'TRUE') then OutStr := 'WINNER_INDICATOR_BUG'
              else OutStr := 'CLEAR';
      EXTRALINE: if (ExtraLineRecPtr^.UserData[24] = 'TRUE') then OutStr := 'WINNER_INDICATOR_EL'
              else OutStr := 'CLEAR';
    end;
  end
  //Home winner indicator (Manual)
  else if (SymbolName = '$Winner_Indicator_Home_Manual') then
  begin
    Case PlaylistType of
      TICKER: if (TickerRecPtr^.UserData[25] = 'TRUE') then OutStr := 'WINNER_INDICATOR'
              else OutStr := 'CLEAR';
      BUG: if (BugRecPtr^.UserData[25] = 'TRUE') then OutStr := 'WINNER_INDICATOR_BUG'
              else OutStr := 'CLEAR';
      EXTRALINE: if (ExtraLineRecPtr^.UserData[25] = 'TRUE') then OutStr := 'WINNER_INDICATOR_EL'
              else OutStr := 'CLEAR';
    end;
  end

  //Next page indicator
  else if (SymbolName = '$Next_Page_Icon') then
  begin
    Case PlaylistType of
      TICKER: if (TickerRecPtr^.UserData[23] = 'TRUE') then OutStr := 'NEXTPAGE3000'
              else OutStr := 'CLEAR';
      BUG: if (BugRecPtr^.UserData[23] = 'TRUE') then OutStr := 'NEXTPAGE3000'
              else OutStr := 'CLEAR';
      EXTRALINE: if (ExtraLineRecPtr^.UserData[23] = 'TRUE') then OutStr := 'NEXTPAGE3000'
              else OutStr := 'CLEAR';
    end;
  end

  //Football field position
  else if (SymbolName = '$Football_Field_Position') then
  begin
    if (GetFootballGameSituationData(CurrentGameData).VisitorHasPossession = TRUE) then
    begin
      //Set tab stops
      TempStr := '[x 1 7 14 21 27 34 40 47 53 60 67 74 80 87 93 100 106 113 119 126 133 139 146 152]';
      //Set tab position
      //NFL/CFB - Field is 100 yards long
      if (CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'CFB') then
      begin
        TempStr := TempStr + '[t ';
        //Visiting team runs from tab 0 to tab 20
        TempStr := TempStr + IntToStr(20 - (GetFootballGameSituationData(CurrentGameData).YardsFromGoal DIV 5));
        TempStr := TempStr + ']';
      end
      //CFL - Field is 110 yards long
      else if (CurrentGameData.League = 'CFL') then
      begin
        TempStr := TempStr + '[t ';
        //Visiting team runs from tab 0 to tab 20
        TempStr := TempStr + IntToStr(20 - ((GetFootballGameSituationData(CurrentGameData).YardsFromGoal*2) DIV 11));
        TempStr := TempStr + ']';
      end;
      //Add arrow indicator - point right for visiting team
      TempStr := TempStr + FIELDARROWRIGHT;
    end
    else if (GetFootballGameSituationData(CurrentGameData).HomeHasPossession = TRUE) then
    begin
      //Set tab stops
      TempStr := '[x 1 7 14 21 27 34 40 47 53 60 67 74 80 87 93 100 106 113 119 126 133 139 146 152]';
      //Set tab position
      //NFL/CFB - Field is 100 yards long
      if (CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'CFB') then
      begin
        TempStr := TempStr + '[t ';
        //Visiting team runs from tab 0 to tab 20
        TempStr := TempStr + IntToStr(2 + (GetFootballGameSituationData(CurrentGameData).YardsFromGoal DIV 5));
        TempStr := TempStr + ']';
      end
      //CFL - Field is 110 yards long
      else if (CurrentGameData.League = 'CFL') then
      begin
        TempStr := TempStr + '[t ';
        //Visiting team runs from tab 0 to tab 20
        TempStr := TempStr + IntToStr(2 + ((GetFootballGameSituationData(CurrentGameData).YardsFromGoal*2) DIV 11));
        TempStr := TempStr + ']';
      end;
      //Add arrow indicator - point right for visiting team
      TempStr := TempStr + FIELDARROWLEFT;
    end
    else TempStr := ' ';
    OutStr := TempStr;
  end

  //Yard marker indicator for center of football field position grid
  else if (SymbolName = '$Football_Field_Center_Yard') then
  begin
    if (CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'CFB') then OutStr := '50'
    else if (CurrentGameData.League = 'CFL') then OutStr := '55'
    else OutStr := ' ';
  end

  //Progress bar
  else if (SymbolName = '$Progress_Bar') then
  begin
    OutStr := IntToStr(GetProgressIndicatorValue(CurrentEntryIndex));
  end;

  //Process command string to see if there are any switches for mixed case or upper case
  OutStr2 := '';
  if (Length(OutStr) > 3) then
  begin
    SwitchPosMixed := Pos('^^M', OutStr);
    SwitchPosUpper := Pos('^^U', OutStr);
    if (SwitchPosMixed > 1) then
    begin
      //Copy any leading upper case characters
      for i := 1 to SwitchPosMixed-1 do
        if (Ord(OutStr[i]) < 255) then OutStr2 := OutStr2 + UpperCase(OutStr[i])
        else OutStr2 := OutStr2 + OutStr[i];
      //There is a return to upper case, so switch back
      if (SwitchPosUpper > 3) then
      begin
        for i := SwitchPosMixed+3 to SwitchPosUpper-1 do
          OutStr2 := OutStr2 + OutStr[i];
        for i := SwitchPosUpper+3 to Length(OutStr) do
        begin
          if (Ord(OutStr[i]) < 255) then OutStr2 := OutStr2 + UpperCase(OutStr[i])
          else OutStr2 := OutStr2 + OutStr[i];
        end
      end
      //No trailing upper case characters, so just copy reamaining characters as mixed case
      else begin
        for i := SwitchPosMixed+3 to Length(OutStr) do
          OutStr2 := OutStr2 + OutStr[i];
      end;
    end
    //Special case if switch to mixed case is the first character
    else if (SwitchPosMixed = 1) then
    begin
      //Copy any leading upper case characters
      //There is a return to upper case, so switch back
      if (SwitchPosUpper > 3) then
      begin
        for i := SwitchPosMixed+3 to SwitchPosUpper-1 do
          OutStr2 := OutStr2 + OutStr[i];
        for i := SwitchPosUpper+3 to Length(OutStr) do
        begin
          if (Ord(OutStr[i]) < 255) then OutStr2 := OutStr2 + UpperCase(OutStr[i])
          else OutStr2 := OutStr2 + OutStr[i];
        end;
      end
      //No trailing upper case characters, so just copy reamaining characters as mixed case
      else begin
        for i := SwitchPosMixed+3 to Length(OutStr) do
          OutStr2 := OutStr2 + OutStr[i];
      end;
    end
    else if (SwitchPosMixed = 0) then
    begin
      OutStr2 := '';
      for i := 1 to Length(OutStr) do
      begin
        if (Ord(OutStr[i]) < 255) then OutStr2 := OutStr2 + ANSIUpperCase(OutStr[i])
        else OutStr2 := OutStr2 + OutStr[i];
      end;
      OutStr := OutStr2;
    end;
  end
  else OutStr2 := OutStr;

  //Process command string to see if there are any switches for going to black characters
  //Modified to support 1-line ticker; adds bias to use larger embedded font characters
  if (OneLineModeEnable) then OneLineBias := 30
  else OneLineBias := 0;
  if (Length(OutStr) > 3) then
  //Repeat as necessary to cover all switch pairs
  repeat
    OutStr := OutStr2;
    OutStr2 := '';
    SwitchPosStartBlack := Pos('^^SB', OutStr);
    SwitchPosEndBlack := Pos('^^EB', OutStr);
    if (SwitchPosStartBlack > 1) AND (SwitchPosEndBlack > SwitchPosStartBlack) then
    begin
      //Copy any leading standard characters
      for i := 1 to SwitchPosStartBlack-1 do OutStr2 := OutStr2 + OutStr[i];
      //There is a return to normal characters, so switch back
      if (SwitchPosEndBlack > 4) then
      begin
        //Pad with leading blank
        OutStr2 := OutStr2 + Chr(190+OneLineBias);
        //Convert to the special characters
        for i := SwitchPosStartBlack+4 to SwitchPosEndBlack-1 do
        begin
          if (OutStr[i] = '0') then OutStr2 := OutStr2 + Chr(180+OneLineBias)
          else if (OutStr[i] = '1') then OutStr2 := OutStr2 + Chr(181+OneLineBias)
          else if (OutStr[i] = '2') then OutStr2 := OutStr2 + Chr(182+OneLineBias)
          else if (OutStr[i] = '3') then OutStr2 := OutStr2 + Chr(183+OneLineBias)
          else if (OutStr[i] = '4') then OutStr2 := OutStr2 + Chr(184+OneLineBias)
          else if (OutStr[i] = '5') then OutStr2 := OutStr2 + Chr(185+OneLineBias)
          else if (OutStr[i] = '6') then OutStr2 := OutStr2 + Chr(186+OneLineBias)
          else if (OutStr[i] = '7') then OutStr2 := OutStr2 + Chr(187+OneLineBias)
          else if (OutStr[i] = '8') then OutStr2 := OutStr2 + Chr(188+OneLineBias)
          else if (OutStr[i] = '9') then OutStr2 := OutStr2 + Chr(189+OneLineBias)
          else if (OutStr[i] = 'T') then OutStr2 := OutStr2 + Chr(191+OneLineBias);
        end;
        //Pad with trailing blank
        OutStr2 := OutStr2 + Chr(190+OneLineBias);
        for i := SwitchPosEndBlack+4 to Length(OutStr) do
          OutStr2 := OutStr2 + OutStr[i];
      end
      //No trailing upper case characters, so just copy remaining characters
      else begin
        //Pad with leading blank
        OutStr2 := OutStr2 + Chr(190+OneLineBias);
        for i := SwitchPosStartBlack+4 to Length(OutStr) do
        begin
          if (OutStr[i] = '0') then OutStr2 := OutStr2 + Chr(180+OneLineBias)
          else if (OutStr[i] = '1') then OutStr2 := OutStr2 + Chr(181+OneLineBias)
          else if (OutStr[i] = '2') then OutStr2 := OutStr2 + Chr(182+OneLineBias)
          else if (OutStr[i] = '3') then OutStr2 := OutStr2 + Chr(183+OneLineBias)
          else if (OutStr[i] = '4') then OutStr2 := OutStr2 + Chr(184+OneLineBias)
          else if (OutStr[i] = '5') then OutStr2 := OutStr2 + Chr(185+OneLineBias)
          else if (OutStr[i] = '6') then OutStr2 := OutStr2 + Chr(186+OneLineBias)
          else if (OutStr[i] = '7') then OutStr2 := OutStr2 + Chr(187+OneLineBias)
          else if (OutStr[i] = '8') then OutStr2 := OutStr2 + Chr(188+OneLineBias)
          else if (OutStr[i] = '9') then OutStr2 := OutStr2 + Chr(189+OneLineBias)
          else if (OutStr[i] = 'T') then OutStr2 := OutStr2 + Chr(191+OneLineBias);
        end;
        //Pad with trailing blank
        OutStr2 := OutStr2 + Chr(190+OneLineBias);
      end;
    end
    //Special case if switch to mixed case is the first character
    else if (SwitchPosStartBlack = 1) then
    begin
      //There is a return to upper case, so switch back
      if (SwitchPosEndBlack > 4) then
      begin
        //Pad with leading blank
        OutStr2 := OutStr2 + Chr(190+OneLineBias);
        //Convert to the special characters
        for i := SwitchPosStartBlack+4 to SwitchPosEndBlack-1 do
        begin
          if (OutStr[i] = '0') then OutStr2 := OutStr2 + Chr(180+OneLineBias)
          else if (OutStr[i] = '1') then OutStr2 := OutStr2 + Chr(181+OneLineBias)
          else if (OutStr[i] = '2') then OutStr2 := OutStr2 + Chr(182+OneLineBias)
          else if (OutStr[i] = '3') then OutStr2 := OutStr2 + Chr(183+OneLineBias)
          else if (OutStr[i] = '4') then OutStr2 := OutStr2 + Chr(184+OneLineBias)
          else if (OutStr[i] = '5') then OutStr2 := OutStr2 + Chr(185+OneLineBias)
          else if (OutStr[i] = '6') then OutStr2 := OutStr2 + Chr(186+OneLineBias)
          else if (OutStr[i] = '7') then OutStr2 := OutStr2 + Chr(187+OneLineBias)
          else if (OutStr[i] = '8') then OutStr2 := OutStr2 + Chr(188+OneLineBias)
          else if (OutStr[i] = '9') then OutStr2 := OutStr2 + Chr(189+OneLineBias)
          else if (OutStr[i] = 'T') then OutStr2 := OutStr2 + Chr(191+OneLineBias);
        end;
        //Pad with trailing blank
        OutStr2 := OutStr2 + Chr(190+OneLineBias);
        for i := SwitchPosEndBlack+4 to Length(OutStr) do
          OutStr2 := OutStr2 + OutStr[i];
      end
      //No trailing upper case characters, so just copy reamaining characters as special characters
      else begin
        //Pad with leading blank
        OutStr2 := OutStr2 + Chr(190+OneLineBias);
        for i := SwitchPosStartBlack+4 to Length(OutStr) do
        begin
          if (OutStr[i] = '0') then OutStr2 := OutStr2 + Chr(180+OneLineBias)
          else if (OutStr[i] = '1') then OutStr2 := OutStr2 + Chr(181+OneLineBias)
          else if (OutStr[i] = '2') then OutStr2 := OutStr2 + Chr(182+OneLineBias)
          else if (OutStr[i] = '3') then OutStr2 := OutStr2 + Chr(183+OneLineBias)
          else if (OutStr[i] = '4') then OutStr2 := OutStr2 + Chr(184+OneLineBias)
          else if (OutStr[i] = '5') then OutStr2 := OutStr2 + Chr(185+OneLineBias)
          else if (OutStr[i] = '6') then OutStr2 := OutStr2 + Chr(186+OneLineBias)
          else if (OutStr[i] = '7') then OutStr2 := OutStr2 + Chr(187+OneLineBias)
          else if (OutStr[i] = '8') then OutStr2 := OutStr2 + Chr(188+OneLineBias)
          else if (OutStr[i] = '9') then OutStr2 := OutStr2 + Chr(189+OneLineBias)
          else if (OutStr[i] = 'T') then OutStr2 := OutStr2 + Chr(191+OneLineBias);
        end;
        //Pad with trailing blank
        OutStr2 := OutStr2 + Chr(190+OneLineBias);
      end;
    end
    else if (SwitchPosStartBlack = 0) then OutStr2 := OutStr;
  Until (Pos('^^SB', OutStr) = 0);

  //Return
  GetValueOfSymbol := OutStr2;
end;

////////////////////////////////////////////////////////////////////////////////
// ENGINE COMMAND STRING FORMATTING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Function to get & return text strings to be used for line-ups
function TEngineInterface.GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
var
  i,j: SmallInt;
  OutRec: LineupText;
  TickerRecPtrCurrent, TickerRecPtrPrevious,TickerRecPtrNext: ^TickerRec;
  PreviousEntryIndex, NextEntryIndex: SmallInt;
  CurrentEntryLeague, PreviousEntryLeague, NextEntryLeague: String;
  CurrentEntryMnemonic, NextEntryMnemonic: String;
  HeaderIndex, CollectionItemCounter: SmallInt;
  OneLeagueOnly: Boolean;
  FirstLeague: String;
  ParentFlag: Boolean;
  CurrentLineupTickerEntryIndex: SmallInt;
begin
  //Always set flag to do line-up
  OutRec.UseLineup := TRUE;

  //Get text for line-ups
  if (OutRec.UseLineup = TRUE) AND (CurrentTickerEntryIndex <> NOENTRYINDEX) then
  begin
    //Init
    HeaderIndex := 1;
    CollectionItemCounter := 1;

    //Get current entry types
    CurrentLineupTickerEntryIndex := CurrentTickerEntryIndex;
    TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);

    //Make sure record is enabled and within time window
    j := 0;
    While ((TickerRecPtrCurrent^.Enabled = FALSE) OR (TickerRecPtrCurrent^.StartEnableDateTime > Now) OR
          (TickerRecPtrCurrent^.EndEnableDateTime <= Now) OR (TickerRecPtrCurrent^.League = 'NONE')) AND
          (j < Ticker_Collection.Count) do
    begin
      Inc(CurrentLineupTickerEntryIndex);
      if (CurrentLineupTickerEntryIndex = Ticker_Collection.Count) then
      begin
        CurrentLineupTickerEntryIndex := 0;
      end;
      TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);
      Inc(j);
    end;

    CurrentEntryLeague :=  TickerRecPtrCurrent^.League;

    Case TickerMode of
     -1: CurrentEntryMnemonic :=  TickerRecPtrCurrent^.Subleague_Mnemonic_Standard;
      1: CurrentEntryMnemonic :=  TickerRecPtrCurrent^.Mnemonic_LiveEvent;
    end;
    NextEntryIndex := CurrentLineupTickerEntryIndex+1;
    //Get type of next entry
    if (NextEntryIndex = Ticker_Collection.Count) then NextEntryIndex := 0;

    //Set first entry
    OutRec.TextFields[HeaderIndex] :=
      MainForm.GetAutomatedLeagueDisplayMnemonic(CurrentEntryMnemonic);

    //Init flag to indicate same parent
    ParentFlag  := TRUE;

    //Get remaining entries
    repeat
      TickerRecPtrNext := Ticker_Collection.At(NextEntryIndex);
      NextEntryLeague := TickerRecPtrNext^.League;
      Case TickerMode of
       -1: NextEntryMnemonic := TickerRecPtrNext^.Subleague_Mnemonic_Standard;
        1: NextEntryMnemonic := TickerRecPtrNext^.Mnemonic_LiveEvent;
      end;

      //If new league or child to prior parent, set text field; otherwise, continue iterating through
      if (((NextEntryMnemonic <> CurrentEntryMnemonic) AND (ParentFlag = TRUE)) OR
           (NextEntryLeague <> CurrentEntryLeague)) AND
         (TickerRecPtrNext^.Enabled = TRUE) AND (TickerRecPtrNext^.StartEnableDateTime <= Now) AND
         (TickerRecPtrNext^.EndEnableDateTime > Now) AND (j < Ticker_Collection.Count) AND
         (TickerRecPtrNext^.League <> 'NONE') then
      begin
        Inc(HeaderIndex);
        //Check if same parent
        if (CurrentEntryLeague = NextEntryLeague) then
        begin
          //Same parent, so use subleague
          OutRec.TextFields[HeaderIndex] := '@' +
            MainForm.GetAutomatedLeagueDisplayMnemonic(NextEntryMnemonic);
        end
        else begin
          //Use parent league for field and clear flag
          OutRec.TextFields[HeaderIndex] :=
            MainForm.GetAutomatedLeagueDisplayMnemonic(NextEntryLeague);
          ParentFlag := FALSE;
        end;
        CurrentEntryLeague := NextEntryLeague;
        CurrentEntryMnemonic := NextEntryMnemonic;
      end;

      if (NextEntryIndex = Ticker_Collection.Count-1) then NextEntryIndex := 0
      else Inc(NextEntryIndex);
      Inc(CollectionItemCounter);
    until (HeaderIndex = 5) OR (CollectionItemCounter = Ticker_Collection.Count*3);

    //Repeat fields if not enough after iterating
    if (HeaderIndex = 1) then
    begin
      //Don't use lineups if only one category
//      OutRec.TextFields[2] :=  ' ';
//      OutRec.TextFields[3] :=  ' ';
//      OutRec.TextFields[4] :=  ' ';
//      OutRec.TextFields[5] :=  ' ';
      OutRec.TextFields[2] :=  OutRec.TextFields[1];
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
      OutRec.TextFields[5] :=  OutRec.TextFields[1];
    end
    else if (HeaderIndex = 2) then
    begin
      //Don't use lineups if only two categories
//      OutRec.TextFields[3] :=  ' ';
//      OutRec.TextFields[4] :=  ' ';
//      OutRec.TextFields[5] :=  ' ';
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[2];
      OutRec.TextFields[5] :=  OutRec.TextFields[1];
    end
    else if (HeaderIndex = 3) then
    begin
//      OutRec.TextFields[4] :=  ' ';
//      OutRec.TextFields[5] :=  ' ';
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
      OutRec.TextFields[5] :=  OutRec.TextFields[2];
    end
    else if (HeaderIndex = 4) then
    begin
//      OutRec.TextFields[5] :=  ' ';
      OutRec.TextFields[5] :=  OutRec.TextFields[1];
    end;
  end
  else
    for i := 1 to 5 do OutRec.TextFields[i] := ' ';
  //Return
  for i := 1 to 5 do if (OutRec.TextFields[i] = '') then OutRec.TextFields[i] := ' ';
  GetLineupText := OutRec;
end;

//Function to get & return value for main ticker progress bar
function TEngineInterface.GetProgressIndicatorValue(CurrentTickerEntryIndex: SmallInt): SmallInt;
var
  i,j: SmallInt;
  OutVal: SmallInt;
  TickerRecPtrCurrent, TickerRecPtrPrevious,TickerRecPtrNext: ^TickerRec;
  PreviousEntryIndex, NextEntryIndex: SmallInt;
  PreviousEntryLeague, CurrentEntryLeague, NextEntryLeague: String;
  PreviousEntryMnemonic, CurrentEntryMnemonic, NextEntryMnemonic: String;
  CollectionItemCounter: SmallInt;
  OneLeagueOnly: Boolean;
  FirstLeague: String;
  ParentFlag: Boolean;
  CurrentLineupTickerEntryIndex: SmallInt;
  BackwardCount, ForwardCount: SmallInt;
  SameTypeCount: SmallInt;
  FoundNextType: Boolean;
begin
  //Get value for progress indicator
  //Init
  CollectionItemCounter := 1;
  OutVal := 100;
  BackwardCount := 0;
  ForwardCount := 0;

  //Make sure the index is specified
  if (CurrentTickerEntryIndex <> NOENTRYINDEX) AND (Ticker_Collection.Count > 0) AND
     (CurrentTickerEntryIndex < Ticker_Collection.Count) then
  begin
    //Get current entry types
    CurrentLineupTickerEntryIndex := CurrentTickerEntryIndex;
    TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);

    //Make sure record is enabled and within time window
    j := 0;
    While ((TickerRecPtrCurrent^.Enabled = FALSE) OR (TickerRecPtrCurrent^.StartEnableDateTime > Now) OR
          (TickerRecPtrCurrent^.EndEnableDateTime <= Now) OR (TickerRecPtrCurrent^.League = 'NONE')) AND
          (j < Ticker_Collection.Count) do
    begin
      Inc(CurrentLineupTickerEntryIndex);
      if (CurrentLineupTickerEntryIndex = Ticker_Collection.Count) then
      begin
        CurrentLineupTickerEntryIndex := 0;
      end;
      TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);
      Inc(j);
    end;

    //Get data for current entry
    CurrentEntryLeague :=  TickerRecPtrCurrent^.League;
    CurrentEntryMnemonic :=  TickerRecPtrCurrent^.Subleague_Mnemonic_Standard;
    //Get index of next entry
    NextEntryIndex := CurrentLineupTickerEntryIndex+1;
    if (NextEntryIndex = Ticker_Collection.Count) then NextEntryIndex := 0;
    //Get index of previous entry
    PreviousEntryIndex := CurrentLineupTickerEntryIndex-1;
    if (PreviousEntryIndex = -1) then PreviousEntryIndex := Ticker_Collection.Count-1;

    //Init flag to indicate same parent
    ParentFlag  := TRUE;

    //Walk backward through ticker entries to get backward count
    repeat
      TickerRecPtrPrevious := Ticker_Collection.At(PreviousEntryIndex);
      PreviousEntryLeague := TickerRecPtrPrevious^.League;
      PreviousEntryMnemonic := TickerRecPtrPrevious^.Subleague_Mnemonic_Standard;
      //If new league or child to prior parent, set text field; otherwise, continue iterating through
      if (((PreviousEntryMnemonic <> CurrentEntryMnemonic) AND (ParentFlag = TRUE)) OR
           (PreviousEntryLeague <> PreviousEntryLeague)) then
      begin
        FoundNextType := TRUE;
      end
      else begin
        if (TickerRecPtrPrevious^.Enabled = TRUE) AND (TickerRecPtrPrevious^.StartEnableDateTime <= Now) AND
           (TickerRecPtrPrevious^.EndEnableDateTime > Now) AND (TickerRecPtrPrevious^.League <> 'NONE') then
        begin
          Inc(BackwardCount);
        end;
      end;
      if (PreviousEntryIndex = 0) then PreviousEntryIndex := Ticker_Collection.Count-1
      else Dec(PreviousEntryIndex);
      Inc(CollectionItemCounter);
    until (PreviousEntryIndex = Ticker_Collection.Count-1) OR
          (CollectionItemCounter = Ticker_Collection.Count) OR (FoundNextType = TRUE);

    //Walk forward through remaining ticker entries to get forward count
    FoundNextType := FALSE;
    CollectionItemCounter := 1;
    repeat
      TickerRecPtrNext := Ticker_Collection.At(NextEntryIndex);
      NextEntryLeague := TickerRecPtrNext^.League;
      NextEntryMnemonic := TickerRecPtrNext^.Subleague_Mnemonic_Standard;
      //If new league or child to prior parent, set text field; otherwise, continue iterating through
      if (((NextEntryMnemonic <> CurrentEntryMnemonic) AND (ParentFlag = TRUE)) OR
           (NextEntryLeague <> CurrentEntryLeague)) then
      begin
        FoundNextType := TRUE;
      end
      else begin
        if (TickerRecPtrNext^.Enabled = TRUE) AND (TickerRecPtrNext^.StartEnableDateTime <= Now) AND
           (TickerRecPtrNext^.EndEnableDateTime > Now) AND (TickerRecPtrNext^.League <> 'NONE') then
        begin
          Inc(ForwardCount);
        end;
      end;
      if (NextEntryIndex = Ticker_Collection.Count-1) then NextEntryIndex := 0
      else Inc(NextEntryIndex);
      Inc(CollectionItemCounter);
    until (NextEntryIndex = 0) OR (CollectionItemCounter = Ticker_Collection.Count) OR
          (FoundNextType = TRUE);

    //Calculate the progress; shown as depletion
    //Scale so that there is always some of the indicator visible
    //Modified to start at 100%; special case if only one entry for category
    if (BackwardCount = 0) AND (ForwardCount = 0) then OutVal := 10
    else
      OutVal := 100 - Trunc(((BackwardCount)/(BackwardCount+ForwardCount+1))*90);
  end
  //Default to 100%
  else OutVal := 100;

  //Set return value
  GetProgressIndicatorValue := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// ENGINE CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to trigger current graphic
procedure TEngineInterface.TriggerGraphic (Mode: SmallInt);
var
  CmdStr: String;
begin
  try
    //Build & send command string
    CmdStr := SOT + 'TG' + ETX + IntToStr(Mode) + EOT;
    if (SocketConnected) AND (EngineParameters.Enabled = TRUE) then ApdWinsockPort1.Output := CmdStr;
    //If aborting, reset packet and disable packet timeout timer
    if (Mode = 2) AND (SocketConnected) then
    begin
      ApdDataPacket1.Enabled := FALSE;
      ApdDataPacket1.Enabled := TRUE;
      //Disable packet timeout timer
      TickerPacketTimeoutTimer.Enabled := FALSE;
    end;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to trigger ticker on the graphics engine.');
    end;
  end;
end;

//Procedure to update data for a specific record from the ticker playlist; called for each record
procedure TEngineInterface.UpdateTickerRecordDataFromDB(PlaylistID: Double; TickerRecordIndex: SmallInt; EventGUID: TGUID);
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  SQLString: String;
begin
  try
    dmMain.Query3.Close;
    dmMain.Query3.SQL.Clear;
    SQLString := 'SELECT * FROM Ticker_Elements WHERE Playlist_ID = '+ FloatToStr(PlaylistID) +
      ' AND Event_GUID = ' + QuotedStr(GUIDToString(EventGUID));
    dmMain.Query3.SQL.Add (SQLString);
    dmMain.Query3.Open;
    if (dmMain.Query3.RecordCount > 0) then
    begin
      TickerRecPtr := Ticker_Collection.At(TickerRecordIndex);
      With TickerRecPtr^ do
      begin
        Enabled := dmMain.Query3.FieldByName('Enabled').AsBoolean;
        League := dmMain.Query3.FieldByName('League').AsString;
        Subleague_Mnemonic_Standard := dmMain.Query3.FieldByName('Subleague_Mnemonic_Standard').AsString;
        Mnemonic_LiveEvent := dmMain.Query3.FieldByName('Mnemonic_LiveEvent').AsString;
        //Load the user-defined text fields
        for i := 1 to 50 do
          UserData[i] := dmMain.Query3.FieldByName('UserData_' + IntToStr(i)).AsString;
        StartEnableDateTime := dmMain.Query3.FieldByName('StartEnableTime').AsDateTime;
        EndEnableDateTime := dmMain.Query3.FieldByName('EndEnableTime').AsDateTime;
        DwellTime := dmMain.Query3.FieldByName('DwellTime').AsInteger;
        //Modified for 1-line ticker
        SecondLineHasData := dmMain.Query3.FieldByName('SecondLineHasData').AsBoolean;
      end;
    end;
    dmMain.Query3.Close;
  except
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred while trying to update data for ticker playlist entry #' +
        IntToStr(TickerRecordIndex));
  end;
end;

//Procedure to update data for a specific record from the bug playlist; called for each record
procedure TEngineInterface.UpdateBugRecordDataFromDB(PlaylistID: Double; BugRecordIndex: SmallInt; EventGUID: TGUID);
var
  i: SmallInt;
  BugRecPtr: ^BugRec;
  SQLString: String;
begin
  try
    dmMain.Query3.Close;
    dmMain.Query3.SQL.Clear;
    SQLString := 'SELECT * FROM Bug_Elements WHERE Playlist_ID = '+ FloatToStr(PlaylistID) +
      ' AND Event_GUID = ' + QuotedStr(GUIDToString(EventGUID));
    dmMain.Query3.SQL.Add (SQLString);
    dmMain.Query3.Open;
    if (dmMain.Query3.RecordCount > 0) then
    begin
      BugRecPtr := Bug_Collection.At(BugRecordIndex);
      With BugRecPtr^ do
      begin
        Enabled := dmMain.Query3.FieldByName('Enabled').AsBoolean;
        League := dmMain.Query3.FieldByName('League').AsString;
        League := dmMain.Query3.FieldByName('League').AsString;
        //Load the user-defined text fields
        for i := 1 to 25 do
          UserData[i] := dmMain.Query3.FieldByName('UserData_' + IntToStr(i)).AsString;
        StartEnableDateTime := dmMain.Query3.FieldByName('StartEnableTime').AsDateTime;
        EndEnableDateTime := dmMain.Query3.FieldByName('EndEnableTime').AsDateTime;
        DwellTime := dmMain.Query3.FieldByName('DwellTime').AsInteger;
      end;
    end;
    dmMain.Query3.Close;
  except
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred while trying to update data for bug playlist entry #' +
        IntToStr(BugRecordIndex));
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to start ticker, bugs and applicable extra line
//Sets all initial data and sends first data record
procedure TEngineInterface.StartTickerData;
var
  i,j,k: SmallInt;
  TempStr, CmdStr: String;
  TickerRecPtr: ^TickerRec;
  BugRecPtr: ^BugRec;
  ExtraLineRecPtr: ^ExtraLineRec;
  FoundGame, FoundStat: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  SponsorLogoToUse: String;
  NextStatStr: String;
  FieldStr: String;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentGameData: GameRec;
  BugClockPhaseStr: String;
  OkToGo: Boolean;
  //Added for 1-line ticker mode
  OneLineTemplateID: SmallInt;
  OneLineTemplateInfo: OneLineTemplateInfoRec;
  SkipTitlePage: Boolean;
  OneLineTemplateDwell: SmallInt;
begin
  //Init flags
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  RecordIsSponsor := FALSE;
  OkToGo := TRUE;
  try

    //////////////////////////////////////////////////////////////////////////
    // ENABLE THE LOGO/CLOCK - START WITH LOGO
    //////////////////////////////////////////////////////////////////////////
    LogoClockMode := 1;
    //Only enable if not in breaking news
    if (CurrentExtraLinePlaylistID <> 0) then
      EnableLogoClock;

    //////////////////////////////////////////////////////////////////////////
    // START TICKER LOGIC
    //////////////////////////////////////////////////////////////////////////
    //Only proceed if at leat one story in Ticker
    if (Ticker_Collection.Count > 0) then
    begin
      //Make sure it;s a valid index
      if (CurrentTickerEntryIndex < 0) then CurrentTickerEntryIndex := 0;

      //Init FoundGame flag; used to prevent sending of commands if game not found
      OKToGo := TRUE;
      FoundGame := TRUE;
      //Turn on packet
      //ApdDataPacket1.Enabled := FALSE;
      //ApdDataPacket1.Enabled := TRUE;
      //Set enable
      PacketEnable := TRUE;
      //Sleep for 250 mS to give packet time to init
      //Sleep(250);

      //Send first record
      //Set default mode - always = 2 for animated mode
      CmdStr := SOT + 'SM' + ETX + '2' + GS;

      //Send the first record for the Ticker; check to make sure it's a valid index
      if (CurrentTickerEntryIndex <= Ticker_Collection.Count-1) then
      begin
        //Init
        j := 0;
        TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
        //Update the data from the database - called for each record
        UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
        While ((TickerRecPtr^.Enabled = FALSE) OR NOT((TickerRecPtr^.StartEnableDateTime <= Now) AND
              (TickerRecPtr^.EndEnableDateTime > Now))) AND (j < Ticker_Collection.Count) do
        begin
          Inc (j);
          Inc(CurrentTickerEntryIndex);
          if (CurrentTickerEntryIndex = Ticker_Collection.Count) then
            CurrentTickerEntryIndex := 0;
          TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
          //Update the data from the database - called for each record
          UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
        end;

        //Set flag if no enabled records found
        if (j >= Ticker_Collection.Count) then OKToGo := FALSE;

        //Proceed if records found
        if (OKToGo) then
        begin
          //MAIN PROCESSING LOOP FOR TICKER FIELDS
          //Get template info and load the termporary fields collection
          try
            TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to load template fields');
          end;

          ////////////////////////////////////////////////////////////////////////
          //Added for 1-line ticker mode
          ////////////////////////////////////////////////////////////////////////
          if (OneLineModeEnable = TRUE) then
          begin
            OneLineTemplateInfo := GetSingleLineTemplateInfo(TickerRecPtr^.Template_ID);
            //Don't process is invalid template ID; if invalid, jump to next record
            if (OneLineTemplateInfo.OneLine_Template_ID <> -1) then
            begin
              //Save the template ID - will be restored
              //Modified to fix 1-line skipping problem
              //TickerRecPtr^.Template_ID := OneLineTemplateInfo.OneLine_Template_ID;
              //TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);
              OneLineTemplateID := OneLineTemplateInfo.OneLine_Template_ID;
              TemplateInfo := LoadTempTemplateFields(OneLineTemplateID);
              //Set dwell time for this page to the default dwell time for the 1-line template
              //TickerRecPtr^.DwellTime := TemplateInfo.Default_Dwell;
              OneLineTemplateDwell := TemplateInfo.Default_Dwell;
              //Check for second page; if required, store template ID in global variable
              //Also check to see that second page has data
              if (TickerRecPtr^.SecondLineHasData) then
              begin
                TemplateHasSecondOneLinePage := OneLineTemplateInfo.OneLine_SecondTemplate_Enable;
                SecondOneLinePageTemplateID := OneLineTemplateInfo.OneLine_SecondTemplate_ID;
              end
              else TemplateHasSecondOneLinePage := FALSE;
              //Now, check to see if we should skip this title page (i.e. it has been flagged using a style chip)
              SkipTitlePage := FALSE;
              for k := 1 to 25 do
                if (Pos('^^SF', TickerRecPtr^.UserData[k]) > 0) then SkipTitlePage := TRUE;
            end
            else
            begin
              //Enable jump to next record timer & set flag
              if (RunningTicker = TRUE) then
              begin
                JumpToNextTickerRecordTimer.Enabled := TRUE;
                OKToGo := FALSE;
              end;
            end;
          end;

          //Get game data if template requires it
          if (TemplateInfo.UsesGameData) then
          begin
            try
              //Get game data if template requires it
              if (TemplateInfo.UsesGameData) then
              begin
                if (TickerRecPtr^.Odds_GameID > 0) then
                  //Use odds only
                  CurrentGameData := GetGameData(TickerRecPtr^.League, IntToStr(TickerRecPtr^.Odds_GameID), TRUE)
                else
                  //Use full game data
                  CurrentGameData := GetGameData(TickerRecPtr^.League, TickerRecPtr^.SI_GCode, FALSE);
              end;
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for ticker');
            end;
            FoundGame := CurrentGameData.DataFound;
          end;

          //Check the game state vs. the state required by the template if not a match, increment
          //Also do check for enabled entries and check time window
          if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.State <> TemplateInfo.RequiredGameState) AND
             (OKToGo) AND //Special case for postponed, delayed or suspended MLB games
          (NOT((CurrentGameData.State > 3) AND (TemplateInfo.RequiredGameState = 3))) then
          Repeat
            Inc(CurrentTickerEntryIndex);
            if (CurrentTickerEntryIndex = Ticker_Collection.Count) then
            begin
              MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
              CurrentTickerEntryIndex := 0;
            end;
            TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
            TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);
            if (TemplateInfo.UsesGameData) then
            begin
              try
                if (TickerRecPtr^.Odds_GameID > 0) then
                  //Use odds only
                  CurrentGameData := GetGameData(TickerRecPtr^.League, IntToStr(TickerRecPtr^.Odds_GameID), TRUE)
                else
                  //Use full game data
                  CurrentGameData := GetGameData(TickerRecPtr^.League, TickerRecPtr^.SI_GCode, FALSE);
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to get game data for ticker');
              end;
              FoundGame := CurrentGameData.DataFound;
            end
            else CurrentGameData.State := -1;
            //Update the data from the database - called for each record
            UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
          Until ((TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.State = TemplateInfo.RequiredGameState) OR
                //Special case for postponed, delayed or suspended MLB games
                ((CurrentGameData.State > 3) AND (TemplateInfo.RequiredGameState = 3)) OR
                (CurrentGameData.State = -1)) AND ((TickerRecPtr^.Enabled = TRUE) AND
                (TickerRecPtr^.StartEnableDateTime <= Now) AND (TickerRecPtr^.EndEnableDateTime > Now));

          ////////////////////////////////////////////////////////////////////////
          //Added for 1-line ticker mode
          ////////////////////////////////////////////////////////////////////////
          if (OneLineModeEnable = TRUE) then
          begin
            OneLineTemplateInfo := GetSingleLineTemplateInfo(TickerRecPtr^.Template_ID);
            //Don't process is invalid template ID; if invalid, jump to next record
            if (OneLineTemplateInfo.OneLine_Template_ID <> -1) then
            begin
              //Modified to fix 1-line skipping problem
              //TickerRecPtr^.Template_ID := OneLineTemplateInfo.OneLine_Template_ID;
              //TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);
              OneLineTemplateID := OneLineTemplateInfo.OneLine_Template_ID;
              TemplateInfo := LoadTempTemplateFields(OneLineTemplateID);
              //Set dwell time for this page to the default dwell time for the 1-line template
              //TickerRecPtr^.DwellTime := TemplateInfo.Default_Dwell;
              OneLineTemplateDwell := TemplateInfo.Default_Dwell;
              //Check for second page; if required, store template ID in global variable
              //Also check to see that second page has data
              if (TickerRecPtr^.SecondLineHasData) then
              begin
                TemplateHasSecondOneLinePage := OneLineTemplateInfo.OneLine_SecondTemplate_Enable;
                SecondOneLinePageTemplateID := OneLineTemplateInfo.OneLine_SecondTemplate_ID;
              end
              else TemplateHasSecondOneLinePage := FALSE;
              //Now, check to see if we should skip this title page (i.e. it has been flagged using a style chip)
              SkipTitlePage := FALSE;
              for k := 1 to 25 do
                if (Pos('^^SF', TickerRecPtr^.UserData[k]) > 0) then SkipTitlePage := TRUE;
            end
            else
            begin
              //Enable jump to next record timer & set flag
              if (RunningTicker = TRUE) then
              begin
                JumpToNextTickerRecordTimer.Enabled := TRUE;
                OKToGo := FALSE;
              end;
            end;
          end;

          //Set the default dwell time - minimum = 2000mS; set packet timeout interval
          //Special case for 1-line ticker mode
          if (OneLineModeEnable = TRUE) then
          begin
            if (OneLineTemplateDwell > 2000) then
            begin
              TickerCommandDelayTimer.Interval := OneLineTemplateDwell;
              TickerPacketTimeoutTimer.Interval := OneLineTemplateDwell + 5000;
            end
            else begin
              TickerCommandDelayTimer.Interval := 2000;
              TickerPacketTimeoutTimer.Interval := OneLineTemplateDwell + 7000;
            end;
          end
          else begin
            if (TickerRecPtr^.DwellTime > 2000) then
            begin
              TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
              TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 5000;
            end
            else begin
              TickerCommandDelayTimer.Interval := 2000;
              TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 7000;
            end;
          end;

          //Set the template type
          CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;
          //Add the fields from the temporary fields collection
          if (Temporary_Fields_Collection.Count > 0) then
          begin
            for j := 0 to Temporary_Fields_Collection.Count-1 do
            begin
              TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
              //Filter out fields ID values of -1 => League designator, etc.
              if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                begin
                  TempStr := TemplateFieldsRecPtr^.Field_Contents;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                    ETX + TempStr + GS;
                end
                //It's a symbolic name, so get the field contents
                else begin
                  TempStr := GetValueOfSymbol(TICKER, TemplateFieldsRecPtr^.Field_Contents, CurrentTickerEntryIndex,
                    CurrentGameData, CurrentTickerDisplayMode);
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  //Check to see if it's the bug clock/phase field; if so, store for insertion into line-up
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX +
                    TempStr + GS;
                end;
              end;
            end;
            //Set ticker display mode control region
            CmdStr := CmdStr + 'SD' + ETX + '1001' + ETX + IntToStr(CurrentTickerDisplayMode) + GS;

            //Write out to the as-run log if it's a sponsor logo
            if (Trim(TickerRecPtr^.SponsorLogo_Name) <> '') then
              WriteToAsRunLog('Started display of sponsor logo: ' + CurrentSponsorLogoName);
          end;

          //Set data row indicator in Ticker grid
          MainForm.TickerPlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex+1;
          MainForm.TickerPlaylistGrid.SetTopLeft(1, MainForm.TickerPlaylistGrid.CurrentDataRow);

          //Set lineup text
          LineupData := GetLineuptext(CurrentTickerEntryIndex, CurrentTickerDisplayMode);
          if (LineupData.UseLineup = TRUE) then
          begin
            if (Trim(LineupData.TextFields[1]) = 'NONE') then
              LineupData.TextFields[1] := ' ';
            CmdStr := CmdStr + 'SI' + ETX + LineupData.TextFields[1] + ETX +
                      LineupData.TextFields[2] + ETX +
                      LineupData.TextFields[3] + ETX +
                      LineupData.TextFields[4] + ETX +
                      LineupData.TextFields[5] + GS;
          end;

          //Resubstitute lower-case formatting strings and line wrap command
          CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[X', '[x', [rfReplaceAll]);
          //Modified for 1-line ticker
          if (OneLineModeEnable = FALSE) then
            CmdStr := StringReplace(CmdStr, '|', '[r 0]', [rfReplaceAll]);

          //Substitute style chip codes
          CmdStr := ProcessStyleChips(CmdStr);

          //Send the command with a trigger
          CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
          if (OKToGo) AND (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
          begin
            try
              //Special code here to deal with skipping title page; if skipping, just fire second page
              if (SkipTitlePage) then
              begin
                Send2ndPageTickerRecord;
                //Clear flag to prevent page from being sent again
                TemplateHasSecondOneLinePage := FALSE;
              end
              else
                ApdWinsockPort1.Output := CmdStr;
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying send ticker command to engine');
            end;
            //Enable packet timeout timer
            TickerPacketTimeoutTimer.Enabled := TRUE;
            //Start timer to send next record
            if (USEDATAPACKET = FALSE) AND (SkipTitlePage = FALSE) then TickerCommandDelayTimer.Enabled := TRUE;
          end
          else if (FoundGame = FALSE) then
          begin
            //Enable delay timer
            if (RunningTicker = TRUE) then
            begin
              JumpToNextTickerRecordTimer.Enabled := TRUE;
            end;
          end;
        end;
      end;
    end;
    //////////////////////////////////////////////////////////////////////////
    // END TICKER LOGIC
    //////////////////////////////////////////////////////////////////////////

    //Delay to stagger displays
    Sleep(2000);

    //////////////////////////////////////////////////////////////////////////
    // START BUG LOGIC
    //////////////////////////////////////////////////////////////////////////
    //Only proceed if at leat one story in BUG
    //Modified for 1-Line ticker
    if (Bug_Collection.Count > 0) AND (OneLineModeEnable = FALSE) then
    begin
      //Make sure it;s a valid index
      if (CurrentBugEntryIndex < 0) then CurrentBugEntryIndex := 0;
      //Init
      BugClockPhaseStr := ' ';
      OKToGo := TRUE;
      //Init FoundGame flag; used to prevent sending of commands if game not found
      FoundGame := TRUE;
      //Turn on packet
      //ApdDataPacket1.Enabled := FALSE;
      //ApdDataPacket1.Enabled := TRUE;
      //Set enable
      PacketEnable := TRUE;
      //Sleep for 250 mS to give packet time to init
      //Sleep(250);

      //Send first record
      //Set default mode - always = 2 for animated mode
      CmdStr := SOT + 'SM' + ETX + '2' + GS;

      //Send the first record for the Bug; check to make sure it's a valid index
      if (CurrentBugEntryIndex <= Bug_Collection.Count-1) then
      begin
        j := 0;
        BugRecPtr := Bug_Collection.At(CurrentBugEntryIndex);
//        While ((BugRecPtr^.Enabled = FALSE) OR (BugRecPtr^.StartEnableDateTime > Now) OR
//              (BugRecPtr^.EndEnableDateTime <= Now)) AND (j < Bug_Collection.Count) do
        While ((BugRecPtr^.Enabled = FALSE) OR NOT((BugRecPtr^.StartEnableDateTime <= Now) AND
              (BugRecPtr^.EndEnableDateTime > Now))) AND (j < Bug_Collection.Count) do
        begin
          Inc (j);
          Inc(CurrentBugEntryIndex);
          if (CurrentBugEntryIndex = Bug_Collection.Count) then
            CurrentBugEntryIndex := 0;
          BugRecPtr := Bug_Collection.At(CurrentBugEntryIndex);
        end;
        //Set flag if no enabled records found
        if (j >= Bug_Collection.Count) then OKToGo := FALSE;

        //Proceed if records found
        if (OKToGo) then
        begin
          //MAIN PROCESSING LOOP FOR BUG FIELDS
          //Get template info and load the termporary fields collection
          //Add bias to template ID if in Live Event mode
          if (CurrentTickerDisplayMode = 1) then
            TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID+50)
          else
            TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID);

          //Get game data if required
          if (TemplateInfo.UsesGameData) then
          begin
            try
              //Get game data if template requires it
              if (TemplateInfo.UsesGameData) then
              begin
                if (BugRecPtr^.Odds_GameID > 0) then
                  //Use odds only
                  CurrentGameData := GetGameData(BugRecPtr^.League, IntToStr(BugRecPtr^.Odds_GameID), TRUE)
                else
                  //Use full game data
                  CurrentGameData := GetGameData(BugRecPtr^.League, BugRecPtr^.SI_GCode, FALSE);
              end;
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for ticker');
            end;
            FoundGame := CurrentGameData.DataFound;
          end;

          //Check the game state vs. the state required by the template if not a match, increment
          //Also do check for enabled entries and check time window
          if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.State <> TemplateInfo.RequiredGameState) AND
          //Special case for postponed, delayed or suspended MLB games
          (NOT((CurrentGameData.State > 3) AND (TemplateInfo.RequiredGameState = 3))) then
          Repeat
            Inc(CurrentBugEntryIndex);
            if (CurrentBugEntryIndex = Bug_Collection.Count) then
            begin
              MainForm.LoadPlaylistCollection(Bug, 0, CurrentBugPlaylistID);
              CurrentBugEntryIndex := 0;
            end;
            BugRecPtr := Bug_Collection.At(CurrentBugEntryIndex);
            TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID);
            if (TemplateInfo.UsesGameData) then
            begin
              try
                if (BugRecPtr^.Odds_GameID > 0) then
                  //Use odds only
                  CurrentGameData := GetGameData(BugRecPtr^.League, IntToStr(BugRecPtr^.Odds_GameID), TRUE)
                else
                  //Use full game data
                  CurrentGameData := GetGameData(BugRecPtr^.League, BugRecPtr^.SI_GCode, FALSE);
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to get game data for bug');
              end;
              FoundGame := CurrentGameData.DataFound;
            end
            else CurrentGameData.State := -1;
          Until ((TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.State = TemplateInfo.RequiredGameState) OR
                ((CurrentGameData.State > 3) AND (TemplateInfo.RequiredGameState = 3)) OR
                (CurrentGameData.State = -1)) AND ((BugRecPtr^.Enabled = TRUE) AND
                (BugRecPtr^.StartEnableDateTime <= Now) AND (BugRecPtr^.EndEnableDateTime > Now));

          //Set the default dwell time - minimum = 2000mS; set packet timeout interval
          if (BugRecPtr^.DwellTime > 2000) then
          begin
            BugCommandDelayTimer.Interval := BugRecPtr^.DwellTime;
            BugPacketTimeoutTimer.Interval := BugRecPtr^.DwellTime + 5117;
          end
          else begin
            BugCommandDelayTimer.Interval := 2000;
            BugPacketTimeoutTimer.Interval := BugRecPtr^.DwellTime + 7117;
          end;

          //Set the template type
          CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;
          //Add the fields from the temporary fields collection
          if (Temporary_Fields_Collection.Count > 0) then
          begin
            for j := 0 to Temporary_Fields_Collection.Count-1 do
            begin
              TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
              //Filter out fields ID values of -1 => League designator
              if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                begin
                  TempStr := TemplateFieldsRecPtr^.Field_Contents;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                    ETX + TempStr + GS;
                end
                //It's a symbolic name, so get the field contents
                else begin
                  try
                    TempStr := GetValueOfSymbol(BUG, TemplateFieldsRecPtr^.Field_Contents, CurrentBugEntryIndex,
                      CurrentGameData, CurrentTickerDisplayMode);
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get value for bug data field');
                  end;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  //Check to see if it's the bug clock/phase field; if so, store for insertion into line-up
                  if (TemplateFieldsRecPtr^.Engine_Field_ID = 2000) then
                    BugClockPhaseStr := TempStr
                  else
                    CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr + GS;
                end;
              end;
            end;
          end;

          //Set data row indicator in Ticker grid
          MainForm.BugPlaylistGrid.CurrentDataRow := CurrentBugEntryIndex+1;
          MainForm.BugPlaylistGrid.SetTopLeft(1, MainForm.BugPlaylistGrid.CurrentDataRow);

          //Set ticker display mode control region
          if (OneLineModeEnable = FALSE) then
            CmdStr := CmdStr + 'SD' + ETX + '1001' + ETX + IntToStr(CurrentTickerDisplayMode) + GS;

          //Set lineup text; bug clock phase string is set above in fields loop
          if (CurrentTickerDisplayMode = 1) then BugClockPhaseStr := ' ';
          if (Trim(BugRecPtr^.League) = 'NONE') then
            CmdStr := CmdStr + 'SI' + ETX + ' ' + ETX + BugClockPhaseStr +
                      ETX + ' ' + ETX + ' ' + ETX + ' ' + GS
          else
            CmdStr := CmdStr + 'SI' + ETX + BugRecPtr^.League + ETX + BugClockPhaseStr +
                      ETX + ' ' + ETX + ' ' + ETX + ' ' + GS;

          //Resubstitute lower-case formatting strings and line wrap command
          CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '|', '[r 0]', [rfReplaceAll]);

          //Substitute style chip codes
          CmdStr := ProcessStyleChips(CmdStr);

          //Send the command with a trigger
          CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
          if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
          begin
            try
              ApdWinsockPort1.Output := CmdStr;
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying send bug command to engine');
            end;
            //Enable packet timeout timer
            BugPacketTimeoutTimer.Enabled := TRUE;
            //Start timer to send next record
            if (USEDATAPACKET = FALSE) then BugCommandDelayTimer.Enabled := TRUE;
          end
          else if (FoundGame = FALSE) then
          begin
            //Enable delay timer
            if (RunningTicker = TRUE) then JumpToNextBugRecordTimer.Enabled := TRUE;
          end;
        end;
      end;
    end;
    //////////////////////////////////////////////////////////////////////////
    // END BUG LOGIC
    //////////////////////////////////////////////////////////////////////////

    //Delay to stagger displays
    Sleep(2000);

    //////////////////////////////////////////////////////////////////////////
    // START EXTRA LINE LOGIC
    //////////////////////////////////////////////////////////////////////////
    //Only proceed if at leat one story in ExtraLine
    //Modified for 1-Line ticker
    if (ExtraLine_Collection.Count > 0) AND (OneLineModeEnable = FALSE) then
    begin
      //Make sure it;s a valid index
      if (CurrentExtraLineEntryIndex < 0) then CurrentExtraLineEntryIndex := 0;

      OKToGo := TRUE;
      //Init FoundGame flag; used to prevent sending of commands if game not found
      FoundGame := TRUE;
      //Set enable
      PacketEnable := TRUE;
      //Sleep for 250 mS to give packet time to init
      //Sleep(250);

      //Send first record
      //Set default mode - always = 2 for animated mode
      CmdStr := SOT + 'SM' + ETX + '2' + GS;

      //Send the first record for the ExtraLine; check to make sure it's a valid index
      if (CurrentExtraLineEntryIndex <= ExtraLine_Collection.Count-1) then
      begin
        j := 0;
        ExtraLineRecPtr := ExtraLine_Collection.At(CurrentExtraLineEntryIndex);
//        While ((ExtraLineRecPtr^.Enabled = FALSE) OR (ExtraLineRecPtr^.StartEnableDateTime > Now) OR
//              (ExtraLineRecPtr^.EndEnableDateTime <= Now)) AND (j < ExtraLine_Collection.Count) do
        While ((ExtraLineRecPtr^.Enabled = FALSE) OR NOT((ExtraLineRecPtr^.StartEnableDateTime <= Now) AND
              (ExtraLineRecPtr^.EndEnableDateTime > Now))) AND (j < ExtraLine_Collection.Count) do
        begin
          Inc (j);
          Inc(CurrentExtraLineEntryIndex);
          if (CurrentExtraLineEntryIndex = ExtraLine_Collection.Count) then
            CurrentExtraLineEntryIndex := 0;
          ExtraLineRecPtr := ExtraLine_Collection.At(CurrentExtraLineEntryIndex);
        end;

        //Set flag if no enabled records found
        if (j >= ExtraLine_Collection.Count) then OKToGo := FALSE;

        //Proceed if records found
        if (OKToGo) then
        begin
          //MAIN PROCESSING LOOP FOR ExtraLine FIELDS
          //Get template info and load the termporary fields collection
          try
            TemplateInfo := LoadTempTemplateFields(ExtraLineRecPtr^.Template_ID);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to load template fields');
          end;

          //Get game data if template requires it
          if (TemplateInfo.UsesGameData) then
          begin
            try
              //Get game data if template requires it
              if (TemplateInfo.UsesGameData) then
              begin
                if (ExtraLineRecPtr^.Odds_GameID > 0) then
                  //Use odds only
                  CurrentGameData := GetGameData(ExtraLineRecPtr^.League, IntToStr(ExtraLineRecPtr^.Odds_GameID), TRUE)
                else
                  //Use full game data
                  CurrentGameData := GetGameData(ExtraLineRecPtr^.League, ExtraLineRecPtr^.SI_GCode, FALSE);
              end;
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for ExtraLine');
            end;
            FoundGame := CurrentGameData.DataFound;
          end;

          //Check the game state vs. the state required by the template if not a match, increment
          //Also do check for enabled entries and check time window
          if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.State <> TemplateInfo.RequiredGameState) AND
          //Special case for postponed, delayed or suspended MLB games
          (NOT((CurrentGameData.State > 3) AND (TemplateInfo.RequiredGameState = 3))) then
          Repeat
            Inc(CurrentExtraLineEntryIndex);
            if (CurrentExtraLineEntryIndex = ExtraLine_Collection.Count) then
            begin
              MainForm.LoadPlaylistCollection(ExtraLine, 0, CurrentExtraLinePlaylistID);
              CurrentExtraLineEntryIndex := 0;
            end;
            ExtraLineRecPtr := ExtraLine_Collection.At(CurrentExtraLineEntryIndex);
            TemplateInfo := LoadTempTemplateFields(ExtraLineRecPtr^.Template_ID);
            if (TemplateInfo.UsesGameData) then
            begin
              try
                if (ExtraLineRecPtr^.Odds_GameID > 0) then
                  //Use odds only
                  CurrentGameData := GetGameData(ExtraLineRecPtr^.League, IntToStr(ExtraLineRecPtr^.Odds_GameID), TRUE)
                else
                  //Use full game data
                  CurrentGameData := GetGameData(ExtraLineRecPtr^.League, ExtraLineRecPtr^.SI_GCode, FALSE);
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to get game data for ExtraLine');
              end;
              FoundGame := CurrentGameData.DataFound;
            end
            else CurrentGameData.State := -1;
          Until ((TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.State = TemplateInfo.RequiredGameState) OR
                ((CurrentGameData.State > 3) AND (TemplateInfo.RequiredGameState = 3)) OR
                (CurrentGameData.State = -1)) AND ((ExtraLineRecPtr^.Enabled = TRUE) AND
                (ExtraLineRecPtr^.StartEnableDateTime <= Now) AND (ExtraLineRecPtr^.EndEnableDateTime > Now));

          //Set the default dwell - minimum = 2000mS
          if (ExtraLineRecPtr^.DwellTime > 2000) then
          begin
            ExtraLineCommandDelayTimer.Interval := ExtraLineRecPtr^.DwellTime + 212;
            ExtraLinePacketTimeoutTimer.Interval := ExtraLineRecPtr^.DwellTime + 5212;
          end
          else begin
            ExtraLineCommandDelayTimer.Interval := 2212;
            ExtraLinePacketTimeoutTimer.Interval := ExtraLineRecPtr^.DwellTime + 7212;
          end;

          //Set the template type
          CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;
          //Add the fields from the temporary fields collection
          if (Temporary_Fields_Collection.Count > 0) then
          begin
            for j := 0 to Temporary_Fields_Collection.Count-1 do
            begin
              TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
              //Filter out fields ID values of -1 => League designator
              if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                begin
                  TempStr := TemplateFieldsRecPtr^.Field_Contents;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                    ETX + TempStr + GS;
                end
                //It's a symbolic name, so get the field contents
                else begin
                  try
                    TempStr := GetValueOfSymbol(EXTRALINE, TemplateFieldsRecPtr^.Field_Contents, CurrentExtraLineEntryIndex,
                      CurrentGameData, CurrentTickerDisplayMode);
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get value for main ExtraLine data field');
                  end;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr + GS;
                end;
              end;
            end;
          end;

          //Set data row indicator in ExtraLine grid
          MainForm.ExtraLinePlaylistGrid.CurrentDataRow := CurrentExtraLineEntryIndex+1;
          MainForm.ExtraLinePlaylistGrid.SetTopLeft(1, MainForm.ExtraLinePlaylistGrid.CurrentDataRow);

          //Set lineup text to blank
          CmdStr := CmdStr + 'SI' + ETX + ' ' + ETX +
                    ' ' + ETX +
                    ' ' + ETX +
                    ' ' + ETX +
                    ' ' + GS;

          //Resubstitute lower-case formatting strings and line wrap command
          CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '|', '[r 0]', [rfReplaceAll]);

          //Substitute style chip codes
          CmdStr := ProcessStyleChips(CmdStr);

          //Disable the logo clock if it is running and the extra line playlist is for breaking news
          if (CurrentExtraLinePlaylistID = 0) then DisableLogoClock;

          //Send the command with a trigger
          CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
          if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
          begin
            try
              ApdWinsockPort1.Output := CmdStr;
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying send ExtraLine command to engine');
            end;
            //Enable packet timeout timer
            ExtraLinePacketTimeoutTimer.Enabled := TRUE;
            //Start timer to send next record
            if (USEDATAPACKET = FALSE) then ExtraLineCommandDelayTimer.Enabled := TRUE;
          end
          else if (FoundGame = FALSE) then
          begin
            //Enable delay timer
            if (RunningTicker = TRUE) then JumpToNextExtraLineRecordTimer.Enabled := TRUE;
          end;
        end;
      end;
    end;
    //////////////////////////////////////////////////////////////////////////
    // END EXTRA LINE LOGIC
    //////////////////////////////////////////////////////////////////////////

    //MOVED TO TOP OF FUNCTION FOR 1-LINE TICKER
    //////////////////////////////////////////////////////////////////////////
    // ENABLE THE LOGO/CLOCK - START WITH LOGO
    //////////////////////////////////////////////////////////////////////////
//    LogoClockMode := 1;
//    //Only enable if not in breaking news
//    if (CurrentExtraLinePlaylistID <> 0) then
//      EnableLogoClock;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send initial text and start ticker');
    end;
  end;
end;

//FUNCTIONS FOR ADVANCING RECORDS
//Handler for packet requesting more data for ticker
procedure TEngineInterface.ApdDataPacket1StringPacket(Sender: TObject; Data: String);
var
  i: SmallInt;
  CurrentTemplateIDNum: SmallInt;
  CurrentTemplateIDNumStr: String;
  RequiredDelay: SmallInt;
begin
  if (USEDATAPACKET) then
  begin
    CurrentTemplateIDNumStr := '';
    CurrentTemplateIDNum := 0;
    //Disable timeout timer
    //Enable delay timer for next command if not previously in single command mode
    //Set current template ID
    if (Length(Data) >= 9) AND (Pos('FAIL', ANSIUpperCase(Data)) = 0) AND (Pos('Refused', ANSIUpperCase(Data)) = 0) then
    begin
      i := 7;
      Repeat
        if (Ord(Data[i]) >= 48) AND (Ord(Data[i]) <= 57) then CurrentTemplateIDNumStr := CurrentTemplateIDNumStr + Data[i];
        Inc(i);
      Until (i > Length(Data)) OR (Data[i] = ':');
      try
        if (Length(CurrentTemplateIDNumStr) > 0) AND (Length(CurrentTemplateIDNumStr) <=3) then
          CurrentTemplateIDNum := StrToInt(Trim(CurrentTemplateIDNumStr))
        else
          //Set to 1 to force next page for ticker
          CurrentTemplateIDNum := 1;
      except
        //Need exception handling here
        WriteToErrorLog('Error occurred while trying to convert template ID number; command may have failed');
      end;
      if (RunningTicker = TRUE) AND (DisableCommandTimer = FALSE) then
      begin
        Case CurrentTemplateIDNum of
        //Ticker
        1..29, 80..300: begin
                 if (USEDATAPACKET) then TickerCommandDelayTimer.Enabled := TRUE;
                 TickerPacketTimeoutTimer.Enabled := FALSE;
               end;
        //Bug
        30..39: begin
                 if (USEDATAPACKET) then BugCommandDelayTimer.Enabled := TRUE;
                 BugPacketTimeoutTimer.Enabled := FALSE;
               end;
        //Extra-Line
        50..59: begin
                 if (USEDATAPACKET) then ExtraLineCommandDelayTimer.Enabled := TRUE;
                 ExtraLinePacketTimeoutTimer.Enabled := FALSE;
               end;
        end;
      end;
    end
    else if (Pos('FAIL', ANSIUpperCase(Data)) <> 0) then
    begin
      WriteToErrorLog('Command status = FAILED; Return String: ' + Data);
    end
    else if (Pos('Refused', ANSIUpperCase(Data)) <> 0) then
    begin
      WriteToErrorLog('Command status = CONNECTION REFUSED; Return String: ' + Data);
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// DISPLAY DWELL TIMERS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Handler for timer to send next command
procedure TEngineInterface.TickerCommandDelayTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) then
  begin
    //Prevent retrigger
    TickerCommandDelayTimer.Enabled := FALSE;
    //Send next record
    //Modified for 1-line ticker
    if (TemplateHasSecondOneLinePage) then
    begin
      Send2ndPageTickerRecord;
      TemplateHasSecondOneLinePage := FALSE;
    end
    else
      SendTickerRecord(TRUE, 0);
  end;
end;
//Handler for 1 mS timer used to send next command when jumping over stats headers
procedure TEngineInterface.JumpToNextTickerRecordTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) then
  begin
    //Prevent retrigger
    JumpToNextTickerRecordTimer.Enabled := FALSE;
    //Send next record
    SendTickerRecord(TRUE, 0);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// BUGS
////////////////////////////////////////////////////////////////////////////////
procedure TEngineInterface.BugCommandDelayTimerTimer(Sender: TObject);
begin
  if (Bug_Collection.Count > 0) then
  begin
    //Prevent retrigger
    BugCommandDelayTimer.Enabled := FALSE;
    //Send next record
    SendBugRecord(TRUE, 0);
  end;
end;

procedure TEngineInterface.JumpToNextBugRecordTimerTimer(Sender: TObject);
begin
  if (Bug_Collection.Count > 0) then
  begin
    //Prevent retrigger
    JumpToNextBugRecordTimer.Enabled := FALSE;
    //Send next record
    SendBugRecord(TRUE, 0);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// EXTRA LINE
////////////////////////////////////////////////////////////////////////////////
procedure TEngineInterface.ExtraLineCommandDelayTimerTimer(Sender: TObject);
begin
  //Prevent retrigger
  if (ExtraLine_Collection.Count > 0) then
  begin
    ExtraLineCommandDelayTimer.Enabled := FALSE;
    //Send next record
    SendExtraLineRecord(TRUE, 0);
  end;
end;

procedure TEngineInterface.JumpToNextExtraLineRecordTimerTimer(Sender: TObject);
begin
  if (ExtraLine_Collection.Count > 0) then
  begin
    //Prevent retrigger
    JumpToNextExtraLineRecordTimer.Enabled := FALSE;
    //Send next record
    SendExtraLineRecord(TRUE, 0);
  end;
end;


////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Procedure to send next record after receipt of packet
procedure TEngineInterface.SendTickerRecord(NextRecord: Boolean; RecordIndex: SmallInt);
var
  i,j,k: SmallInt;
  TempStr, CmdStr: String;
  TickerRecPtr, TickerRecPtrNew: ^TickerRec;
  FoundGame: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  IsLast: Boolean;
  OverlayIndex, OverlayIndexNext: SmallInt;
  IsNewOverlay, PadText: Boolean;
  InGameStatsRequired: Boolean;
  FoundGameInDatamart: Boolean;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentGameData: GameRec;
  OkToGo: Boolean;
  //Added for 1-line ticker mode
  OneLineTemplateID: SmallInt;
  OneLineTemplateInfo: OneLineTemplateInfoRec;
  SkipTitlePage: Boolean;
  OneLineTemplateDwell: SmallInt;
begin
  //Init flags
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  TemplateHasSecondOneLinePage := FALSE;

  //Init FoundGame flag - used to prevent inadverant commands from being sent if game not found
  FoundGame := TRUE;
  RecordIsSponsor := FALSE;

  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;

  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Init
    IsLast := FALSE;

    //Make sure we haven't dumped put
    if ((TickerAbortFlag = FALSE) AND (Ticker_Collection.Count > 0)) then
    begin
      OKToGo := TRUE;
      //Check if flag set to just send next record
      if (NEXTRECORD = TRUE) then
      begin
        //Make sure there's at least one record left before sending more data;
        //If not, reset to top if in looping mode
        if (CurrentTickerEntryIndex >= Ticker_Collection.Count-1) AND
           (LoopTicker = TRUE) then
        begin
          try
            //Reload the Ticker collection in case it has been modified; clear flag to indicate
            //it's not the first time through
            MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
            if (Ticker_Collection.Count > 0) then CurrentTickerEntryIndex := 0;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reload main ticker collection');
          end;
          //Check time window and enable
          TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
          //Update the data from the database - called for each record
          UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
          //Make sure record is enabled and within time window
          j := 0;
          While ((TickerRecPtr^.Enabled = FALSE) OR NOT ((TickerRecPtr^.StartEnableDateTime <= Now) AND
                (TickerRecPtr^.EndEnableDateTime > Now))) AND (j < Ticker_Collection.Count) do
          begin
            Inc(CurrentTickerEntryIndex);
            //Set pointer
            TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
            //Update the data from the database - called for each record
            UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
            if (CurrentTickerEntryIndex = Ticker_Collection.Count) then
            begin
              try
                MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
                CurrentTickerEntryIndex := 0;
                TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
                //Update the data from the database - called for each record
                UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
                j := 0;
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to reload main ticker collection');
              end;
            end;
            Inc(j);
          end;
          //Set flag if no enabled records found
          if (j >= Ticker_Collection.Count) then OKToGo := FALSE;
        end
        //Not last record, so increment Ticker playout collection object index
        else if (CurrentTickerEntryIndex < Ticker_Collection.Count-1) then
        begin
          //Make sure record is enabled and within time window
          j := 0;
          Repeat
            Inc(CurrentTickerEntryIndex);
            if (CurrentTickerEntryIndex = Ticker_Collection.Count) then
            begin
              try
                MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
                CurrentTickerEntryIndex := 0;
                TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
                //Update the data from the database - called for each record
                UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
                j := 0;
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to reload main ticker collection');
              end;
            end;
            TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
            Inc (j);
            //Update the data from the database - called for each record
            UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
          Until ((TickerRecPtr^.Enabled = TRUE) AND (TickerRecPtr^.StartEnableDateTime <= Now) AND
                (TickerRecPtr^.EndEnableDateTime > Now)) OR (j >= Ticker_Collection.Count);
          //Set flag if no enabled records found
          if (j >= Ticker_Collection.Count) then OKToGo := FALSE;
        end
        //At end and not looping, so clear screen
        else if (CurrentTickerEntryIndex >= Ticker_Collection.Count-1) AND
                (LoopTicker = FALSE) then
        begin
          //Increment to end
          Inc(CurrentTickerEntryIndex);
        end;
        //Set flag if at last collection obejct and not looping
        if (CurrentTickerEntryIndex = Ticker_Collection.Count-1) AND
           (LoopTicker = FALSE) then IsLast := TRUE;
        //Enable command timer
        DisableCommandTimer := FALSE;
      end
      //Triggering specific record, so set current entry index
      else begin
        CurrentTickerEntryIndex := RecordIndex;
        //Disble command timer
        DisableCommandTimer := TRUE;
      end;

      //Proceed if not at end or in looping mode and at beggining
      if (CurrentTickerEntryIndex <= Ticker_Collection.Count-1) AND (OKToGo) then
      begin
        //Get pointer to current record
        TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);

        //Update the data from the database - called for each record
        UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);

        //Set default mode
        CmdStr := SOT + 'SM' + ETX + '2' + GS;

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR TICKER FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);

        ////////////////////////////////////////////////////////////////////////
        //Added for 1-line ticker mode
        ////////////////////////////////////////////////////////////////////////
        if (OneLineModeEnable = TRUE) then
        begin
          OneLineTemplateInfo := GetSingleLineTemplateInfo(TickerRecPtr^.Template_ID);
          //Don't process is invalid template ID; if invalid, jump to next record
          if (OneLineTemplateInfo.OneLine_Template_ID <> -1) then
          begin
            //Modified to fix 1-line skipping problem
            //TickerRecPtr^.Template_ID := OneLineTemplateInfo.OneLine_Template_ID;
            //TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);
            OneLineTemplateID := OneLineTemplateInfo.OneLine_Template_ID;
            TemplateInfo := LoadTempTemplateFields(OneLineTemplateID);
            //Set dwell time for this page to the default dwell time for the 1-line template
            OneLineTemplateDwell := TemplateInfo.Default_Dwell;
            //Check for second page; if required, store template ID in global variable
            //Also check to see that second page has data
            if (TickerRecPtr^.SecondLineHasData) then
            begin
              TemplateHasSecondOneLinePage := OneLineTemplateInfo.OneLine_SecondTemplate_Enable;
              SecondOneLinePageTemplateID := OneLineTemplateInfo.OneLine_SecondTemplate_ID;
            end
            else TemplateHasSecondOneLinePage := FALSE;
            //Now, check to see if we should skip this title page (i.e. it has been flagged using a style chip)
            SkipTitlePage := FALSE;
            for k := 1 to 25 do
              if (Pos('^^SF', TickerRecPtr^.UserData[k]) > 0) then SkipTitlePage := TRUE;
            //Log skip page flag set
            if (SkipTitlePage) then WriteToErrorLog('101 Setting flag to skip page due to style chip code for page: ' +
              IntToStr(TickerRecPtr^.Event_Index));
          end
          else begin
            //Log template not found
            WriteToErrorLog('102 1-Line template ID not found for template ID: ' + IntToStr(TickerRecPtr^.Template_ID));
            //Enable jump to next record timer & exit procedure
            if (RunningTicker = TRUE) then
            begin
              JumpToNextTickerRecordTimer.Enabled := TRUE;
              Exit;
            end;
          end;
        end;

        //Get game data if template requires it
        if (TemplateInfo.UsesGameData) then
        begin
          try
            if (TickerRecPtr^.Odds_GameID > 0) then
              //Use odds only
              CurrentGameData := GetGameData(TickerRecPtr^.League, IntToStr(TickerRecPtr^.Odds_GameID), TRUE)
            else
              //Use full game data
              CurrentGameData := GetGameData(TickerRecPtr^.League, TickerRecPtr^.SI_GCode, FALSE);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to get game data for ticker');
          end;
          FoundGame := CurrentGameData.DataFound;
          //Log if game not found
          if (FoundGame = FALSE) then
            WriteToErrorLog('103 Game not found for Game ID: ' + TickerRecPtr^.SI_GCode);
        end;

        //Check the game state vs. the state required by the template if not a match, increment
        //Also do check for enabled entries and check time window
        if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.State <> TemplateInfo.RequiredGameState) AND
        //Special case for postponed, delayed or suspended MLB games
        (NOT((CurrentGameData.State > 3) AND (TemplateInfo.RequiredGameState = 3))) AND
         (NEXTRECORD = TRUE) then
        Repeat
          Inc(CurrentTickerEntryIndex);
          if (CurrentTickerEntryIndex = Ticker_Collection.Count) then
          begin
            MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
            CurrentTickerEntryIndex := 0;
          end;
          TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
          //Update the data from the database - called for each record
          UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
          TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);
          ////////////////////////////////////////////////////////////////////////
          //Added for 1-line ticker mode
          ////////////////////////////////////////////////////////////////////////
          if (OneLineModeEnable = TRUE) then
          begin
            OneLineTemplateInfo := GetSingleLineTemplateInfo(TickerRecPtr^.Template_ID);
            //Don't process is invalid template ID; if invalid, jump to next record
            if (OneLineTemplateInfo.OneLine_Template_ID <> -1) then
            begin
              //Modified to fix 1-line skipping problem
              //TickerRecPtr^.Template_ID := OneLineTemplateInfo.OneLine_Template_ID;
              //TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);
              OneLineTemplateID := OneLineTemplateInfo.OneLine_Template_ID;
              TemplateInfo := LoadTempTemplateFields(OneLineTemplateID);
              //Set dwell time for this page to the default dwell time for the 1-line template
              //TickerRecPtr^.DwellTime := TemplateInfo.Default_Dwell;
              OneLineTemplateDwell := TemplateInfo.Default_Dwell;
              //Check for second page; if required, store template ID in global variable
              //Also check to see that second page has data
              if (TickerRecPtr^.SecondLineHasData) then
              begin
                TemplateHasSecondOneLinePage := OneLineTemplateInfo.OneLine_SecondTemplate_Enable;
                SecondOneLinePageTemplateID := OneLineTemplateInfo.OneLine_SecondTemplate_ID;
              end
              else TemplateHasSecondOneLinePage := FALSE;
              //Now, check to see if we should skip this title page (i.e. it has been flagged using a style chip)
              SkipTitlePage := FALSE;
              for k := 1 to 25 do
                if (Pos('^^SF', TickerRecPtr^.UserData[k]) > 0) then SkipTitlePage := TRUE;
              //Log skip page flag set
              if (SkipTitlePage) then WriteToErrorLog('104 Setting flag to skip page due to style chip code for page: ' +
                IntToStr(TickerRecPtr^.Event_Index));
            end
            else begin
              //Log template not found
              WriteToErrorLog('105 1-Line template ID not found for template ID: ' + IntToStr(TickerRecPtr^.Template_ID));
              //Enable jump to next record timer & exit procedure
              if (RunningTicker = TRUE) then
              begin
                JumpToNextTickerRecordTimer.Enabled := TRUE;
                Exit;
              end;
            end;
          end;
          //Check for use of game data
          if (TemplateInfo.UsesGameData) then
          begin
            try
              if (TickerRecPtr^.Odds_GameID > 0) then
                //Use odds only
                CurrentGameData := GetGameData(TickerRecPtr^.League, IntToStr(TickerRecPtr^.Odds_GameID), TRUE)
              else
                //Use full game data
                CurrentGameData := GetGameData(TickerRecPtr^.League, TickerRecPtr^.SI_GCode, FALSE);
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for ticker');
            end;
            FoundGame := CurrentGameData.DataFound;
            //Log if game not found
            if (FoundGame = FALSE) then
              WriteToErrorLog('106 Game not found for Game ID: ' + TickerRecPtr^.SI_GCode);
          end
          else CurrentGameData.State := -1;
        Until ((TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.State = TemplateInfo.RequiredGameState) OR
              ((CurrentGameData.State > 3) AND (TemplateInfo.RequiredGameState = 3)) OR
              (CurrentGameData.State = -1)) AND ((TickerRecPtr^.Enabled = TRUE) AND
              (TickerRecPtr^.StartEnableDateTime <= Now) AND (TickerRecPtr^.EndEnableDateTime > Now));

        //Set the default dwell time - minimum = 2000mS; set packet timeout interval
        //Special case for 1-line ticker mode
        if (OneLineModeEnable = TRUE) then
        begin
          if (OneLineTemplateDwell > 2000) then
          begin
            TickerCommandDelayTimer.Interval := OneLineTemplateDwell;
            TickerPacketTimeoutTimer.Interval := OneLineTemplateDwell + 5000;
          end
          else begin
            TickerCommandDelayTimer.Interval := 2000;
            TickerPacketTimeoutTimer.Interval := OneLineTemplateDwell + 7000;
          end;
        end
        else begin
          //Set the default dwell time - minimum = 2000mS; set packet timeout interval
          if (TickerRecPtr^.DwellTime > 2000) then
          begin
            //If sponsor logo dwell is specified, it's a sponsor logo, so use that for the dwell time
            if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
            begin
              TickerCommandDelayTimer.Interval := (TickerRecPtr^.SponsorLogo_Dwell*1000);
              TickerPacketTimeoutTimer.Interval := (TickerRecPtr^.SponsorLogo_Dwell*1000) + 5000;
            end
            else begin
              TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
              TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 5000;
            end;
          end
          else begin
            TickerCommandDelayTimer.Interval := 2000;
            TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 7000;
          end;
        end;

        //Set the template type
        CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;
        //Add the fields from the temporary fields collection
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            //Filter out fields ID values of -1 => League designator
            if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
            begin
              //Add region command
              //If not a symbolic fields, send field contents directly
              if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
              begin
                TempStr := TemplateFieldsRecPtr^.Field_Contents;
                if (Trim(TempStr) = '') then TempStr := ' ';
                CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                  ETX + TempStr + GS;
              end
              //It's a symbolic name, so get the field contents
              else begin
                try
                  TempStr := GetValueOfSymbol(TICKER, TemplateFieldsRecPtr^.Field_Contents, CurrentTickerEntryIndex,
                    CurrentGameData, CurrentTickerDisplayMode);
                except
                  if (ErrorLoggingEnabled = True) then
                    WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                end;
                if (Trim(TempStr) = '') then TempStr := ' ';
                CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr + GS;
              end;
            end;
          end;
          //Write out to the as-run log if it's a sponsor logo
          if (Trim(TickerRecPtr^.SponsorLogo_Name) <> '') then
            WriteToAsRunLog('Started display of sponsor logo: ' + CurrentSponsorLogoName);
        end;

        //Set data row indicator in Ticker grid
        if (NEXTRECORD) then
        begin
          try
            MainForm.TickerPlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex+1;
            MainForm.TickerPlaylistGrid.SetTopLeft(1, MainForm.TickerPlaylistGrid.CurrentDataRow);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reposition main ticker grid cursor to correct record');
          end;
        end;

        //Set ticker display mode control region
        //Modified for 1-line ticker mode
        if ((NEXTRECORD = FALSE) AND (OneLineModeEnable = FALSE)) OR (SwitchingModes) then
        begin
          CmdStr := CmdStr + 'SD' + ETX + '1001' + ETX + IntToStr(CurrentTickerDisplayMode) + GS;
          //Clear flag to prevent ticker mode command from being sent the next time around
          SwitchingModes := FALSE;
          //Re-enable bug - fire event handler for standard bug timer
          BugCommandDelayTimerTimer(Self);
        end;

        //Set the lineup text
        try
          if (GetLineuptext(CurrentTickerEntryIndex, CurrentTickerDisplayMode).UseLineup = TRUE) then
          begin
            LineupData := GetLineuptext(CurrentTickerEntryIndex, CurrentTickerDisplayMode);
            if (Trim(LineupData.TextFields[1]) = 'NONE') then
              LineupData.TextFields[1] := ' ';
            CmdStr := CmdStr + 'SI' + ETX + LineupData.TextFields[1] + ETX +
                      LineupData.TextFields[2] + ETX +
                      LineupData.TextFields[3] + ETX +
                      LineupData.TextFields[4] + ETX +
                      LineupData.TextFields[5] + GS;
          end;
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to get and set main ticker line-up text');
        end;

        //Resubstitute lower-case formatting strings and line wrap command
        CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[X', '[x', [rfReplaceAll]);
        //Modified for 1-line ticker
        if (OneLineModeEnable = FALSE) then
          CmdStr := StringReplace(CmdStr, '|', '[r 0]', [rfReplaceAll]);

        //Substitute style chip codes
        try
          CmdStr := ProcessStyleChips(CmdStr);
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to process style chips for main ticker record');
        end;

        //Send the new data
        CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
        begin
          //Enable packet timeout timer
          if (NEXTRECORD) then TickerPacketTimeoutTimer.Enabled := TRUE;
          //Send command
          try
            if (OKToGo) then
            begin
              //Special code here to deal with skipping title page; if skipping, just fire second page
              if (SkipTitlePage) then
              begin
                Send2ndPageTickerRecord;
                //Clear flag to prevent page from being sent again
                TemplateHasSecondOneLinePage := FALSE;
                //Log if skipping page
                WriteToErrorLog('107 Skipping page - SkipTitlePage flag set: ' + IntToStr(TickerRecPtr^.Event_Index));
              end
              else
                ApdWinsockPort1.Output := CmdStr;
            end;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying send ticker command to engine');
          end;
          //Start timer to send next record
          //Modified for one line ticker mode
          if (USEDATAPACKET = FALSE) AND (OkToGo) AND (NEXTRECORD = TRUE) AND (SkipTitlePage = FALSE) then
            TickerCommandDelayTimer.Enabled := TRUE;
        end
        //Game not found, so don't send commands & go to next command
        else if (FoundGame = FALSE) then
        begin
          //Enable delay timer
          if (RunningTicker = TRUE) then
          begin
            JumpToNextTickerRecordTimer.Enabled := TRUE;
            //Log if game not found
            WriteToErrorLog('108 Skipping page - Game Not Found flag set: ' + TickerRecPtr^.SI_GCode);
          end;
        end;
      end
      //OK to go was set to false
      else begin
        //Enable delay timer
        if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer.Enabled := TRUE;
        //Log if game not found
        WriteToErrorLog('109 Skipping page - OK to Go flag was cleared: ' + IntToStr(TickerRecPtr^.Event_Index));
      end;
    end;
    //Turn on packet
    ApdDataPacket1.Enabled := TRUE;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send additional records to ticker');
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// Procedure for sending 2nd page of 2-line template when in 1-line ticker mode
////////////////////////////////////////////////////////////////////////////////
procedure TEngineInterface.Send2ndPageTickerRecord;
var
  i,j: SmallInt;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  FoundGame: Boolean;
  IsLast: Boolean;
  OkToGo: Boolean;
  TickerRecPtr: ^TickerRec;
  CmdStr: String;
  TemplateInfo: TemplateDefsRec;
  //Added for 1-line ticker mode
  OneLineTemplateInfo: OneLineTemplateInfoRec;
  CurrentGameState: SmallInt;
  CurrentGameData: GameRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  LineupData: LineupText;
  TempStr: String;
  TwoLineTemplateDwell: SmallInt;
  TwoLineTemplateID: SmallInt;
begin
  //Init flags
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  //Init FoundGame flag - used to prevent inadverant commands from being sent if game not found
  FoundGame := TRUE;

  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;

  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Init
    IsLast := FALSE;

    //Make sure we haven't dumped put
    if ((TickerAbortFlag = FALSE) AND (Ticker_Collection.Count > 0) AND (TemplateHasSecondOneLinePage)) then
    begin
      OKToGo := TRUE;

      //Proceed if not at end or in looping mode and at beggining
      if (CurrentTickerEntryIndex <= Ticker_Collection.Count-1) AND (OKToGo) then
      begin
        //Get pointer to current record
        TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);

        //Update the data from the database - called for each record
        UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);

        //Set default mode
        CmdStr := SOT + 'SM' + ETX + '2' + GS;

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR TICKER FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);
        //Set template ID to ID for 2nd 1-line template page & load fields
        //Modified to fix 1-line skipping problem
        //TickerRecPtr^.Template_ID := SecondOneLinePageTemplateID;
        TwoLineTemplateID := SecondOneLinePageTemplateID;
        TemplateInfo := LoadTempTemplateFields(TwoLineTemplateID);

        //Set dwell time
        TwoLineTemplateDwell := TemplateInfo.Default_Dwell;

        //Get game data if template requires it
        if (TemplateInfo.UsesGameData) then
        begin
          try
            if (TickerRecPtr^.Odds_GameID > 0) then
              //Use odds only
              CurrentGameData := GetGameData(TickerRecPtr^.League, IntToStr(TickerRecPtr^.Odds_GameID), TRUE)
            else
              //Use full game data
              CurrentGameData := GetGameData(TickerRecPtr^.League, TickerRecPtr^.SI_GCode, FALSE);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to get game data for ticker');
          end;
          FoundGame := CurrentGameData.DataFound;
        end;

        //Set the default dwell time - minimum = 2000mS; set packet timeout interval
        //Special case for 1-line ticker mode
        if (OneLineModeEnable = TRUE) then
        begin
          if (TwoLineTemplateDwell > 2000) then
          begin
            TickerCommandDelayTimer.Interval := TwoLineTemplateDwell;
            TickerPacketTimeoutTimer.Interval := TwoLineTemplateDwell + 5000;
          end
          else begin
            TickerCommandDelayTimer.Interval := 2000;
            TickerPacketTimeoutTimer.Interval := TwoLineTemplateDwell + 7000;
          end;
        end
        else begin
          if (TickerRecPtr^.DwellTime > 2000) then
          begin
            //If sponsor logo dwell is specified, it's a sponsor logo, so use that for the dwell time
            if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
            begin
              TickerCommandDelayTimer.Interval := (TickerRecPtr^.SponsorLogo_Dwell*1000);
              TickerPacketTimeoutTimer.Interval := (TickerRecPtr^.SponsorLogo_Dwell*1000) + 5000;
            end
            else begin
              TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
              TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 5000;
            end;
          end
          else begin
            TickerCommandDelayTimer.Interval := 2000;
            TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 7000;
          end;
        end;

        //Set the template type
        CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;
        //Add the fields from the temporary fields collection
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            //Filter out fields ID values of -1 => League designator
            if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
            begin
              //Add region command
              //If not a symbolic fields, send field contents directly
              if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
              begin
                TempStr := TemplateFieldsRecPtr^.Field_Contents;
                if (Trim(TempStr) = '') then TempStr := ' ';
                CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                  ETX + TempStr + GS;
              end
              //It's a symbolic name, so get the field contents
              else begin
                try
                  TempStr := GetValueOfSymbol(TICKER, TemplateFieldsRecPtr^.Field_Contents, CurrentTickerEntryIndex,
                    CurrentGameData, CurrentTickerDisplayMode);
                except
                  if (ErrorLoggingEnabled = True) then
                    WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                end;
                if (Trim(TempStr) = '') then TempStr := ' ';
                CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr + GS;
              end;
            end;
          end;
          //Write out to the as-run log if it's a sponsor logo
          if (Trim(TickerRecPtr^.SponsorLogo_Name) <> '') then
            WriteToAsRunLog('Started display of sponsor logo: ' + CurrentSponsorLogoName);
        end;

        //Set ticker display mode control region
        //Modified for 1-line ticker mode
        if (SwitchingModes) then
        begin
          CmdStr := CmdStr + 'SD' + ETX + '1001' + ETX + IntToStr(CurrentTickerDisplayMode) + GS;
          //Clear flag to prevent ticker mode command from being sent the next time around
          SwitchingModes := FALSE;
          //Re-enable bug - fire event handler for standard bug timer
          BugCommandDelayTimerTimer(Self);
        end;

        //Set the lineup text
        try
          if (GetLineuptext(CurrentTickerEntryIndex, CurrentTickerDisplayMode).UseLineup = TRUE) then
          begin
            LineupData := GetLineuptext(CurrentTickerEntryIndex, CurrentTickerDisplayMode);
            if (Trim(LineupData.TextFields[1]) = 'NONE') then
              LineupData.TextFields[1] := ' ';
            CmdStr := CmdStr + 'SI' + ETX + LineupData.TextFields[1] + ETX +
                      LineupData.TextFields[2] + ETX +
                      LineupData.TextFields[3] + ETX +
                      LineupData.TextFields[4] + ETX +
                      LineupData.TextFields[5] + GS;
          end;
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to get and set main ticker line-up text');
        end;

        //Resubstitute lower-case formatting strings and line wrap command
        CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[X', '[x', [rfReplaceAll]);
        //Modified for 1-line ticker
        if (OneLineModeEnable = FALSE) then
          CmdStr := StringReplace(CmdStr, '|', '[r 0]', [rfReplaceAll]);

        //Substitute style chip codes
        try
          CmdStr := ProcessStyleChips(CmdStr);
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to process style chips for main ticker record');
        end;

        //Send the new data
        CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
        begin
          //Enable packet timeout timer
          TickerPacketTimeoutTimer.Enabled := TRUE;
          //Send command
          try
            ApdWinsockPort1.Output := CmdStr;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying send ticker command to engine');
          end;
          //Start timer to send next record
          if (USEDATAPACKET = FALSE) then TickerCommandDelayTimer.Enabled := TRUE;
        end
        //Game not found, so don't send commands & go to next command
        else if (FoundGame = FALSE) then
        begin
          //Enable delay timer
          if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer.Enabled := TRUE;
        end;
      end
      //OK to go was set to false
      else begin
        //Enable delay timer
        if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer.Enabled := TRUE;
      end;
    end;
    //Turn on packet
    ApdDataPacket1.Enabled := TRUE;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send additional records to ticker');
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// BUG
////////////////////////////////////////////////////////////////////////////////
//Procedure to send next record after receipt of packet
procedure TEngineInterface.SendBugRecord(NextRecord: Boolean; RecordIndex: SmallInt);
var
  i,j: SmallInt;
  TempStr, CmdStr: String;
  BugRecPtr: ^BugRec;
  FoundGame: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  IsLast: Boolean;
  OverlayIndex, OverlayIndexNext: SmallInt;
  IsNewOverlay, PadText: Boolean;
  InGameStatsRequired: Boolean;
  FoundGameInDatamart: Boolean;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentGameData: GameRec;
  BugClockPhaseStr: String;
  OkToGo: Boolean;
begin
  //Init flags
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  //Init FoundGame flag - used to prevent inadverant commands from being sent if game not found
  FoundGame := TRUE;
  RecordIsSponsor := FALSE;

  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;

  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Init
    IsLast := FALSE;

    //Make sure we haven't dumped put
    //Modified for 1-Line ticker
    if ((TickerAbortFlag = FALSE) AND (Bug_Collection.Count > 0)) AND (OneLineModeEnable = FALSE) then
    begin
      //Init
      BugClockPhaseStr := ' ';
      OKToGo := TRUE;
      //Check if flag set to just send next record
      if (NEXTRECORD = TRUE) then
      begin
         //Make sure there's at least one record left before sending more data;
        //If not, reset to top if in looping mode
        if (CurrentBugEntryIndex >= Bug_Collection.Count-1) AND
           (LoopTicker = TRUE) then
        begin
          //Reload the Bug collection in case it has been modified; clear flag to indicate
          //it's not the first time through
          try
            MainForm.LoadPlaylistCollection(Bug, 0, CurrentBugPlaylistID);
            if (Bug_Collection.Count > 0) then CurrentBugEntryIndex := 0;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reload bug collection');
          end;
          //Check time window and enable
          BugRecPtr := Bug_Collection.At(CurrentBugEntryIndex);
          //Make sure record is enabled and within time window
          j := 0;
          While ((BugRecPtr^.Enabled = FALSE) OR NOT ((BugRecPtr^.StartEnableDateTime <= Now) AND
                (BugRecPtr^.EndEnableDateTime > Now))) AND (j < Bug_Collection.Count) do
          begin
            Inc(CurrentBugEntryIndex);
            //Set pointer
            BugRecPtr := Bug_Collection.At(CurrentBugEntryIndex);
            //Update the data from the database - called for each record
            UpdateBugRecordDataFromDB(CurrentBugPlaylistID, CurrentBugEntryIndex, BugRecPtr^.Event_GUID);
            if (CurrentBugEntryIndex = Bug_Collection.Count) then
            begin
              try
                MainForm.LoadPlaylistCollection(BUG, 0, CurrentBugPlaylistID);
                CurrentBugEntryIndex := 0;
                BugRecPtr := Bug_Collection.At(CurrentBugEntryIndex);
                //Update the data from the database - called for each record
                UpdateBugRecordDataFromDB(CurrentBugPlaylistID, CurrentBugEntryIndex, BugRecPtr^.Event_GUID);
                j := 0;
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to reload bug collection');
              end;
            end;
            Inc(j);
          end;
          //Set flag if no enabled records found
          if (j >= Bug_Collection.Count) then OKToGo := FALSE;
        end
        //Not last record, so increment Bug playout collection object index
        else if (CurrentBugEntryIndex < Bug_Collection.Count-1) then
        begin
          //Make sure record is enabled and within time window
          j := 0;
          Repeat
            Inc(CurrentBugEntryIndex);
            if (CurrentBugEntryIndex = Bug_Collection.Count) then
            begin
              try
                MainForm.LoadPlaylistCollection(Bug, 0, CurrentBugPlaylistID);
                CurrentBugEntryIndex := 0;
                //Check time window and enable
                BugRecPtr := Bug_Collection.At(CurrentBugEntryIndex);
                //Update the data from the database - called for each record
                UpdateBugRecordDataFromDB(CurrentBugPlaylistID, CurrentBugEntryIndex, BugRecPtr^.Event_GUID);
                j := 0;
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to reload bug collection');
              end;
            end;
            BugRecPtr := Bug_Collection.At(CurrentBugEntryIndex);
            Inc (j);
            //Update the data from the database - called for each record
            UpdateBugRecordDataFromDB(CurrentBugPlaylistID, CurrentBugEntryIndex, BugRecPtr^.Event_GUID);
          Until ((BugRecPtr^.Enabled = TRUE) AND (BugRecPtr^.StartEnableDateTime <= Now) AND
                (BugRecPtr^.EndEnableDateTime > Now)) OR (j >= Bug_Collection.Count);
          //Set flag if no enabled records found
          if (j >= Bug_Collection.Count) then OKToGo := FALSE;
        end
        //At end and not looping, so clear screen
        else if (CurrentBugEntryIndex >= Bug_Collection.Count-1) AND
                (LoopTicker = FALSE) then
        begin
          //Increment to end
          Inc(CurrentBugEntryIndex);
        end;
        //Set flag if at last collection obejct and not looping
        if (CurrentBugEntryIndex = Bug_Collection.Count-1) AND
           (LoopTicker = FALSE) then IsLast := TRUE;
        //Enable command timer
        DisableCommandTimer := FALSE;
      end
      //Triggering specific record, so set current entry index
      else begin
        CurrentBugEntryIndex := RecordIndex;
        //Disble command timer
        DisableCommandTimer := TRUE;
      end;
      //Proceed if not at end or in looping mode and at beggining
      if (CurrentBugEntryIndex <= Bug_Collection.Count-1) AND (OKToGo = TRUE) then
      begin
        //Get pointer to current record
        BugRecPtr := Bug_Collection.At(CurrentBugEntryIndex);

        //Update the data from the database - called for each record
        UpdateBugRecordDataFromDB(CurrentBugPlaylistID, CurrentBugEntryIndex, BugRecPtr^.Event_GUID);

        //Set default mode
        CmdStr := SOT + 'SM' + ETX + '2' + GS;

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR Bug FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        //Add bias to template ID if in Live Event mode
        if (CurrentTickerDisplayMode = 1) then
          TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID+50)
        else
          TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID);

        //Get game data if template requires it
        if (TemplateInfo.UsesGameData) then
        begin
          try
            if (BugRecPtr^.Odds_GameID > 0) then
              //Use odds only
              CurrentGameData := GetGameData(BugRecPtr^.League, IntToStr(BugRecPtr^.Odds_GameID), TRUE)
            else
              //Use full game data
              CurrentGameData := GetGameData(BugRecPtr^.League, BugRecPtr^.SI_GCode, FALSE);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to get game data for bug');
          end;
          FoundGame := CurrentGameData.DataFound;
        end;

        //Check the game state vs. the state required by the template if not a match, increment
        //Also do check for enabled entries and check time window
        if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.State <> TemplateInfo.RequiredGameState) AND
          //Special case for postponed, delayed or suspended MLB games
          (NOT((CurrentGameData.State > 3) AND (TemplateInfo.RequiredGameState = 3))) AND
          (NEXTRECORD = TRUE) then
        Repeat
          Inc(CurrentBugEntryIndex);
          if (CurrentBugEntryIndex = Bug_Collection.Count) then
          begin
            MainForm.LoadPlaylistCollection(Bug, 0, CurrentBugPlaylistID);
            CurrentBugEntryIndex := 0;
          end;
          BugRecPtr := Bug_Collection.At(CurrentBugEntryIndex);
          UpdateBugRecordDataFromDB(CurrentBugPlaylistID, CurrentBugEntryIndex, BugRecPtr^.Event_GUID);
          //Add bias to template ID if in Live Event mode
          if (CurrentTickerDisplayMode = 1) then
            TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID+50)
          else
            TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID);
          if (TemplateInfo.UsesGameData) then
          begin
            try
              if (BugRecPtr^.Odds_GameID > 0) then
                //Use odds only
                CurrentGameData := GetGameData(BugRecPtr^.League, IntToStr(BugRecPtr^.Odds_GameID), TRUE)
              else
                //Use full game data
                CurrentGameData := GetGameData(BugRecPtr^.League, BugRecPtr^.SI_GCode, FALSE);
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for bug');
            end;
            FoundGame := CurrentGameData.DataFound;
          end
          else CurrentGameData.State := -1;
        Until ((TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.State = TemplateInfo.RequiredGameState) OR
              ((CurrentGameData.State > 3) AND (TemplateInfo.RequiredGameState = 3)) OR
              (CurrentGameData.State = -1)) AND ((BugRecPtr^.Enabled = TRUE) AND
              (BugRecPtr^.StartEnableDateTime <= Now) AND (BugRecPtr^.EndEnableDateTime > Now));

        //Set the default dwell time - minimum = 2000mS; set packet timeout interval
        if (BugRecPtr^.DwellTime > 2000) then
        begin
          BugCommandDelayTimer.Interval := BugRecPtr^.DwellTime;
          BugPacketTimeoutTimer.Interval := BugRecPtr^.DwellTime + 5117;
        end
        else begin
          BugCommandDelayTimer.Interval := 2000;
          BugPacketTimeoutTimer.Interval := BugRecPtr^.DwellTime + 7117;
        end;

        //Set the template type
        CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;
        //Add the fields from the temporary fields collection
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            //Filter out fields ID values of -1 => League designator
            if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
            begin
              //Add region command
              //If not a symbolic fields, send field contents directly
              if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
              begin
                TempStr := TemplateFieldsRecPtr^.Field_Contents;
                if (Trim(TempStr) = '') then TempStr := ' ';
                CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                  ETX + TempStr + GS;
              end
              //It's a symbolic name, so get the field contents
              else begin
                try
                  TempStr := GetValueOfSymbol(BUG, TemplateFieldsRecPtr^.Field_Contents, CurrentBugEntryIndex,
                    CurrentGameData, CurrentTickerDisplayMode);
                except
                  if (ErrorLoggingEnabled = True) then
                    WriteToErrorLog('Error occurred while trying to get value for bug data field');
                end;
                if (Trim(TempStr) = '') then TempStr := ' ';
                //Check to see if it's the bug clock/phase field; if so, store for insertion into line-up
                if (TemplateFieldsRecPtr^.Engine_Field_ID = 2000) then
                  BugClockPhaseStr := TempStr
                else
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr + GS;
              end;
            end;
          end;
        end;

        //Set data row indicator in Bug grid
        if (NEXTRECORD) then
        begin
          try
            MainForm.BugPlaylistGrid.CurrentDataRow := CurrentBugEntryIndex+1;
            MainForm.BugPlaylistGrid.SetTopLeft(1, MainForm.BugPlaylistGrid.CurrentDataRow);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reposition bug grid cursor to correct record');
          end;
        end;

        //Set ticker display mode control region
        //Modified to support 1-line ticker mode
        if (OneLineModeEnable = FALSE) then
          CmdStr := CmdStr + 'SD' + ETX + '1001' + ETX + IntToStr(CurrentTickerDisplayMode) + GS;

        //Set lineup text
        if (CurrentTickerDisplayMode = 1) then BugClockPhaseStr := ' ';
        if (Trim(BugRecPtr^.League) = 'NONE') then
          CmdStr := CmdStr + 'SI' + ETX + ' ' + ETX + BugClockPhaseStr +
                    ETX + ' ' + ETX + ' ' + ETX + ' ' + GS
        else
          CmdStr := CmdStr + 'SI' + ETX + BugRecPtr^.League + ETX + BugClockPhaseStr +
                    ETX + ' ' + ETX + ' ' + ETX + ' ' + GS;

        //Resubstitute lower-case formatting strings and line wrap command
        CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '|', '[r 0]', [rfReplaceAll]);

        //Substitute style chip codes
        try
          CmdStr := ProcessStyleChips(CmdStr);
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to process style chips for bug record');
        end;

        //Send the new data
        CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
        begin
          //Enable packet timeout timer
          if (NEXTRECORD) then BugPacketTimeoutTimer.Enabled := TRUE;
          //Send command
          try
            ApdWinsockPort1.Output := CmdStr;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying send bug command to engine');
          end;
          //Start timer to send next record
          if (USEDATAPACKET = FALSE) AND (NEXTRECORD = TRUE) then BugCommandDelayTimer.Enabled := TRUE;
        end
        //Game not found, so don;t send commands & go to next command
        else if (FoundGame = FALSE) then
        begin
          //Enable delay timer
          if (RunningTicker = TRUE) then JumpToNextBugRecordTimer.Enabled := TRUE;
        end;
      end
      //OK to go was set to false
      else begin
        //Enable delay timer
        if (RunningTicker = TRUE) then JumpToNextBugRecordTimer.Enabled := TRUE;
      end;
    end;
    //Turn on packet
    ApdDataPacket1.Enabled := TRUE;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send additional records to Bug');
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// EXTRA LINE
////////////////////////////////////////////////////////////////////////////////
//Procedure to send next record after receipt of packet or on timer
procedure TEngineInterface.SendExtraLineRecord(NextRecord: Boolean; RecordIndex: SmallInt);
var
  i,j: SmallInt;
  TempStr, CmdStr: String;
  ExtraLineRecPtr, ExtraLineRecPtrNew: ^ExtraLineRec;
  FoundGame: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  IsLast: Boolean;
  InGameStatsRequired: Boolean;
  FoundGameInDatamart: Boolean;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentGameData: GameRec;
  OkToGo: Boolean;
  ExtendDelay: Boolean;
begin
  //Init flags
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  ExtendDelay := FALSE;
  //Init FoundGame flag - used to prevent inadverant commands from being sent if game not found
  FoundGame := TRUE;

  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;

  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Init
    IsLast := FALSE;
    //Set flag to extend delay if running opening animation
    if (CurrentExtraLineEntryIndex = -1) then ExtendDelay := TRUE;

    //Make sure we haven't dumped put
    //Modified for 1-Line ticker
    if ((TickerAbortFlag = FALSE) AND (ExtraLine_Collection.Count > 0)) AND (OneLineModeEnable = FALSE) then
    begin
      OKToGo := TRUE;
      //Check if flag set to just send next record
      if (NEXTRECORD = TRUE) then
      begin
        //Make sure there's at least one record left before sending more data;
        //If not, reset to top if in looping mode
        if (CurrentExtraLineEntryIndex >= ExtraLine_Collection.Count-1) AND
           (LoopTicker = TRUE) then
        begin
          try
            //Reload the ExtraLine collection in case it has been modified; clear flag to indicate
            //it's not the first time through
            MainForm.LoadPlaylistCollection(EXTRALINE, 0, CurrentExtraLinePlaylistID);
            if (ExtraLine_Collection.Count > 0) then CurrentExtraLineEntryIndex := 0;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reload main ExtraLine collection');
          end;
          //Check time window and enable
          ExtraLineRecPtr := ExtraLine_Collection.At(CurrentExtraLineEntryIndex);
          //Make sure record is enabled and within time window
          j := 0;
          While ((ExtraLineRecPtr^.Enabled = FALSE) OR NOT((ExtraLineRecPtr^.StartEnableDateTime <= Now) AND
                (ExtraLineRecPtr^.EndEnableDateTime > Now))) AND (j < ExtraLine_Collection.Count) do
          begin
            Inc(CurrentExtraLineEntryIndex);
            if (CurrentExtraLineEntryIndex = ExtraLine_Collection.Count) then
            begin
              try
                MainForm.LoadPlaylistCollection(EXTRALINE, 0, CurrentExtraLinePlaylistID);
                CurrentExtraLineEntryIndex := 0;
                //Check time window and enable
                ExtraLineRecPtr := ExtraLine_Collection.At(CurrentExtraLineEntryIndex);
                j := 0;
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to reload main ExtraLine collection');
              end;
            end;
            //Check time window and enable
            ExtraLineRecPtr := ExtraLine_Collection.At(CurrentExtraLineEntryIndex);
            Inc(j);
          end;
          //Set flag if no enabled records found
          if (j >= ExtraLine_Collection.Count) then OKToGo := FALSE;
        end
        //Not last record, so increment ExtraLine playout collection object index
        else if (CurrentExtraLineEntryIndex < ExtraLine_Collection.Count-1) then
        begin
          //Make sure record is enabled and within time window
          j := 0;
          Repeat
            Inc(CurrentExtraLineEntryIndex);
            if (CurrentExtraLineEntryIndex = ExtraLine_Collection.Count) then
            begin
              try
                MainForm.LoadPlaylistCollection(EXTRALINE, 0, CurrentExtraLinePlaylistID);
                CurrentExtraLineEntryIndex := 0;
                //Check time window and enable
                ExtraLineRecPtr := ExtraLine_Collection.At(CurrentExtraLineEntryIndex);
                j := 0;
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to reload main ExtraLine collection');
              end;
            end;
            ExtraLineRecPtr := ExtraLine_Collection.At(CurrentExtraLineEntryIndex);
            Inc (j);
          Until ((ExtraLineRecPtr^.Enabled = TRUE) AND (ExtraLineRecPtr^.StartEnableDateTime <= Now) AND
                (ExtraLineRecPtr^.EndEnableDateTime > Now)) OR (j >= ExtraLine_Collection.Count);
          //Set flag if no enabled records found
          if (j >= ExtraLine_Collection.Count) then OKToGo := FALSE;
        end
        //At end and not looping, so clear screen
        else if (CurrentExtraLineEntryIndex >= ExtraLine_Collection.Count-1) AND
                (LoopTicker = FALSE) then
        begin
          //Increment to end
          Inc(CurrentExtraLineEntryIndex);
        end;
        //Set flag if at last collection obejct and not looping
        if (CurrentExtraLineEntryIndex = ExtraLine_Collection.Count-1) AND
           (LoopTicker = FALSE) then IsLast := TRUE;
        //Enable command timer
        DisableCommandTimer := FALSE;
      end
      //Triggering specific record, so set current entry index
      else begin
        CurrentExtraLineEntryIndex := RecordIndex;
        //Disble command timer
        DisableCommandTimer := TRUE;
      end;

      //Proceed if not at end or in looping mode and at beggining
      if (CurrentExtraLineEntryIndex <= ExtraLine_Collection.Count-1) AND (OkToGo) then
      begin
        //Get pointer to current record
        ExtraLineRecPtr := ExtraLine_Collection.At(CurrentExtraLineEntryIndex);

        //Set default mode
        CmdStr := SOT + 'SM' + ETX + '2' + GS;

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR ExtraLine FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        TemplateInfo := LoadTempTemplateFields(ExtraLineRecPtr^.Template_ID);

        //Get game data if template requires it
        if (TemplateInfo.UsesGameData) then
        begin
          try
            if (ExtraLineRecPtr^.Odds_GameID > 0) then
              //Use odds only
              CurrentGameData := GetGameData(ExtraLineRecPtr^.League, IntToStr(ExtraLineRecPtr^.Odds_GameID), TRUE)
            else
              //Use full game data
              CurrentGameData := GetGameData(ExtraLineRecPtr^.League, ExtraLineRecPtr^.SI_GCode, FALSE);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to get game data for ExtraLine');
          end;
          FoundGame := CurrentGameData.DataFound;
        end;

        //Check the game state vs. the state required by the template if not a match, increment
        //Also do check for enabled entries and check time window
        if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.State <> TemplateInfo.RequiredGameState) AND
          //Special case for postponed, delayed or suspended MLB games
          (NOT((CurrentGameData.State > 3) AND (TemplateInfo.RequiredGameState = 3))) AND
          (NEXTRECORD = TRUE) then
        Repeat
          Inc(CurrentExtraLineEntryIndex);
          if (CurrentExtraLineEntryIndex = ExtraLine_Collection.Count) then
          begin
            MainForm.LoadPlaylistCollection(ExtraLine, 0, CurrentExtraLinePlaylistID);
            CurrentExtraLineEntryIndex := 0;
            //Get new record pointer
            ExtraLineRecPtr := ExtraLine_Collection.At(CurrentExtraLineEntryIndex);
          end;
          ExtraLineRecPtr := ExtraLine_Collection.At(CurrentExtraLineEntryIndex);
          TemplateInfo := LoadTempTemplateFields(ExtraLineRecPtr^.Template_ID);
          if (TemplateInfo.UsesGameData) then
          begin
            try
              if (ExtraLineRecPtr^.Odds_GameID > 0) then
                //Use odds only
                CurrentGameData := GetGameData(ExtraLineRecPtr^.League, IntToStr(ExtraLineRecPtr^.Odds_GameID), TRUE)
              else
                //Use full game data
                CurrentGameData := GetGameData(ExtraLineRecPtr^.League, ExtraLineRecPtr^.SI_GCode, FALSE);
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for ExtraLine');
            end;
            FoundGame := CurrentGameData.DataFound;
          end
          else CurrentGameData.State := -1;
        Until ((TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.State = TemplateInfo.RequiredGameState) OR
              ((CurrentGameData.State > 3) AND (TemplateInfo.RequiredGameState = 3)) OR
              (CurrentGameData.State = -1)) AND ((ExtraLineRecPtr^.Enabled = TRUE) AND
              (ExtraLineRecPtr^.StartEnableDateTime <= Now) AND (ExtraLineRecPtr^.EndEnableDateTime > Now));

        //Set the default dwell - minimum = 2000mS
        if (ExtraLineRecPtr^.DwellTime > 2000) then
        begin
          ExtraLineCommandDelayTimer.Interval := ExtraLineRecPtr^.DwellTime + 212;
          ExtraLinePacketTimeoutTimer.Interval := ExtraLineRecPtr^.DwellTime + 5212;
          //Extend delay if first entry to account for opening animation
          if (ExtendDelay) then
          begin
            ExtraLineCommandDelayTimer.Interval := ExtraLineCommandDelayTimer.Interval + 2000;
            ExtraLinePacketTimeoutTimer.Interval := ExtraLinePacketTimeoutTimer.Interval + 2000;
          end;
        end
        else begin
          ExtraLineCommandDelayTimer.Interval := 2212;
          ExtraLinePacketTimeoutTimer.Interval := ExtraLineRecPtr^.DwellTime + 7212;
        end;

        //Set the template type
        CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;
        //Add the fields from the temporary fields collection
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            //Filter out fields ID values of -1 => League designator
            if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
            begin
              //Add region command
              //If not a symbolic fields, send field contents directly
              if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
              begin
                TempStr := TemplateFieldsRecPtr^.Field_Contents;
                if (Trim(TempStr) = '') then TempStr := ' ';
                CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                  ETX + TempStr + GS;
              end
              //It's a symbolic name, so get the field contents
              else begin
                try
                  TempStr := GetValueOfSymbol(EXTRALINE, TemplateFieldsRecPtr^.Field_Contents, CurrentExtraLineEntryIndex,
                    CurrentGameData, CurrentTickerDisplayMode);
                except
                  if (ErrorLoggingEnabled = True) then
                    WriteToErrorLog('Error occurred while trying to get value for main ExtraLine data field');
                end;
                if (Trim(TempStr) = '') then TempStr := ' ';
                CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr + GS;
              end;
            end;
          end;
        end;

        //Set data row indicator in ExtraLine grid
        if (NEXTRECORD) then
        begin
          try
            MainForm.ExtraLinePlaylistGrid.CurrentDataRow := CurrentExtraLineEntryIndex+1;
            MainForm.ExtraLinePlaylistGrid.SetTopLeft(1, MainForm.ExtraLinePlaylistGrid.CurrentDataRow);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reposition main ExtraLine grid cursor to correct record');
          end;
        end;

        //Set ExtraLine display mode control region
        if (NEXTRECORD = FALSE) then
          CmdStr := CmdStr + 'SD' + ETX + '1001' + ETX + IntToStr(CurrentTickerDisplayMode) + GS;

        //Set the lineup text to blanks
        try
          CmdStr := CmdStr + 'SI' + ETX + ' ' + ETX +
                    ' ' + ETX +
                    ' ' + ETX +
                    ' ' + ETX +
                    ' ' + GS;
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to get and set main ExtraLine line-up text');
        end;

        //Resubstitute lower-case formatting strings and line wrap command
        CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '|', '[r 0]', [rfReplaceAll]);

        //Substitute style chip codes
        try
          CmdStr := ProcessStyleChips(CmdStr);
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to process style chips for main ExtraLine record');
        end;

        //Send the new data
        CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
        begin
          //Enable packet timeout timer
          if (NEXTRECORD) then ExtraLinePacketTimeoutTimer.Enabled := TRUE;
          //Send command
          try
            //If breaking news, clear the logo
            if (ScheduledPlaylistInfo[3].Playlist_ID = 0) then DisableLogoClock;
            ApdWinsockPort1.Output := CmdStr;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying send ExtraLine command to engine');
          end;
          //Start timer to send next record
          if (USEDATAPACKET = FALSE) AND (NEXTRECORD = TRUE) then ExtraLineCommandDelayTimer.Enabled := TRUE;
        end
        //Game not found, so don't send commands & go to next command
        else if (FoundGame = FALSE) then
        begin
          //Enable delay timer
          if (RunningTicker = TRUE) then JumpToNextExtraLineRecordTimer.Enabled := TRUE;
        end;
      end
      //OK to go was set to false
      else begin
        //Enable delay timer
        if (RunningTicker = TRUE) then JumpToNextExtraLineRecordTimer.Enabled := TRUE;
      end;
    end;
    //Turn on packet
    ApdDataPacket1.Enabled := TRUE;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send additional records to ExtraLine');
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// SOCKET EVENT HANDLES
////////////////////////////////////////////////////////////////////////////////
//Handler for packet re-enable timer
procedure TEngineInterface.PacketEnableTimerTimer(Sender: TObject);
begin
  //Disable to prevent retrigger
  PacketEnableTimer.Enabled := FALSE;
  //Re-enable packets
  PacketEnable := TRUE;
end;

//Handler to turn off packet recevied indicator after 250 mS
procedure TEngineInterface.PacketIndicatorTimerTimer(Sender: TObject);
begin
  PacketIndicatorTimer.Enabled := FALSE;
  MainForm.ApdStatusLight2.Lit := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// PACKET TIMEOUT TIMERS
////////////////////////////////////////////////////////////////////////////////
//Handler for ticker packet timeout timer - sends next command if status command not
//received
procedure TEngineInterface.TickerPacketTimeoutTimerTimer(Sender: TObject);
begin
  //Disable timer to prevent retriggering
  TickerPacketTimeoutTimer.Enabled := FALSE;
  //Enable delay timer
  if (RunningTicker = TRUE) then
    //Send next record
    SendTickerRecord(TRUE, 0);
  //Log the timeout
  if (ErrorLoggingEnabled = True) then
  begin
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('A timeout occurred while waiting for a Main Ticker command return status from the graphics engine.');
  end;
end;
//Handler for bug packet timeout timer - sends next command if status command not
//received
procedure TEngineInterface.BugPacketTimeoutTimerTimer(Sender: TObject);
begin
  //Disable timer to prevent retriggering
  BugPacketTimeoutTimer.Enabled := FALSE;
  //Enable delay timer
  if (RunningTicker = TRUE) then
    //Send next record
    SendBugRecord(TRUE, 0);
  //Log the timeout
  if (ErrorLoggingEnabled = True) then
  begin
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('A timeout occurred while waiting for a Bug command return status from the graphics engine.');
  end;
end;
//Handler for extra line packet timeout timer - sends next command if status command not
//received
procedure TEngineInterface.ExtraLinePacketTimeoutTimerTimer(Sender: TObject);
begin
  //Disable timer to prevent retriggering
  ExtraLinePacketTimeoutTimer.Enabled := FALSE;
  //Enable delay timer
  if (RunningTicker = TRUE) then
    //Send next record
    SendExtraLineRecord(TRUE, 0);
  //Log the timeout
  if (ErrorLoggingEnabled = True) then
  begin
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('A timeout occurred while waiting for an ExtraLine command return status from the graphics engine.');
  end;
end;

//Handler to light indicator
procedure TEngineInterface.ApdWinsockPort1WsConnect(Sender: TObject);
begin
  MainForm.ApdStatusLight1.Lit := TRUE;
  SocketConnected := TRUE;
end;

//Handler to turn off indicator
procedure TEngineInterface.ApdWinsockPort1WsDisconnect(Sender: TObject);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine');
    if (RunningTicker) then
    begin
      //Force to standard mode
      MainForm.GoToStandardTickerMode;
      //Abort
      MainForm.AbortCurrentEvent;
    end;
  end;
end;

//Handler for socket error
procedure TEngineInterface.ApdWinsockPort1WsError(Sender: TObject; ErrCode: Integer);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine');
    if (RunningTicker) then
    begin
      //Force to standard mode
      MainForm.GoToStandardTickerMode;
      //Abort
      MainForm.AbortCurrentEvent;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// LOGO/CLOCK DISPLAY FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to enable the logo/clock display
procedure TEngineInterface.EnableLogoClock;
var
  CmdStr: String;
begin
  //Don't send commands if not running
  if (RunningTicker = TRUE) then
  begin
    CurrentTimeZone := -6; //1st of three logo cycles
    SendNextClockLogoCommand;
    //Enable the update timer
    DisplayClockTimer.Enabled := TRUE;
  end;
end;

//Procedure to disable the logo/clock display
procedure TEngineInterface.DisableLogoClock;
var
  CmdStr: String;
begin
  //Don't send commands if not running
  if (RunningTicker = TRUE) then
  begin
    //Set default mode
    CmdStr := SOT + 'SM' + ETX + '2' + GS;
    //Set template ID = 70 for logo/clock
    CmdStr := CmdStr + 'ST' + ETX + '70' + GS;
    //Set data fields - clear background region
    CmdStr := CmdStr + 'SD' + ETX + '0' + ETX + 'CLEAR' + GS;
    CmdStr := CmdStr + 'SD' + ETX + '1' + ETX + ' ' + GS;
    CmdStr := CmdStr + 'SD' + ETX + '1000' + ETX + 'CLEAR' + GS;
    CmdStr := CmdStr + 'SD' + ETX + '1002' + ETX + '90' + GS;
    //Send the commsnd
    CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
    if (SocketConnected) AND (EngineParameters.Enabled = TRUE) then
    begin
      //Send command
      ApdWinsockPort1.Output := CmdStr;
    end;
    //Disable the update timer
    DisplayClockTimer.Enabled := FALSE;
  end;
end;

//Function to get the time string to display for the current time zone
procedure TEngineInterface.SendNextClockLogoCommand;
var
  TimeStr: String;
  TimeVal: TDateTime;
  CmdStr: String;
  LogoName: String;
begin
  Case CurrentTimeZone of
       //First three are logo cycles
   -6..-4: begin
         Inc(CurrentTimeZone);
         LogoClockMode := 1;
       end;
       //Pacific time
   -3: begin
         //Increment time zone
         CurrentTimeZone := -2;
         //Set time val
         TimeVal := Now -(3/24);
         //Set string; append time zone suffix
         TimeStr := TimeToStr(TimeVal) + ' PT';
         LogoClockMode := 2;
       end;
       //Mountain time
   -2: begin
         //Increment time zone
         CurrentTimeZone := -1;
         //Set time val
         TimeVal := Now -(2/24);
         //Set string; append time zone suffix
         TimeStr := TimeToStr(TimeVal) + ' MT';
         LogoClockMode := 2;
       end;
       //Central time
   -1: begin
         //Increment time zone
         CurrentTimeZone := 0;
         //Set time val
         TimeVal := Now -(1/24);
         //Set string; append time zone suffix
         TimeStr := TimeToStr(TimeVal) + ' CT';
         LogoClockMode := 2;
       end;
       //Eastern time
    0: begin
         //Increment time zone
         CurrentTimeZone := 1;
         //Set time val
         TimeVal := Now;
         //Set string; append time zone suffix
         TimeStr := TimeToStr(TimeVal) + ' ET';
         LogoClockMode := 2;
       end;
       //Atlantic time
    1: begin
         //Reset time zone to first of three logo cycles
         CurrentTimeZone := -6;
         //Set time val
         TimeVal := Now +(1/24);
         //Set string; append time zone suffix
         TimeStr := TimeToStr(TimeVal) + ' AT';
       end;
  end;

  //If ticker is in live event mode or if extra line is running or if in logo only mode,
  //reset to force logo display
  //Modified for 1-line ticker mode
  if (CurrentTickerDisplayMode = 1) OR (OneLineModeEnable = TRUE) OR
     (ExtraLine_Collection.Count > 0) OR (MainForm.ScoreLogoOnly.Checked = TRUE) then
  begin
    CurrentTimeZone := -6;
    LogoClockMode := 1;
  end;

  //Update region
  Case LogoClockMode of
      //Logo mode
   1: begin
        //Set default mode
        CmdStr := SOT + 'SM' + ETX + '2' + GS;
        //Set template ID = 70 for logo/clock
        CmdStr := CmdStr + 'ST' + ETX + '70' + GS;
        //Get logo name based on state of radio button control
        Case MainForm.LogoSelect.ItemIndex of
          0: LogoName := 'THESCORE';
          1: LogoName := 'THESCOREHD';
          2: LogoName := 'THESCORECOM';
        end;
        //If logo, set background for data field
        CmdStr := CmdStr + 'SD' + ETX + '0' + ETX + LogoName + GS;
        CmdStr := CmdStr + 'SD' + ETX + '1' + ETX + '' + GS;
        CmdStr := CmdStr + 'SD' + ETX + '1000' + ETX + 'CLEAR' + GS;
        CmdStr := CmdStr + 'SD' + ETX + '1002' + ETX + '90' + GS;
        //Send the commsnd
        CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) then
        begin
          //Send command
          ApdWinsockPort1.Output := CmdStr;
        end;
        //Disable the update timer
        DisplayClockTimer.Enabled := TRUE;
      end;
      //Clock mode
   2: begin
        //Set default mode
        CmdStr := SOT + 'SM' + ETX + '2' + GS;
        //Set template ID = 70 for logo/clock
        CmdStr := CmdStr + 'ST' + ETX + '70' + GS;
        //If logo, set background for data field
        CmdStr := CmdStr + 'SD' + ETX + '0' + ETX + 'CLOCKBG' + GS;
        CmdStr := CmdStr + 'SD' + ETX + '1' + ETX + TimeStr + GS;
        CmdStr := CmdStr + 'SD' + ETX + '1000' + ETX + 'CLEAR' + GS;
        CmdStr := CmdStr + 'SD' + ETX + '1002' + ETX + '90' + GS;
        //Send the commsnd
        CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) then
        begin
          //Send command
          ApdWinsockPort1.Output := CmdStr;
        end;
        //Enable the update timer
        DisplayClockTimer.Enabled := TRUE;
      end;
  end;
end;

//Handler for displayable clock update timer
procedure TEngineInterface.DisplayClockTimerTimer(Sender: TObject);
var
  CmdStr: String;
begin
  //Modified for 1-Line ticker
  if (OneLineModeEnable = FALSE) then SendNextClockLogoCommand;
end;

////////////////////////////////////////////////////////////////////////////////
// BREAKING NEWS FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to clear breaking news
procedure TEngineInterface.ClearBreakingNews;
var
  CmdStr: String;
begin
  //Don't send commands if not running
  //if (RunningTicker = TRUE) then
  begin
    //Set default mode
    CmdStr := SOT + 'SM' + ETX + '2' + GS;
    //Set template ID = 70 for logo/clock
    CmdStr := CmdStr + 'ST' + ETX + '51' + GS;
    CmdStr := CmdStr + 'SD' + ETX + '0' + ETX + 'EXTRALINE' + GS;
    CmdStr := CmdStr + 'SD' + ETX + '1' + ETX + ' ' + GS;
    CmdStr := CmdStr + 'SD' + ETX + '2' + ETX + ' ' + GS;
    CmdStr := CmdStr + 'SD' + ETX + '1000' + ETX + 'CLEAR' + GS;
    CmdStr := CmdStr + 'SD' + ETX + '1002' + ETX + '0' + GS;
    CmdStr := CmdStr + 'SI' + ETX + ' ' + ETX + ' ' + ETX + ' ' + ETX + ' ' + ETX + ' ' + GS;
    //Send the command
    CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
    if (SocketConnected) AND (EngineParameters.Enabled = TRUE) then
    begin
      //Send command
      ApdWinsockPort1.Output := CmdStr;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// LOGGING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToErrorLog (ErrorString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   ErrorLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists('c:\TSTN_Error_LogFiles') = FALSE) then CreateDir('c:\TSTN_Error_LogFiles');
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   ErrorLogStr := TimeStr + '    ' + DateStr + '    ' + ErrorString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := 'c:\TSTN_Error_Logfiles\TSTNErrorLog' + DayStr + '.txt';
   AssignFile (ErrorLogFile, FileName);
   //Write error information to one line in file
   //If file doesn't exist create a new one. Otherwise, check to see if it's one
   //month old. If so, delete & create a new one. Otherwise, append error to file
   if (Not (FileExists(FileName))) then
   begin
      ReWrite (ErrorLogFile);
      try
         Writeln (ErrorLogFile, ErrorLogStr);      finally         CloseFile (ErrorLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (ErrorLogFile);      try         Readln (ErrorLogFile, TestStr);      finally         CloseFile (ErrorLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (ErrorLogFile);         {Create a new logfile}         ReWrite (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end;   end;end;

// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToAsRunLog (AsRunString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   AsRunLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists(AsRunLogFileDirectoryPath) = FALSE) then
   begin
     if (DirectoryExists('c:\TSTN_AsRun_LogFiles') = FALSE) then
       CreateDir('c:\TSTN_AsRun_LogFiles');
     DirectoryStr := 'c:\TSTN_AsRun_LogFiles\';
   end
   else DirectoryStr := AsRunLogFileDirectoryPath;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   AsRunLogStr := TimeStr + '    ' + DateStr + '    ' + AsRunString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'TSTNAsRunLog' + DayStr + '.txt';
   AssignFile (AsRunLogFile, FileName);
   {If (FileExists (ErrorLogFile)) then Erase (ErrorLogFile);}
   {Write error information to one line in file}
   {If file doesn't exist create a new one. Otherwise, check to see if it's one }
   {month old. If so, delete & create a new one. Otherwise, append error to file}
   if (Not (FileExists(FileName))) then begin
      ReWrite (AsRunLogFile);
      try
         Writeln (AsRunLogFile, AsRunLogStr);      finally         CloseFile (AsRunLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (AsRunLogFile);      try         Readln (AsRunLogFile, TestStr);      finally         CloseFile (AsRunLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then      begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (AsRunLogFile);         {Create a new logfile}         ReWrite (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end;   end;end;

end.


