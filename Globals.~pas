unit Globals;

interface

uses
  StBase, StColl, Classes;

type
  TickerRec = Record
    Event_Index: Smallint;
    Event_GUID: TGUID;
    Enabled: Boolean;
    Is_Child: Boolean;
    League: String[10];
    Template_ID: SmallInt;
    Record_Type: SmallInt;
    GameID: String[20];
    SponsorLogo_Name: String[50];
    SponsorLogo_Dwell: SmallInt;
    StatStoredProcedure: String[50];
    StatType: SmallInt;
    StatTeam: String[50];
    StatLeague: String[20];
    StatRecordNumber: SmallInt;
    UserData: Array[1..25] of String[255];
    StartEnableDateTime: TDateTime;
    EndEnableDateTime: TDateTime;
    Selected: Boolean;
    DwellTime: LongInt;
    Description: String[100];
    Comments: String[100];
  end;

  BreakingNewsRec = Record
    Event_Index: Smallint;
    Event_GUID: TGUID;
    Enabled: Boolean;
    League: String[10];
    Template_ID: SmallInt;
    Record_Type: SmallInt;
    UserData: Array[1..25] of String[255];
    StartEnableDateTime: TDateTime;
    EndEnableDateTime: TDateTime;
    Selected: Boolean;
    DwellTime: LongInt;
    Description: String[100];
    Comments: String[100];
  end;

  ColorRec = record
    Description: String;
    DBIndexValue: SmallInt;
  end;

  TeamRec = record
    League: String[25];
    OldSTTeamCode: String[8];
    NewSTTeamCode: String[15];
    StatsIncID: Double;
    LongName: String[100];
    ShortName: String[100];
    BaseName: String[100];
    DisplayName1: String[50];
    DisplayName2: String[50];
  end;

  TeamDisplayInfoRec = record
    DisplayMnemonic: String[10];
    Displayname: String[50];
    Division: String[50];
    Conference: String[50];
  end;

  StatRec = Record
    StatLeague: String[10];
    StatType: SmallInt;
    StatDescription: String[50];
    StatHeader: String[50];
    StatAbbreviation: String[10];
    StatStoredProcedure: String[50];
    StatDataField: String[20];
    UseStatQualifier: Boolean;
    StatQualifier: String[100];
    StatQualifierValue: SmallInt;
  end;

  SelectedStatRec = record
    StatType: SmallInt;
    StatStoredProcedure: String;
    StatLeague: String;
    StatTeam: String;
  end;

  SponsorLogoRec = Record
    SponsorLogoIndex: SmallInt;
    SponsorLogoName: String[50];
    SponsorLogoFilename: String[50];
  end;

  PromoLogoRec = Record
    PromoLogoIndex: SmallInt;
    PromoLogoName: String[20];
    PromoLogoFilename: String[20];
  end;

  GamePhaseRec = record
    League: String[10];
    ST_Phase_Code: SmallInt;
    SI_Phase_Code: SmallInt;
    Label_Period: String[20];
    Display_Period: String[10];
    End_Is_Half: Boolean;
    Display_Half: String[20];
    Display_Final: String[20];
    Display_Extended1: String[10];
    Display_Extended2: String[10];
    Game_OT: Boolean;
  end;

  TemplateDefsRec = record
    Template_ID: SmallInt;
    Template_Type: SmallInt;
    Template_Description: String[100];
    Template_Has_Children: Boolean;
    Template_Is_Child: Boolean;
    Record_Type: SmallInt;
    SceneDirectorID: SmallInt;
    Default_Dwell: Word;
    ManualLeague: Boolean;
    UsesGameData: Boolean;
    RequiredGameState: SmallInt;
    TemplateSponsorType: SmallInt;
    StartEnableDateTime: TDateTime;
    EndEnableDateTime: TDateTime;
    IsFantasyTemplate: Boolean; //OptionalFlag1
    IsLeadersTemplate: Boolean; //OptionalFlag2
  end;

  ChildTemplateIDRec = record
    Template_ID: SmallInt;
    Child_Template_ID: SmallInt;
    Child_Default_Enable_State: Boolean;
  end;

  TemplateFieldsRec = record
    Template_ID: SmallInt;
    Field_ID: SmallInt;
    Field_Type: SmallInt;
    Field_Is_Manual: Boolean;
    Field_Label: String[50];
    Action_Controller_ID: SmallInt;
    Scene_Field_Name_A: String[200];
    Scene_Field_Name_B: String[200];
    Field_Contents: String[200];
    Field_Format_Prefix: String[50];
    Field_Format_Suffix: String[50];
    Field_Max_Length: SmallInt;
    LastValueCheckString: String[255];
    ValueHasChanged: Boolean;
  end;

  CategoryRec = record
    Category_Type: SmallInt;
    Category_ID: SmallInt;
    Category_Name: String[50];
    Category_Label: String[10];
    Category_Description: String[100];
    Category_Is_Sport: Boolean;
    Category_Sport: String[10];
    Sport_Games_Table_Name: String[100];
  end;

  CategoryTemplatesRec = record
    Category_ID: SmallInt;
    Template_ID: SmallInt;
    Template_Type: SmallInt;
    Template_Description: String[100];
  end;

  GameRec = record
    League: String[10];
    GameID: String[15];
    DataFound: Boolean;
    SubLeague: String[50];
    DoubleHeader: Boolean;
    DoubleHeaderGameNumber: SmallInt;
    VStatsIncID: LongInt;
    HStatsIncID: LongInt;
    HName: String[50];
    VName: String[50];
    HMnemonic: String[10];
    VMnemonic: String[10];
    HRecord: String[15];
    VRecord: String[15];
    HStreak: String[15];
    VStreak: String[15];
    HRank: String[4];
    VRank: String[4];
    HScore: SmallInt;
    VScore: SmallInt;
    VOpeningOdds: Double;
    HOpeningOdds: Double;
    OpeningTotal: Double;
    VCurrentOdds: Double;
    HCurrentOdds: Double;
    CurrentTotal: Double;
    Phase: SmallInt;
    TimeMin: SmallInt;
    TimeSec: SmallInt;
    StateForInProgressCheck: SmallInt;
    State: SmallInt;
    Count: String[5];
    Situation: String[50];
    Batter: String[25];
    Baserunners: Array[1..3] of String[25];
    Description: String[100];
    Date: TDateTime;
    StartTime: TDateTime;
    Visitor_Param1, Visitor_Param2: SmallInt;
    Home_Param1, Home_Param2: SmallInt;
  end;

  RecordTypeRec = record
    Playlist_Type: SmallInt;
    Record_Type: SmallInt;
    Record_Description: String[50];
  end;

  LineupText = Record
    UseLineup: Boolean;
    TextFields: Array[1..6] of String;
  end;

  EngineRec = Record
    Enabled: Boolean;
    IPAddress: String;
    Port: String;
  end;

  PlaylistInfoRec = record
    PlaylistName: String[50];
    LastPlaylistSaveTimeString: String[50];
  end;

  StyleChipRec = record
    StyleChip_Index: SmallInt;
    StyleChip_Description: String[50];
    StyleChip_Code: String[4];
    StyleChip_Type: SmallInt;
    StyleChip_String: String[20];
    StyleChip_FontCode: SmallInt;
    StyleChip_CharacterCode: SmallInt;
  end;

  LeagueCodeRec = record
    Heading: String[10];
  end;

  AutomatedLeagueRec = record
    SI_LeagueCode: String[10];
    ST_LeagueCode: String[10];
    Display_Mnemonic: String[10];
  end;

  WeatherIconRec = record
    Icon_Description: String[50];
    Icon_Name: String[50];
  end;

  FootballSituationRec = record
    VisitorHasPossession: Boolean;
    HomeHasPossession: Boolean;
    YardsFromGoal: SmallInt;
    Down: SmallInt;
    Distance: SmallInt;
  end;

  PlayoutStationInfoRec = record
    Station_ID: SmallInt;
    Station_Description: String[20];
  end;

  WeatherForecastRec = record
    CurrentLeague: String[10];
    CurrentHomeStatsID: LongInt;
    Weather_Stadium_Name: String[50];
    Weather_Current_Temperature: String[5];
    Weather_High_Temperature: String[5];
    Weather_Low_Temperature: String[5];
    Weather_Current_Conditions: String[50];
    Weather_Forecast_Icon_Day: String[50];
    Weather_Forecast_Icon_Night: String[50];
  end;

  //For team records
  TeamRecordRec = record
    Param: Array[1..4] of String;
  end;

  //For starting pitchers
  StartingPitcherStatsRec = record
    CurrentGameID: String[15];
    Home_Pitcher_Name_Last: String[20];
    Home_Pitcher_Name_First: String[20];
    Home_Pitcher_Wins: String[5];
    Home_Pitcher_Losses: String[5];
    Visitor_Pitcher_Name_Last: String[20];
    Visitor_Pitcher_Name_First: String[20];
    Visitor_Pitcher_Wins: String[5];
    Visitor_Pitcher_Losses: String[5];
  end;

  //For game final pitcher stats
  FinalPitcherStatsRec = record
    CurrentGameID: String[15];
    NeedToAddSavingPitcher: Boolean;
    Winning_Pitcher_Name_Last: String[20];
    Winning_Pitcher_Name_First: String[20];
    Winning_Pitcher_Wins: String[5];
    Winning_Pitcher_Losses: String[5];
    Winning_Pitcher_IP: String[5];
    Winning_Pitcher_K: String[5];
    Winning_Pitcher_BB: String[5];
    Winning_Pitcher_Hits: String[5];
    Winning_Pitcher_Runs: String[5];
    Winning_Pitcher_EarnedRuns: String[5];
    Winning_Pitcher_HomeRuns: String[5];
    Losing_Pitcher_Name_Last: String[20];
    Losing_Pitcher_Name_First: String[20];
    Losing_Pitcher_Wins: String[5];
    Losing_Pitcher_Losses: String[5];
    Losing_Pitcher_IP: String[5];
    Losing_Pitcher_K: String[5];
    Losing_Pitcher_BB: String[5];
    Losing_Pitcher_Hits: String[5];
    Losing_Pitcher_Runs: String[5];
    Losing_Pitcher_EarnedRuns: String[5];
    Losing_Pitcher_HomeRuns: String[5];
    Saving_Pitcher_Name_Last: String[20];
    Saving_Pitcher_Name_First: String[20];
    Saving_Pitcher_Saves: String[5];
  end;

  FinalBatterStatsRec = record
    Name_Last: String[20];
    Name_First: String[20];
    Atbats: String[5];
    Hits: String[5];
    Runs: String[5];
    Doubles: String[5];
    Triples: String[5];
    HomeRuns: String[5];
    RBI: String[5];
    StolenBases: String[5];
  end;

  //Basketball stats function records
  FinalBBallStatsRec = record
    CurrentLeague: String[10];
    CurrentGameID: String[15];
    Name_Last: String[20];
    Name_First: String[20];
    Points: String;
    Rebounds: String;
    Assists: String;
  end;

  //Hockey stats function records
  //Skater stats
  FinalSkaterStatsRec = record
    CurrentGameID: String[15];
    Name_Last: String[20];
    Name_First: String[20];
    Goals: String;
    Assists: String;
    SOG: String;
  end;

  //Goalie stats
  FinalGoalieStatsRec = record
    CurrentGameID: String[15];
    Name_Last: String[20];
    Name_First: String[20];
    Shots: String;
    Saves: String;
    GoalsAgainst: String;
  end;

  //Football stats function records
  FinalFBStatsRec = record
    CurrentLeague: String[10];
    CurrentGameID: String[15];
    VPassingName_Last: String[20];
    VPassingName_First: String[20];
    VPassingAttempts: String;
    VPassingCompletions: String;
    VPassingYards: String;
    VPassingTouchdowns: String;
    VPassingInterceptions: String;
    HPassingName_Last: String[20];
    HPassingName_First: String[20];
    HPassingAttempts: String;
    HPassingCompletions: String;
    HPassingYards: String;
    HPassingTouchdowns: String;
    HPassingInterceptions: String;
    VRushingName_Last: String[20];
    VRushingName_First: String[20];
    VRushingAttempts: String;
    VRushingYards: String;
    VRushingTouchdowns: String;
    HRushingName_Last: String[20];
    HRushingName_First: String[20];
    HRushingAttempts: String;
    HRushingYards: String;
    HRushingTouchdowns: String;
    VReceivingName_Last: String[20];
    VReceivingName_First: String[20];
    VReceivingReceptions: String;
    VReceivingYards: String;
    VReceivingTouchdowns: String;
    HReceivingName_Last: String[20];
    HReceivingName_First: String[20];
    HReceivingReceptions: String;
    HReceivingYards: String;
    HReceivingTouchdowns: String;
  end;

  //General stats function records
  BoxscoreRec = record
    CurrentLeague: String[10];
    CurrentGameID: String[15];
    VNumPeriods: SmallInt;
    VScore: String[3];
    VPeriod1: String[3];
    VPeriod2: String[3];
    VPeriod3: String[3];
    VPeriod4: String[3];
    VPeriod5: String[3];
    VPeriod6: String[3];
    VPeriod7: String[3];
    HNumPeriods: SmallInt;
    HScore: String[3];
    HPeriod1: String[3];
    HPeriod2: String[3];
    HPeriod3: String[3];
    HPeriod4: String[3];
    HPeriod5: String[3];
    HPeriod6: String[3];
    HPeriod7: String[3];
  end;

  //////////////////////////////////////////////////////////////////////////////
  //Begin Fantasy record types
  //////////////////////////////////////////////////////////////////////////////
  //Generic
  FantasyGeneralStatsRec = record
    League: String[10];
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    StatValue: Array[1..10] of String;
  end;

  //NFL
  FantasyFootballPassingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Attempts: Array[1..10] of String;
    Completions: Array[1..10] of String;
    PassingYards: Array[1..10] of String;
    PassingTDs: Array[1..10] of String;
  end;

  FantasyFootballRushingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Rushes: Array[1..10] of String;
    Yards: Array[1..10] of String;
    TDs: Array[1..10] of String;
  end;

  FantasyFootballReceivingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Receptions: Array[1..10] of String;
    Yards: Array[1..10] of String;
    TDs: Array[1..10] of String;
  end;

  //MLB
  FantasyBaseballPitchingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Hits: Array[1..10] of String;
    Walks: Array[1..10] of String;
    Wins: Array[1..10] of String;
    Losses: Array[1..10] of String;
    Saves: Array[1..10] of String;
    Strikeouts: Array[1..10] of String;
    EarnedRuns: Array[1..10] of String;
  end;

  FantasyBaseballBattingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    AtBats: Array[1..10] of String;
    Hits: Array[1..10] of String;
    RBI: Array[1..10] of String;
    Doubles: Array[1..10] of String;
    Triples: Array[1..10] of String;
    HomeRuns: Array[1..10] of String;
    Walks: Array[1..10] of String;
    StolenBases: Array[1..10] of String;
  end;

  //NBA
  FantasyBasketballStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Points: Array[1..10] of String;
    Rebounds: Array[1..10] of String;
    Assists: Array[1..10] of String;
    ThreePtFieldGoalsMade: Array[1..10] of String;
    Blocks: Array[1..10] of String;
    Steals: Array[1..10] of String;
    FieldGoalsMade: Array[1..10] of String;
    FieldGoalAttempts: Array[1..10] of String;
  end;

  //NHL
  FantasyHockeyPointsStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Points: Array[1..10] of String;
  end;

  FantasyHockeyGoalsStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Goals: Array[1..10] of String;
  end;

  FantasyHockeyAssistsStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Assists: Array[1..10] of String;
  end;
  //////////////////////////////////////////////////////////////////////////////
  //End Fantasy record types
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  //Start Season Leaders record types
  //////////////////////////////////////////////////////////////////////////////
  //General leader type
  SeasonLeadersGeneralStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    League: String[10];
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    StatValue: Array[1..10] of String;
  end;

  //////////////////////////////////////////////////////////////////////////////
  //End Season Leaders record types
  //////////////////////////////////////////////////////////////////////////////

  DefaultTemplatesRec = record
    Category_ID: Smallint;
    Category_Name: String[50];
    Template_Index: Integer;
    Template_ID: Integer;
    Template_Description: String[255];
    Enabled: Boolean;
    Selected: Boolean;
  end;

  SymbolValueRec = record
    SymbolValue: String;
    OKToDisplay: Boolean;
  end;

  //Added for XPression support
  CommandRec = record
    Dwell: SmallInt;
    Command: ANSIString;
  end;

  CommandTypeRec = record
    CommandType: SmallInt;
    CommandDescription: String[100];
    CommandPrefix: String[10];
  end;

  ActionTypeRec = record
		ActionType: SmallInt;
    ActionDescription: String[100];
  end;

  TemplateCommandRec = record
    TemplateID: Double;
    ActionID: LongInt;
    CommandID: LongInt;
    ActionType: SmallInt;
    ActionDescription: String[100];
    CommandDescription: String[100];
    CommandType: SmallInt;
    SceneDirectorID: SmallInt;
    SceneDirectorName: String[100];
    ActionControllerID: SmallInt;
    AlwaysFireActionController: Boolean;
    ActionController_A: String[100];
    ActionController_B: String[100];
    ActionIsCrawl: Boolean;
    Delay: SmallInt;
    CommandEnabled: Boolean;
  end;

  SceneDirectorRec = record
    SceneDirectorID: SmallInt;
    SceneDirectorName: String[100];
    SceneDirectorState: SmallInt;
  end;

  ActionControllerRec = record
    SceneID: SmallInt;
    ActionControllerID: SmallInt;
    SceneDirectorName: String[100];
    ActionControllerName_A: String[100];
    ActionControllerName_B: String[100];
    ActionControllerState: SmallInt;
    ActionIsCrawl: Boolean;
  end;

  ScoreChipRec = record
    ChipDirectorName: String[20];
    CurrentChipStatus: Boolean;
    NewChipStatus: Boolean;
  end;

  CurrentSponsorLogoRec = Record
    CurrentSponsorLogoName: String[50];
    CurrentSponsorLogoDwell: SmallInt;
    CurrentSponsorTemplate: SmallInt;
    LastDisplayedSponsorLogoName: String[50];
  end;
  
var
  DebugMode: Boolean;
  PrefsFile: TextFile;
  Operator: String;
  Description: String;
  User_Collection: TStCollection; //Collection used for user logins
  Ticker_Collection: TStCollection; //Collection used for zipper entries
  BreakingNews_Collection: TStCollection; //Collection used for zipper entries
  Temp_Ticker_Collection: TStCollection; //Collection used for zipper entries
  Temp_BreakingNews_Collection: TStCollection; //Collection used for zipper entries
  Team_Collection: TStCollection; //Collection used for NCAA teams
  Ticker_Playout_Collection: TStCollection; //Collection used for zipper playout
  Overlay_Collection: TStCollection; //Collection used for overlays
  SponsorLogo_Collection: TStCollection; //Collection used for Sponsor logos
  PromoLogo_Collection: TStCollection; //Collection used for Promo logos
  Stat_Collection: TStCollection; //Collection used for stats types
  Temp_Stat_Collection: TStCollection; //Collection used for stats types
  Game_Phase_Collection: TStCollection; //Collection used for stats types
  Template_Defs_Collection: TStCollection; //Collection used for Template definitions
  Child_Template_ID_Collection: TStCollection; //Collection uses for child template ID definitions
  Template_Fields_Collection: TStCollection; //Collection for template fields
  Temporary_Fields_Collection: TStCollection; //Collection for template fields
  Categories_Collection: TStCollection; //Collection for ticker categories
  Category_Templates_Collection: TStCollection; //Collection for templates that apply to ticker categories
  Sports_Collection: TStCollection; //Collection for ticker categories
  RecordType_Collection: TStCollection; //Collection for record types
  StyleChip_Collection: TStCollection; //Collection for style chips
  LeagueCode_Collection: TStCollection; //Collection for league codes
  Automated_League_Collection: TStCollection; //Collection used for leagues
  Weather_Icon_Collection: TStCollection; //Collection used for weather icons
  PlayoutStationInfo_Collection: TStCollection; //Collection used for playout station info
  Default_Templates_Collection: TStCollection; //Collection used for default templates by league/category
  Selected_Templates_Collection: TStCollection; //Collection used for selected templates by league/category
  DBConnectionString, DBConnectionString2: String;
  StationID: SmallInt;
  SearchText: String;
  SpellCheckerDictionaryDir: String;
  LoginComplete: Boolean;
  LastPageIndex: SmallInt;
  LoopDefaultZipper: Boolean;
  CurrentTickerEntryIndex: SmallInt;
  CurrentSponsorEntryIndex: SmallInt;
  CurrentTickerPlaylistID: Double;
  CurrentSponsorPlaylistID: Double;
  LastOverlay: String;
  NewOverlayPending: Boolean;
  ZipperColors: Array[0..8] of ColorRec;
  GameEntryMode: SmallInt; //1 = NCAA, 2 = NFL
  MaxCharsMatchupNotes, MaxCharsGameNotes: SmallInt;
  CurrentLeague: String;
  UseXMLGTServer: Boolean;
  SelectedRecordForCopy: SmallInt;
  SelectForCut: Boolean;
  TemporaryTickerDataArray: Array[1..1500] of TickerRec;
  TemporaryTickerDataCount: SmallInt;
  TemporaryBreakingNewsDataArray: Array[1..500] of TickerRec;
  TemporaryBreakingNewsDataCount: SmallInt;
  SocketConnected: Boolean;
  EngineParameters: EngineRec;
  Error_Condition: Boolean;
  ErrorLoggingEnabled: Boolean;
  AsRunLoggingEnabled: Boolean;
  PacketEnable: Boolean;
  TickerDisplayMode: SmallInt;
  CurrentSponsorLogoName: Array[1..2] of String;
  CurrentSponsorLogoDwell: SmallInt;
  RunningTicker: Boolean;
  TickerAbortFlag: Boolean;
  LoopTicker: Boolean;
  ErrorLogFile: TextFile; {File for logging hardware interface errors}
  AsRunLogFile: TextFile; {File for logging sponsors & GPIs}
  AsRunLogFileDirectoryPath: String;
  PlaylistInfo: Array[1..3] of PlaylistInfoRec;
  LogoClockMode: SmallInt;
  CurrentTimeZone: SmallInt;
  LogoClockEnabled: Boolean;
  LastDuplicationCount: SmallInt;
  EngineIPAddress: String;
  ForceUpperCase: Boolean;
  ForceUpperCaseGameInfo: Boolean;
  GameStartTimeOffset: Double;
  TimeZoneSuffix: String;
  DefaultTickerMode: SmallInt;
  EnableTemplateFieldFormatting: Boolean;
  UseGameFinalGlyph: Boolean;
  GameFinalGlyphCharacterID: SmallInt;
  EnablePersistentSponsorLogo: Boolean;
  DefaultToOneLoopThrough: Boolean;
  BlankOutLineupForSponsors: Boolean;
  EnableLocalPlaylistControls: Boolean;
  CurrentBreakingNewsEntryIndex: SmallInt;
  AllowAlertBackgroundsForNews: Boolean;
  AutoTrimText: Boolean;
  EnableGameScheduleLookAhead: Boolean;
  EnableDefaultTemplates: Boolean;
  PlaylistAutoSaveEnable: Boolean;
  PlaylistAutoSaveInterval: SmallInt;

  //Used for Phase 2 stats data to prevent multiple stored proc calls
  CurrentWeatherForecastRec: WeatherForecastRec;
  CurrentStartingPitcherStatsRec: StartingPitcherStatsRec;
  CurrentFinalPitcherStatsRec: FinalPitcherStatsRec;
  CurrentBoxscoreRec: BoxscoreRec;
  CurrentBBallStatsRec: FinalBBallStatsRec;
  CurrentFBStatsRec: FinalFBStatsRec;
  //For fantasy stats
  CurrentFantasyFootballPassingStatsRec: FantasyFootballPassingStatsRec;
  CurrentFantasyFootballRushingStatsRec: FantasyFootballRushingStatsRec;
  CurrentFantasyFootballReceivingStatsRec: FantasyFootballReceivingStatsRec;
  CurrentFantasyBaseballPitchingStatsRec: FantasyBaseballPitchingStatsRec;
  CurrentFantasyBaseballBattingStatsRec: FantasyBaseballBattingStatsRec;
  CurrentFantasyBasketballPointsStatsRec: FantasyBasketballStatsRec;
  CurrentFantasyBasketballReboundsStatsRec: FantasyBasketballStatsRec;
  CurrentFantasyBasketballAssistsStatsRec: FantasyBasketballStatsRec;
  CurrentFantasyHockeyPointsStatsRec: FantasyHockeyPointsStatsRec;
  CurrentFantasyHockeyGoalsStatsRec: FantasyHockeyGoalsStatsRec;
  CurrentFantasyHockeyAssistsStatsRec: FantasyHockeyAssistsStatsRec;

  //Version 3.1 - New Fantasy Record Types
  CurrentFantasyBaseballHitsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballHomeRunsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballRBIsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballRunsScoredStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballStolenBasesStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballWinsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballSavesStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballStrikeoutsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballInningsPitchedStatsRec: FantasyGeneralStatsRec;

  //Version 3.1 - Start season leaders record types
  Current_NFL_QB_Leaders_Yds: SeasonLeadersGeneralStatsRec;
  Current_NFL_QB_Leaders_TDs: SeasonLeadersGeneralStatsRec;
  Current_NFL_RB_Leaders_Yds: SeasonLeadersGeneralStatsRec;
  Current_NFL_RB_Leaders_TDs: SeasonLeadersGeneralStatsRec;
  Current_NFL_WR_Leaders_Yds: SeasonLeadersGeneralStatsRec;
  Current_NFL_WR_Leaders_TDs: SeasonLeadersGeneralStatsRec;
  Current_NFL_Field_Goal_Leaders_FGM: SeasonLeadersGeneralStatsRec;

  Current_MLB_AL_Leaders_Hits: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_HR: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_RBIs: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Runs: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Stolen_Bases: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Strikeouts: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Innings_Pitched: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Wins: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Saves: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_ERA: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Hits: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_HR: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_RBIs: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Runs: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Stolen_Bases: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Strikeouts: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Innings_Pitched: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Wins: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Saves: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_ERA: SeasonLeadersGeneralStatsRec;

  Current_NHL_Leaders_GPG: SeasonLeadersGeneralStatsRec;
  Current_NHL_Leaders_APG: SeasonLeadersGeneralStatsRec;
  Current_NHL_Leaders_PPG: SeasonLeadersGeneralStatsRec;
  Current_NHL_Leaders_Saves: SeasonLeadersGeneralStatsRec;
  Current_NHL_Leaders_GAA: SeasonLeadersGeneralStatsRec;

  Current_NBA_Leaders_PPG: SeasonLeadersGeneralStatsRec;
  Current_NBA_Leaders_RPG: SeasonLeadersGeneralStatsRec;
  Current_NBA_Leaders_APG: SeasonLeadersGeneralStatsRec;
  Current_NBA_Leaders_SPG: SeasonLeadersGeneralStatsRec;
  Current_NBA_Leaders_BPG: SeasonLeadersGeneralStatsRec;
  //End season leaders record types

  //To enable showing time and day in schedule for games not scheduled for the current day
  ShowTimeAndDay: Boolean;

  //For filtering template lists
  EnableCrawlTemplates: Boolean;

  //Format for SNY
  FormatForSNY: Boolean;

  //Abbreviations
  ABBV_Rushes: String;
  ABBV_Receptions: String;
  ABBV_Yards: String;
  ABBV_TDs: String;
  ABBV_INTs: String;
  ABBV_Points: String;
  ABBV_Rebounds: String;
  ABBV_Assists: String;
  ABBV_Steals: String;
  ABBV_Blocks: String;
  ABBV_PPG: String;
  ABBV_RPG: String;
  ABBV_APG: String;
  ABBV_SPG: String;
  ABBV_BPG: String;
  ABBV_Goals: String;
  ABBV_Saves: String;

  //For disabling automated rankings
  UseManualRankings_NCAAB, UseManualRankings_NCAAF, UseManualRankings_NCAAW: Boolean;

  //Added for NCAAB Tournament
  UseSeedingForNCAABGames: Boolean;

  //Added for NCAAF BCS Rankings
  UseBCSRankings_NCAAF: Boolean;

  //For actions/commands
  QueuedCommandCollection: TStringList;

  ActionType_Collection: TStCollection;
  CommandType_Collection: TStCollection;
  TemplateCommand_Collection: TStCollection;
  TempTemplateCommand_Collection: TStCollection;
  ActionController_Collection: TStCollection;
  SceneDirector_Collection: TStCollection;
  LastTemplateID: SmallInt;
  LastSceneDirectorID: SmallInt;
  LastLineupData: LineUpText;
  SceneName: String;
  TickerBedInDelay: SmallInt;
  LineupInDelay: SmallInt;
  LineupOutDelay: SmallInt;
  LastCommandSentWasLineup: Boolean;
  InitialDataTemplateFired: Boolean;
  ScoreChipStatus: Array[1..6] of ScoreChipRec;
  LastGameIDDisplayed: String;

  //For sponsor logos
  CurrentSponsorInfo: CurrentSponsorLogoRec;

  //For headers
  FantasyStartTemplate: SmallInt;
  FantasyEndTemplate: SmallInt;
  LeadersStartTemplate: SmallInt;
  LeadersEndTemplate: SmallInt;

  CrawlIsEnabled: Boolean;
  UseLongDelaysForPlayout: Boolean;

  EnableTeamRecords_MLB, EnableTeamRecords_NBA, EnableTeamRecords_NFL, EnableTeamRecords_NHL: Boolean;
  EnableTeamRecords_NCAAB, EnableTeamRecords_NCAAF, EnableTeamRecords_NCAAW, EnableTeamRecords_MLS: Boolean;
  
const
  //Default delay for pages
  DEFAULTDELAY = 5000;

  //For playlist modes
  TICKER = 1;
  BREAKINGNEWS = 2;

  //For control
  NOENTRYINDEX = -1;

  //For record moves
  UP = -1;
  DOWN = 1;

  //Color format strings
  BLACK = '[c 0]';
  YELLOW = '[c 16]';
  GRAY = '[c 17]';
  WHITE = '[c 18]';
  BLUE = '[c 19]';

  //For football field position
  FIELDARROWRIGHT = #180;
  FIELDARROWLEFT = #181;
  FOOTBALLPOSSESSION = #200;
  LEADERINDICATOR = #200;

  //Game States for Stats Inc.
  PREGAME = 1;
  INPROGRESS = 2;
  FINAL = 3;
  POSTPONED = 4;
  DELAYED = 5;
  SUSPENDED = 6;
  CANCELLED = 7;
  UNDEFINED = -1;

  SINGLETEMPLATEONLY = FALSE;
  ALLDEFAULTTEMPLATES = TRUE;

  //Sponsor templates
  ONE_LINE_SPONSOR_TAGLINE = 1;
  ONE_LINE_SPONSOR_FULL_PAGE = 51;
  ONE_LINE_LOWER_RIGHT_SPONSOR_IN = 52;
  ONE_LINE_LOWER_RIGHT_SPONSOR_OUT = 53;
  TWO_LINE_SPONSOR_TAGLINE = 1001;
  TWO_LINE_SPONSOR_FULL_PAGE = 1051;
  TWO_LINE_LOWER_RIGHT_SPONSOR_IN = 1052;
  TWO_LINE_LOWER_RIGHT_SPONSOR_OUT = 1053;

  //For XPression support
  //For command types
  COMMAND_TYPE_LOAD_PROJECT = 1;
  COMMAND_TYPE_OPEN_SCENE = 2;
  COMMAND_TYPE_UPDATE_TEMPLATE_FIELDS = 3;
  COMMAND_TYPE_FIRE_SCENE_DIRECTOR = 4;
  COMMAND_TYPE_FIRE_ACTION_CONTROLLER = 5;

  //For action types
  ACTION_TEMPLATE_IN_INITIAL = 1;
  ACTION_TEMPLATE_UPDATE = 2;
  ACTION_TEMPLATE_OUT = 3;
  ACTION_TEMPLATE_RESET = 4;

  SCENE_DIRECTOR_OUT = 0;
  SCENE_DIRECTOR_IN_A = 1;
  SCENE_DIRECTOR_IN_B = 2;

  ACTION_CONTROLLER_OUT = 0;
  ACTION_CONTROLLER_IN_A = 1;
  ACTION_CONTROLLER_IN_B = 2;

  ACTION_NOT_CRAWL = FALSE;
  ACTION_IS_CRAWL = TRUE;

  DONT_RESET_ALL = FALSE;
  RESET_ALL = TRUE;

  TEMPLATE_SPONOSR_TYPE_IN = 1;
  TEMPLATE_SPONOSR_TYPE_OUT = 2;

  TEMPLATE_TYPE_NORMAL = 1;
  TEMPLATE_TYPE_BREAKING_NEWS = 2;
  TEMPLATE_TYPE_PROGRAM_ALERT = 3;

  SCENE_DIRECTOR_ID_GAME_STATS = 7;
  SCENE_DIRECTOR_ID_NCAA_GAME_STATS = 8;

  ANIMATION_CONTROLLER_ID_GAME_DATA = 11;
  ANIMATION_CONTROLLER_ID_NCAA_GAME_DATA = 16;
  ANIMATION_CONTROLLER_ID_GAME_STATS_DATA = 12;
  ANIMATION_CONTROLLER_ID_NCAA_GAME_STATS_DATA = 17;

  LOG_QUEUE_COMMANDS = FALSE;
  LOG_OUTGOING_COMMANDS = TRUE;
  LOG_DELAYS = FALSE;

implementation

end.
