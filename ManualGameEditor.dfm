object ManualGameDlg: TManualGameDlg
  Left = 267
  Top = 286
  BorderStyle = bsDialog
  Caption = 'Manual Game/Odds Editor'
  ClientHeight = 472
  ClientWidth = 434
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object ManualGameEditPanel: TPanel
    Left = 8
    Top = 56
    Width = 417
    Height = 361
    BevelWidth = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label2: TLabel
      Left = 16
      Top = 207
      Width = 90
      Height = 16
      Caption = 'Visitor Name'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 335
      Top = 191
      Width = 45
      Height = 16
      Caption = 'Visitor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 335
      Top = 207
      Width = 42
      Height = 16
      Caption = 'Score'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 17
      Top = 293
      Width = 87
      Height = 16
      Caption = 'Home Name'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 335
      Top = 277
      Width = 42
      Height = 16
      Caption = 'Home'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 335
      Top = 293
      Width = 42
      Height = 16
      Caption = 'Score'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 16
      Top = 75
      Width = 92
      Height = 16
      Caption = 'Game Period'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object StartTimeLabel: TLabel
      Left = 73
      Top = 51
      Width = 117
      Height = 16
      Caption = 'Game Start Time'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object VisitorScore: TSpinEdit
      Left = 335
      Top = 229
      Width = 65
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxValue = 999
      MinValue = 0
      ParentFont = False
      TabOrder = 1
      Value = 0
    end
    object HomeScore: TSpinEdit
      Left = 335
      Top = 315
      Width = 65
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxValue = 999
      MinValue = 0
      ParentFont = False
      TabOrder = 2
      Value = 0
    end
    object GamePhase: TComboBox
      Left = 16
      Top = 95
      Width = 169
      Height = 24
      Style = csDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 16
      ParentFont = False
      TabOrder = 0
    end
    object ManualGamesOverrideEnable: TCheckBox
      Left = 18
      Top = 17
      Width = 191
      Height = 17
      Caption = 'Manual Game Override'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
    object VisitorName: TEdit
      Left = 16
      Top = 230
      Width = 305
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
    end
    object HomeName: TEdit
      Left = 16
      Top = 315
      Width = 305
      Height = 24
      ReadOnly = True
      TabOrder = 5
    end
    object GameState: TRadioGroup
      Left = 16
      Top = 135
      Width = 385
      Height = 50
      Caption = 'Game State'
      Columns = 3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Items.Strings = (
        'Pre-Game'
        'In-Progress'
        'Final')
      ParentFont = False
      TabOrder = 6
    end
    object EndOfPeriod: TCheckBox
      Left = 224
      Top = 98
      Width = 125
      Height = 17
      Caption = 'End of Period'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
    end
    object StartTime: TDateTimePicker
      Left = 201
      Top = 47
      Width = 115
      Height = 26
      Date = 38760.853983645840000000
      Time = 38760.853983645840000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Kind = dtkTime
      ParentFont = False
      TabOrder = 8
    end
  end
  object BitBtn1: TBitBtn
    Left = 120
    Top = 425
    Width = 90
    Height = 41
    Caption = '&OK'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 224
    Top = 425
    Width = 90
    Height = 41
    Caption = '&Cancel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Kind = bkCancel
  end
  object Panel2: TPanel
    Left = 8
    Top = 5
    Width = 417
    Height = 49
    BevelWidth = 2
    TabOrder = 3
    object Label21: TLabel
      Left = 7
      Top = 4
      Width = 112
      Height = 16
      Caption = 'Selected Game:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SelectedGameLabel: TLabel
      Left = 8
      Top = 25
      Width = 401
      Height = 16
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
end
