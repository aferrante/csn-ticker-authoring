object Prefs: TPrefs
  Left = 430
  Top = 45
  BorderStyle = bsDialog
  Caption = 'User Preferences'
  ClientHeight = 540
  ClientWidth = 593
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 208
    Top = 493
    Width = 81
    Height = 38
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 304
    Top = 493
    Width = 81
    Height = 38
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkCancel
  end
  object Panel2: TPanel
    Left = 16
    Top = 167
    Width = 361
    Height = 145
    TabOrder = 2
    object Label2: TLabel
      Left = 16
      Top = 8
      Width = 253
      Height = 16
      Caption = 'SQL Server ADO Connection Strings:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 16
      Top = 88
      Width = 149
      Height = 16
      Caption = 'Sportbase Database:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 16
      Top = 32
      Width = 158
      Height = 16
      Caption = 'Main Ticker Database:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Edit2: TEdit
      Left = 16
      Top = 54
      Width = 329
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object Edit3: TEdit
      Left = 16
      Top = 110
      Width = 329
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 16
    Top = 8
    Width = 361
    Height = 49
    TabOrder = 3
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 263
      Height = 16
      Caption = 'Authoring Station ID (Must Be Unique):'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SpinEdit1: TSpinEdit
      Left = 287
      Top = 12
      Width = 57
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 2
      MaxValue = 99
      MinValue = 1
      ParentFont = False
      TabOrder = 0
      Value = 1
    end
  end
  object Panel3: TPanel
    Left = 16
    Top = 317
    Width = 361
    Height = 71
    TabOrder = 4
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 254
      Height = 16
      Caption = 'Spellchecker Dictionary Folder Path:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Edit1: TEdit
      Left = 16
      Top = 30
      Width = 329
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
  object GraphicsEngineEnable: TPanel
    Left = 16
    Top = 393
    Width = 361
    Height = 90
    TabOrder = 5
    object Label4: TLabel
      Left = 36
      Top = 31
      Width = 81
      Height = 16
      Caption = 'IP Address:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 236
      Top = 31
      Width = 33
      Height = 16
      Caption = 'Port:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EngineEnable: TCheckBox
      Left = 34
      Top = 8
      Width = 255
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Ticker Graphics Engine Enabled'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object EngineIPAddress: TEdit
      Left = 36
      Top = 53
      Width = 173
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object EnginePort: TEdit
      Left = 236
      Top = 53
      Width = 85
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
  end
  object Panel4: TPanel
    Left = 16
    Top = 64
    Width = 361
    Height = 97
    TabOrder = 6
    object ForceUpperCaseCheckbox: TCheckBox
      Left = 56
      Top = 16
      Width = 248
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Force Upper-Case (Overall)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object ForceUpperCaseGameInfoCheckbox: TCheckBox
      Left = 56
      Top = 40
      Width = 248
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Force Upper-Case (Game Info)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object DefaultToSingleLoopCheckbox: TCheckBox
      Left = 56
      Top = 64
      Width = 248
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Default to Single Loop Through'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
  object RadioGroup1: TRadioGroup
    Left = 384
    Top = 138
    Width = 193
    Height = 57
    Caption = 'Default Ticker Mode'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ItemIndex = 0
    Items.Strings = (
      '1-Line'
      '2-Line')
    ParentFont = False
    TabOrder = 7
    Visible = False
  end
  object Panel5: TPanel
    Left = 384
    Top = 8
    Width = 193
    Height = 121
    TabOrder = 8
    object Label6: TLabel
      Left = 10
      Top = 16
      Width = 156
      Height = 16
      Caption = 'Use Manual Rankings:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ManualRankings_NCAAB: TCheckBox
      Left = 24
      Top = 43
      Width = 145
      Height = 17
      Alignment = taLeftJustify
      Caption = 'NCAAB (CBK)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object ManualRankings_NCAAW: TCheckBox
      Left = 24
      Top = 67
      Width = 145
      Height = 17
      Alignment = taLeftJustify
      Caption = 'NCAAW (WCBK)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object ManualRankings_NCAAF: TCheckBox
      Left = 24
      Top = 91
      Width = 145
      Height = 17
      Alignment = taLeftJustify
      Caption = 'NCAAF (CFB)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
  object Panel6: TPanel
    Left = 384
    Top = 200
    Width = 192
    Height = 281
    TabOrder = 9
    object Label9: TLabel
      Left = 8
      Top = 8
      Width = 157
      Height = 16
      Caption = 'Enable Team Records'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EnableTeamRecords_Checkbox_MLB: TCheckBox
      Left = 59
      Top = 36
      Width = 73
      Height = 17
      Caption = 'MLB'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 0
    end
    object EnableTeamRecords_Checkbox_NBA: TCheckBox
      Left = 59
      Top = 65
      Width = 73
      Height = 17
      Caption = 'NBA'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 1
    end
    object EnableTeamRecords_Checkbox_NFL: TCheckBox
      Left = 59
      Top = 95
      Width = 73
      Height = 17
      Caption = 'NFL'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 2
    end
    object EnableTeamRecords_Checkbox_NHL: TCheckBox
      Left = 59
      Top = 125
      Width = 73
      Height = 17
      Caption = 'NHL'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 3
    end
    object EnableTeamRecords_Checkbox_NCAAB: TCheckBox
      Left = 59
      Top = 154
      Width = 73
      Height = 17
      Caption = 'NCAAB'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 4
    end
    object EnableTeamRecords_Checkbox_NCAAF: TCheckBox
      Left = 59
      Top = 184
      Width = 73
      Height = 17
      Caption = 'NCAAF'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 5
    end
    object EnableTeamRecords_Checkbox_NCAAW: TCheckBox
      Left = 59
      Top = 214
      Width = 73
      Height = 17
      Caption = 'NCAAW'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 6
    end
    object EnableTeamRecords_Checkbox_MLS: TCheckBox
      Left = 59
      Top = 244
      Width = 73
      Height = 17
      Caption = 'MLS'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 7
    end
  end
end
