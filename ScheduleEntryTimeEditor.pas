unit ScheduleEntryTimeEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, ExtCtrls, Spin;

type
  TScheduleEntryEditDlg = class(TForm)
    Panel1: TPanel;
    Label15: TLabel;
    Label19: TLabel;
    Label37: TLabel;
    Label68: TLabel;
    ScheduleEntryStartDate: TDateTimePicker;
    ScheduleEntryEndDate: TDateTimePicker;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    ScheduleEntryStartTime: TDateTimePicker;
    ScheduleEntryEndTime: TDateTimePicker;
    DefaultDwellTimeLabel: TLabel;
    DefaultDwellTime: TSpinEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ScheduleEntryEditDlg: TScheduleEntryEditDlg;

implementation

{$R *.DFM}

end.
